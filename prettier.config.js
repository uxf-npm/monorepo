const prettierConfig = {
    bracketSameLine: false,
    bracketSpacing: true,
    parser: "typescript",
    plugins: [require("prettier-plugin-tailwindcss")],
    printWidth: 120,
    semi: true,
    singleQuote: false,
    tabWidth: 4,
    trailingComma: "all",
};

module.exports = prettierConfig;
