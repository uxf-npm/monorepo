# @uxf/ui

Monorepo je postavené nad NPM workspaces (npm version >= 7)

### Jak vyvíjet?

```bash
git clone git@gitlab.uxf.cz:uxf/npm-packages/monorepo.git ui

cd ui

git checkout beta

npm install --legacy-peer-deps

make dev
```

### Jak publikovat nový balíček?

Je potřeba se zalogovat do npm (jméno a heslo je v passboltu)

```bash
npm login
```

Publikování se provádí scriptem `publish.sh` (zatím vydávám jen beta verze)

```bash
./publish.sh
```

Stylování komponent vs dark theme:

```bash
.uxf-button {
    @apply w-full;

    :root .light & {
        @apply bg-amber-900;
    }

    :root .dark & {
        @apply bg-teal-900;
    }
}
```