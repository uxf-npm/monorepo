const postcssConfig = {
    plugins: [
        require("postcss-focus-visible"),
        require("postcss-import"),
        require("tailwindcss/nesting"),
        require("tailwindcss"),
        require("autoprefixer"),
        require("postcss-pxtorem")({
            minPixelValue: 1,
            propList: ["*"],
        }),
    ],
};

module.exports = postcssConfig;
