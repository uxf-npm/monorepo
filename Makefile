clean:
	git clean -fx

dev:
	npm run dev

lint:
	npm run eslint:check

test: lint typecheck

typecheck:
	npm run typecheck
