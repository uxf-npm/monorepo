#!/bin/sh

# stop on first error
set -e

if [ -z "$(git status --porcelain)" ]; then
  echo "Publishing process started"
else
  echo "🚫 You have uncommited changes in git repository."
  exit 1
fi

echo "🔵 Removing git ignored files from repository"
git clean -fx

oldPackageVersion=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g')

echo "🔵 Setting new version to main package.json"
npm version prerelease --preid=beta --workspaces --legacy-peer-deps
npm version prerelease --preid=beta --no-commit-hooks --no-git-tag-version

newPackageVersion=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[", ]//g')

rm -rf ./packages/ui/css/ && mkdir ./packages/ui/css/
cp ./packages/ui/**/*.css ./packages/ui/css/

sed -i.bu "s/\"@uxf\/ui\": \"\^1\.0\.0-beta\.[0-9]\+\",/\"@uxf\/ui\": \"^$newPackageVersion\",/g" ./packages/form/package.json

git add .
git commit -m "$newPackageVersion"
git tag $newPackageVersion
git push -o ci.skip
git push origin $newPackageVersion -o ci.skip

# TODO @vejvis
if [ -z "$(git status --porcelain)" ]; then
  echo "Git is clean"
else
  echo "🚫 Uncommited changes TODO @vejvis"
  exit 1
fi

echo "🔵 Build and publish npm package"
npm run build
npm publish --workspaces --tag=beta


