module.exports = {
    extends: "@uxf/eslint-config",
    parser: "@typescript-eslint/parser",
    parserOptions: {
        project: "tsconfig.eslint.json",
        sourceType: "module",
    },
    rules: {
        "react/destructuring-assignment": ["error", "never"],
        "react/jsx-curly-brace-presence": ["error", { props: "never", children: "never", propElementValues: "always" }],
        "jsx-a11y/anchor-is-valid": "off", // TODO co s timhle? chyby v linku v index.ts
        "jsx-a11y/no-static-element-interactions": "off",
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/no-noninteractive-element-interactions": "off",
    },
};
