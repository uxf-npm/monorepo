import "./generated.css";
import { defaultConfig, StorybookContextProvider } from "@uxf/ui/utils/storybook-config";
import { UiContextProvider } from "@uxf/ui/context";
import { getProviderConfig } from "@uxf/ui/_private-utils/get-provider-config";
import { FlashMessages } from "@uxf/ui/flash-messages/flash-messages";
import { getFlashMessagesRef } from "@uxf/ui/flash-messages/flash-messages-service";
import "dayjs/locale/cs";
import { locale } from "dayjs";

locale("cs");

export const decorators = [
    (Story) => (
        <StorybookContextProvider config={defaultConfig}>
            <UiContextProvider value={getProviderConfig()}>
                <Story />
                <FlashMessages ref={getFlashMessagesRef()} />
            </UiContextProvider>
        </StorybookContextProvider>
    ),
];

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {},
};
