const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = {
    stories: ["../packages/**/*.stories.@(ts|tsx)"],
    staticDirs: ["./static-assets"],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "@storybook/addon-interactions",
        "@storybook/addon-storysource",
        {
            name: "@storybook/addon-postcss",
            options: {
                postcssLoaderOptions: {
                    implementation: require("postcss"),
                },
            },
        },
    ],
    framework: "@storybook/react",
    webpackFinal: async (config) => {
        config.resolve.plugins = [new TsconfigPathsPlugin()];
        return config;
    },
};
