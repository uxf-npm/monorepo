/** @type {import('@uxf/icons-generator/types').Config} */
const iconsConfig = {
    mode: "simple",
    typescript: true,
    configDirectory: "/packages/ui/config/",
    generatedDirectory: "/.storybook/static-assets/icons-generated/",
    spriteFileName: "_icon-sprite.svg",
    typeName: "IconsType",
    moduleDefinition: {
        moduleName: "@uxf/ui/icon/theme",
        typeName: "IconsSet",
        format: "interface",
    },
    icons: require("./packages/ui/utils/icons-config"),
};

module.exports = iconsConfig;
