const stylelintConfig = {
    customSyntax: "postcss-syntax",
    extends: ["stylelint-config-standard", "stylelint-config-tailwindcss", "stylelint-use-nesting"],
    plugins: ["stylelint-use-nesting"],
    rules: {
        "at-rule-no-unknown": [true, { ignoreAtRules: ["tailwind", "apply", "layer"] }],
        "csstools/use-nesting": true,
        "no-descending-specificity": null,
        "selector-type-no-unknown": null,
        indentation: 4,
        "selector-class-pattern": null,
        "property-no-vendor-prefix": null,
    },
};

module.exports = stylelintConfig;
