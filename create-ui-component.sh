#!/bin/bash

COMPONENT=$1

# name is required
if [ -z $COMPONENT ]
then
  echo "Component name is required."
  exit
fi

DIRECTORY="./packages/ui/$COMPONENT"


# check if component already exists
if [ -d $DIRECTORY ]
then
  echo "Component already exists."
  exit
fi

# file names
FILE_INDEX="$DIRECTORY/index.tsx"
FILE_CSS="$DIRECTORY/$COMPONENT.css"
FILE_STORY="$DIRECTORY/$COMPONENT.stories.tsx"
FILE_COMPONENT="$DIRECTORY/$COMPONENT.tsx"

# component first upper case
COMPONENTFC=`echo $COMPONENT|sed -r 's/^(.)|-(.)/\U\1\U\2/g'`

echo "Create component: $COMPONENT"
echo "Create component: $COMPONENTFC"
echo "- directory: $DIRECTORY"
echo "- file index: $FILE_INDEX"
echo "- file css: $FILE_CSS"
echo "- file story: $FILE_STORY"
echo "- file component: $FILE_COMPONENT"

mkdir -p $DIRECTORY

# STYLE
echo ".uxf-$COMPONENT {
    @apply bg-red-300;
}
" > $FILE_CSS

# COMPONENT
echo "function $COMPONENTFC() {
    return (
        <div className=\"uxf-$COMPONENT\">
            xx
        </div>
    );
}

export default $COMPONENTFC;
" > $FILE_COMPONENT;

# INDEX
echo "export { default as $COMPONENTFC } from \"./$COMPONENT\";
" > $FILE_INDEX

# STORY
echo "import { $COMPONENTFC } from \"./index\";

export default {
    title: \"UI/$COMPONENTFC\",
    component: $COMPONENTFC,
};

export function Default() {
    return (
        <div className=\"space-y-2\">
            <$COMPONENTFC/>
        </div>
    );
}
" > $FILE_STORY