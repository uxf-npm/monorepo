import { Button } from "@uxf/ui/button";
import { HexColor } from "@uxf/ui/types";
import React from "react";
import { Control } from "react-hook-form";
import { Form } from "../storybook/form";
import { ColorRadioGroup } from "./index";

export default {
    title: "Form/ColorRadioGroup",
    component: ColorRadioGroup,
};

const options: { value: HexColor; label: string }[] = [
    {
        value: "#ff0000",
        label: "Red",
    },
    {
        value: "#00ff00",
        label: "Green",
    },
    {
        value: "#ff00ff",
        label: "Purple",
    },
    {
        value: "#000000",
        label: "Red",
    },
    {
        value: "#ffff00",
        label: "Green",
    },
    {
        value: "#00ffff",
        label: "Purple",
    },
    {
        value: "#cccccc",
        label: "Red",
    },
    {
        value: "#3b1298",
        label: "Green",
    },
    {
        value: "#0000ff",
        label: "Purple",
    },
];

export function Default() {
    const storyFormColorRadioGroup = (control: Control) => (
        <div className="space-y-4">
            <ColorRadioGroup
                control={control}
                id="color-radio-group"
                label="Color radio group form"
                name="color-radio-group"
                options={options}
            />
            <ColorRadioGroup
                control={control}
                id="color-radio-group"
                isDisabled
                label="Color radio group form disabled"
                name="color-radio-group-disabled"
                options={options}
            />
            <ColorRadioGroup
                control={control}
                helperText="Choose one option"
                id="color-radio-group"
                label="Color radio group form with helper text"
                name="color-radio-group-helper-text"
                options={options}
            />
            <ColorRadioGroup
                control={control}
                id="color-radio-group"
                label="Color radio group form with variant button"
                name="color-radio-group-button"
                options={options}
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormColorRadioGroup(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormColorRadioGroup(control)}</div>
                </div>
            )}
        </Form>
    );
}
