import React from "react";
import { Form } from "../storybook/form";
import { Textarea } from "./textarea";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/Textarea",
    component: Textarea,
};

export function Default() {
    const storyFormTextAreas = (control: Control) => (
        <div className="space-y-4">
            <Textarea
                label="Textarea"
                name="textarea"
                control={control}
                placeholder="placeholder"
                form="form-textarea"
            />
            <Textarea
                label="Textarea required"
                name="textarea-required"
                control={control}
                placeholder="placeholder"
                form="form-textarea"
                isRequired
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormTextAreas(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormTextAreas(control)}</div>
                </div>
            )}
        </Form>
    );
}
