import { Textarea as UITextarea, TextareaProps as UITextareaProps } from "@uxf/ui/textarea";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler, useCallback } from "react";

export type TextareaProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<UITextareaProps, "isInvalid" | "name" | "onChange" | "value"> & {
        requiredMessage?: string;
    };

export function Textarea<FormData extends Record<string, any>>(props: TextareaProps<FormData>) {
    const { field, fieldState } = useController<FormData>({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur = useCallback<FocusEventHandler<HTMLTextAreaElement>>(
        (e) => {
            field.onBlur();
            props.onBlur?.(e);
        },
        [field, props],
    );

    return (
        <UITextarea
            autoComplete={props.autoComplete}
            className={props.className}
            disableAutoHeight={props.disableAutoHeight}
            form={props.form}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            isDisabled={props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            maxLength={props.maxLength}
            minLength={props.minLength}
            name={field.name}
            onBlur={onBlur}
            onChange={field.onChange}
            onFocus={props.onFocus}
            placeholder={props.placeholder}
            ref={field.ref}
            rows={props.rows}
            value={field.value || ""}
        />
    );
}
