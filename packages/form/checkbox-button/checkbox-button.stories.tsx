import React from "react";
import { Form } from "../storybook/form";
import { CheckboxButton } from "./checkbox-button";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/CheckboxButton",
    component: CheckboxButton,
};

export function Default() {
    const storyFormCheckboxes = (control: Control) => (
        <div className="flex flex-col space-y-2">
            <CheckboxButton label="CheckboxButton form" name="checkbox-button1" control={control} />
            <CheckboxButton
                label="CheckboxButton form required"
                name="checkbox-button-required"
                control={control}
                isRequired
            />
            <CheckboxButton
                label="CheckboxButton form disabled"
                name="checkbox-button-disabled"
                control={control}
                isDisabled
            />
            <CheckboxButton
                label="CheckboxButton form readOnly"
                name="checkbox-button-read-only"
                control={control}
                isReadOnly
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormCheckboxes(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormCheckboxes(control)}</div>
                </div>
            )}
        </Form>
    );
}
