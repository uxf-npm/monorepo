import { srOnly } from "@uxf/styles/mixins/sr-only";
import React, { ReactNode, useId, useMemo } from "react";
import {
    Control,
    DefaultValues,
    FieldValues,
    SubmitHandler,
    useForm,
    UseFormReset,
    UseFormReturn,
    ValidationMode,
} from "react-hook-form";

export type FormSubmitHandler<TFieldValues extends FieldValues> = (
    values: TFieldValues,
    reset: UseFormReset<TFieldValues>,
) => Promise<unknown>;

export type FormControl<TFieldValues extends FieldValues> = Control<TFieldValues> & { formId: string };

export interface FormProps<TFieldValues extends FieldValues> {
    children: (control: FormControl<TFieldValues>, formApi: UseFormReturn<TFieldValues>) => ReactNode;
    defaultValues?: DefaultValues<TFieldValues>;
    mode?: keyof ValidationMode;
    onSubmit?: FormSubmitHandler<TFieldValues>;
}

export function Form<TFieldValues extends FieldValues>(props: FormProps<TFieldValues>) {
    const formId = useId();

    const formApi = useForm<TFieldValues>({
        mode: props.mode ?? "onSubmit",
        defaultValues: props.defaultValues,
    });

    const onSubmit: SubmitHandler<TFieldValues> = async (values) => {
        if (props.onSubmit) {
            await props.onSubmit(values, formApi.reset);
        }
    };

    const control = useMemo(() => Object.assign(formApi.control, { formId }), [formApi.control, formId]);

    return (
        <form id={formId} noValidate onSubmit={formApi.handleSubmit(onSubmit)}>
            {props.children(control, formApi)}
            {props.onSubmit && <input style={srOnly} type="submit" />}
        </form>
    );
}
