import { Button } from "@uxf/ui/button";
import React from "react";
import { TextInput } from "../text-input";
import { Form } from "./form";

export default {
    title: "Form/Form",
    component: Form,
};

export function Default() {
    return (
        // eslint-disable-next-line no-console
        <Form<{ default: string }> onSubmit={async (values) => console.log(values)}>
            {(control) => (
                <>
                    <div className="light mb-4 space-y-4 p-4">
                        <TextInput label="Default input" name="default" control={control} isRequired />
                        <Button type="submit">Submit</Button>
                    </div>
                    <div className="dark space-y-4 bg-gray-900 p-4">
                        <TextInput label="Default input" name="default" control={control} isRequired />
                        <Button type="submit">Submit</Button>
                    </div>
                </>
            )}
        </Form>
    );
}
