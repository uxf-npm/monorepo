import React from "react";
import { TextInput } from "./text-input";
import { Form } from "../storybook/form";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/TextInput",
    component: TextInput,
};

export function Default() {
    const storyFormTextInputs = (control: Control) => (
        <div className="space-y-4">
            <TextInput placeholder="Zadejte ..." label="Default input" name="default" control={control} />
            <TextInput placeholder="Zadejte ..." label="Required input" name="required" control={control} isRequired />
            <TextInput
                placeholder="Zadejte ..."
                label="E-mail required input"
                name="email"
                type="email"
                control={control}
                isRequired
            />
            <TextInput
                placeholder="Zadejte ..."
                label="Phone required input"
                name="phone"
                type="tel"
                control={control}
                isRequired
            />
            <TextInput
                placeholder="Zadejte ..."
                label="Password input"
                name="password"
                type="password"
                control={control}
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormTextInputs(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormTextInputs(control)}</div>
                </div>
            )}
        </Form>
    );
}
