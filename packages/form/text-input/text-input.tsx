import { TextInput as UITextInput, TextInputProps as UITextInputProps } from "@uxf/ui/text-input";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler } from "react";

export type TextInputProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<
        UITextInputProps,
        "isInvalid" | "max" | "min" | "pattern" | "step" | "name" | "onChange" | "value" | "type"
    > & {
        requiredMessage?: string;
    } & (
        | { type: "email"; invalidEmailMessage?: string; invalidPhoneMessage?: never }
        | { type: "tel"; invalidEmailMessage?: never; invalidPhoneMessage?: string }
        | {
              type?: "password" | "search" | "text" | "url";
              invalidEmailMessage?: never;
              invalidPhoneMessage?: never;
          }
    );

const EMAIL_REGEXP =
    /^(([^<>()[\]\\.,;:\s@À-ÖÙ-öù-ÿĀ-žḀ-ỿ"]+(\.[^<>()[\]\\.,;:\s@À-ÖÙ-öù-ÿĀ-žḀ-ỿ"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PHONE_REGEXP = /^(\+)?[\d\s]*$/;

export function TextInput<FormData extends FieldValues>(props: TextInputProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            pattern:
                props.type === "email"
                    ? {
                          value: EMAIL_REGEXP,
                          message: props.invalidEmailMessage ?? "E-mail by měl být ve\xa0formátu: info@email.cz",
                      }
                    : props.type === "tel"
                    ? {
                          value: PHONE_REGEXP,
                          message: props.invalidPhoneMessage ?? "Zadaný telefon není ve správném formátu",
                      }
                    : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    return (
        <UITextInput
            autoComplete={props.autoComplete}
            className={props.className}
            enterKeyHint={props.enterKeyHint}
            form={props.form}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            inputMode={props.inputMode}
            isDisabled={props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            leftAddon={props.leftAddon}
            leftElement={props.leftElement}
            maxLength={props.maxLength}
            minLength={props.minLength}
            name={field.name}
            onBlur={onBlur}
            onChange={field.onChange}
            onFocus={props.onFocus}
            onKeyDown={props.onKeyDown}
            placeholder={props.placeholder}
            ref={field.ref}
            rightAddon={props.rightAddon}
            rightElement={props.rightElement}
            size={props.size}
            type={props.type}
            value={field.value ?? ""}
            variant={props.variant}
        />
    );
}
