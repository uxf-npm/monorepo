import { Button } from "@uxf/ui/button";
import { MultiComboboxOption } from "@uxf/ui/mutli-combobox";
import React from "react";
import { Control } from "react-hook-form";
import { Form } from "../storybook/form";
import { MultiCombobox } from "./multi-combobox";

export default {
    title: "Form/MultiCombobox",
    component: MultiCombobox,
};

const options: MultiComboboxOption[] = [
    { id: "one", label: "Option one", color: "red" },
    { id: "two", label: "Option two", color: "blue" },
    { id: "three", label: "Option three", color: "green" },
    { id: "four", label: "Option four" },
    { id: "five", label: "Option five" },
    { id: "six", label: "Option six" },
];

export function Default() {
    const storyFormComboboxes = (control: Control) => (
        <div className="space-y-8">
            <MultiCombobox
                control={control}
                helperText="Začněte psát pro vyhledávání..."
                id="form-combobox"
                label="Combobox form"
                name="combobox1"
                options={options}
                placeholder="Placeholder"
            />
            <MultiCombobox
                control={control}
                helperText="Začněte psát pro vyhledávání..."
                id="form-combobox"
                label="Combobox with checkboxes"
                name="combobox-with-checkboxes"
                options={options}
                placeholder="Placeholder"
                withCheckboxes
            />
            <MultiCombobox
                control={control}
                helperText="Začněte psát pro vyhledávání..."
                id="form-combobox"
                isRequired
                label="Combobox form required"
                name="combobox-required"
                options={options}
                placeholder="Placeholder"
            />
            <MultiCombobox
                control={control}
                id="form-combobox"
                isDisabled
                label="Combobox form disabled"
                name="combobox-disabled"
                options={options}
                placeholder="Placeholder"
            />
            <MultiCombobox
                control={control}
                dropdownPlacement="top"
                id="form-combobox"
                label="Combobox form with dropdown top"
                name="select-dropdown-top"
                options={options}
                placeholder="Placeholder"
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormComboboxes(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormComboboxes(control)}</div>
                </div>
            )}
        </Form>
    );
}
