import React from "react";
import { NumberInput } from "./number-input";
import { Form } from "../storybook/form";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/NumberInput",
    component: NumberInput,
};

export function Default() {
    const storyFormNumberInputs = (control: Control) => (
        <div className="light mb-4 space-y-4 p-4">
            <NumberInput label="Default input" name="default" control={control} />
            <NumberInput label="Required input" name="required" control={control} isRequired />
            <NumberInput label="Decímal input" name="decimal" decimalPlaces={3} control={control} />
            <NumberInput label="max50 input" name="max50" max={50} control={control} />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormNumberInputs(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormNumberInputs(control)}</div>
                </div>
            )}
        </Form>
    );
}
