import { TextInput as UITextInput, TextInputProps as UITextInputProps } from "@uxf/ui/text-input";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler, KeyboardEventHandler } from "react";

export type NumberInputProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<
        UITextInputProps,
        "inputMode" | "isInvalid" | "maxLength" | "minLength" | "name" | "onChange" | "value" | "type"
    > & {
        decimalPlaces?: number;
        requiredMessage?: string;
    };

export function NumberInput<FormData extends FieldValues>(props: NumberInputProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            max:
                typeof props.max === "number" && !isNaN(props.max)
                    ? {
                          value: props.max,
                          message: `Hodnota musí být nižší než ${props.max}.`,
                      }
                    : undefined,
            min:
                typeof props.min === "number" && !isNaN(props.min)
                    ? {
                          value: props.min,
                          message: `Hodnota musí být vyšší než ${props.min}.`,
                      }
                    : undefined,
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    const onKeyDown: KeyboardEventHandler<HTMLInputElement> = (event) => {
        if (event.key === "e") {
            event.preventDefault();
        }
        props.onKeyDown?.(event);
    };

    return (
        <UITextInput
            autoComplete={props.autoComplete}
            className={props.className}
            enterKeyHint={props.enterKeyHint}
            form={props.form}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            inputMode={props.decimalPlaces ? "decimal" : "numeric"}
            isDisabled={props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            leftAddon={props.leftAddon}
            leftElement={props.leftElement}
            name={field.name}
            onBlur={onBlur}
            onChange={field.onChange}
            onFocus={props.onFocus}
            onKeyDown={onKeyDown}
            pattern={props.pattern}
            placeholder={props.placeholder}
            step={props.decimalPlaces ? Number(0).toFixed(props.decimalPlaces - 1) + "1" : props.step}
            ref={field.ref}
            rightAddon={props.rightAddon}
            rightElement={props.rightElement}
            size={props.size}
            type="number"
            value={field.value ?? ""}
            variant={props.variant}
        />
    );
}
