import React from "react";
import { DatePickerInput } from "./date-picker-input";
import { Form } from "../storybook/form";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/DatePickerInput",
    component: DatePickerInput,
};

export function Default() {
    const storyFormDatePickers = (control: Control) => (
        <div className="space-y-4">
            <DatePickerInput label="Default date picker" name="default" control={control} />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormDatePickers(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 text-white lg:w-1/2">
                        {storyFormDatePickers(control)}
                    </div>
                </div>
            )}
        </Form>
    );
}
