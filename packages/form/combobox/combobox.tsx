import { Combobox as UICombobox, ComboboxProps as UIComboboxProps } from "@uxf/ui/combobox";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler } from "react";

export type ComboProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<UIComboboxProps, "isInvalid" | "name" | "onChange" | "value"> & {
        requiredMessage?: string;
    };

export function Combobox<FormData extends Record<string, any>>(props: ComboProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    return (
        <UICombobox
            className={props.className}
            dropdownPlacement={props.dropdownPlacement}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            isDisabled={props.options.length === 0 || props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            name={field.name}
            onBlur={onBlur}
            onChange={(value) => field.onChange(value)}
            onFocus={props.onFocus}
            options={props.options}
            placeholder={props.placeholder}
            ref={field.ref}
            value={field.value}
        />
    );
}
