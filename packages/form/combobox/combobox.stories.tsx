import React from "react";
import { Form } from "../storybook/form";
import { Combobox } from "./combobox";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/Combobox",
    component: Combobox,
};

const options = [
    { id: "one", label: "Option one" },
    { id: "two", label: "Option two" },
    { id: "three", label: "Option three" },
];

export function Default() {
    const storyFormComboboxes = (control: Control) => (
        <div className="space-y-8">
            <Combobox
                label="Combobox form"
                name="combobox1"
                control={control}
                placeholder="Placeholder"
                options={options}
                id="form-combobox"
                helperText="Začněte psát pro vyhledávání..."
            />
            <Combobox
                label="Combobox form required"
                name="combobox-required"
                control={control}
                placeholder="Placeholder"
                options={options}
                id="form-combobox"
                isRequired
                helperText="Začněte psát pro vyhledávání..."
            />
            <Combobox
                label="Combobox form disabled"
                name="combobox-disabled"
                control={control}
                placeholder="Placeholder"
                options={options}
                id="form-combobox"
                isDisabled
            />
            <Combobox
                label="Combobox form with dropdown top"
                dropdownPlacement="top"
                name="select-dropdown-top"
                control={control}
                placeholder="Placeholder"
                options={options}
                id="form-combobox"
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormComboboxes(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormComboboxes(control)}</div>
                </div>
            )}
        </Form>
    );
}
