import React from "react";
import { Select } from "./select";
import { Form } from "../storybook/form";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/Select",
    component: Select,
};

const options = [
    { id: "one", label: "Option one" },
    { id: "two", label: "Option two" },
    { id: "three", label: "Option three" },
];

export function Default() {
    const storyFormSelects = (control: Control) => (
        <div className="space-y-4">
            <Select
                label="Select form"
                name="select"
                control={control}
                placeholder="placeholder"
                options={options}
                id="form-select"
            />
            <Select
                label="Select form disabled"
                name="select-disabled"
                control={control}
                placeholder="placeholder"
                options={options}
                id="form-select"
                isDisabled
            />
            <Select
                label="Select form with helper text"
                name="select-helper-text"
                control={control}
                placeholder="placeholder"
                options={options}
                id="form-select"
                helperText="Choose one option"
            />
            <Select
                label="Select form with dropdown top"
                dropdownPlacement="top"
                name="select-dropdown-top"
                control={control}
                placeholder="placeholder"
                options={options}
                id="form-select"
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormSelects(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormSelects(control)}</div>
                </div>
            )}
        </Form>
    );
}
