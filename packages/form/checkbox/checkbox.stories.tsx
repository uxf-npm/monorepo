import React from "react";
import { Form } from "../storybook/form";
import { Checkbox } from "./checkbox";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/Checkbox",
    component: Checkbox,
};

export function Default() {
    const storyFormCheckboxes = (control: Control) => (
        <div className="space-y-2">
            <Checkbox label="Checkbox form" name="checkbox1" control={control} />
            <Checkbox label="Checkbox form required" name="checkbox-required" control={control} isRequired />
            <Checkbox label="Checkbox form disabled" name="checkbox-disabled" control={control} isDisabled />
            <Checkbox label="Checkbox form readOnly" name="checkbox-readonly" control={control} isReadOnly />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormCheckboxes(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormCheckboxes(control)}</div>
                </div>
            )}
        </Form>
    );
}
