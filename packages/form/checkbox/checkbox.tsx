import { Checkbox as UICheckbox, CheckboxProps as UICheckboxProps } from "@uxf/ui/checkbox";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler } from "react";

export type ComboProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<UICheckboxProps, "isInvalid" | "name" | "onChange" | "value"> & {
        requiredMessage?: string;
    };

export function Checkbox<FormData extends Record<string, any>>(props: ComboProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    return (
        <UICheckbox
            className={props.className}
            isDisabled={props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            name={field.name}
            onBlur={onBlur}
            onChange={(value) => field.onChange(value)}
            onFocus={props.onFocus}
            ref={field.ref}
            value={field.value}
        />
    );
}
