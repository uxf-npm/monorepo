import React from "react";
import { Form } from "../storybook/form";
import { Toggle } from "./toggle";
import { Control } from "react-hook-form";
import { Button } from "@uxf/ui/button";

export default {
    title: "Form/Toggle",
    component: Toggle,
};

export function Default() {
    const storyFormToggles = (control: Control) => (
        <div className="space-y-2">
            <Toggle label="Toggle form" name="toggle1" control={control} />
            <Toggle label="Toggle form required" name="toggle-required" control={control} isRequired />
            <Toggle label="Toggle form disabled" name="toggle-disabled" control={control} isDisabled />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormToggles(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormToggles(control)}</div>
                </div>
            )}
        </Form>
    );
}
