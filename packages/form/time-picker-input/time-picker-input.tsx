import {
    TimePickerInput as UITimePickerInput,
    TimePickerInputProps as UITimePickerInputProps,
} from "@uxf/ui/time-picker-input";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";
import React, { FocusEventHandler } from "react";

export type TimePickerInputProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<UITimePickerInputProps, "isInvalid" | "max" | "min" | "pattern" | "step" | "name" | "onChange" | "value"> & {
        requiredMessage?: string;
    };

export function TimePickerInput<FormData extends FieldValues>(props: TimePickerInputProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    return (
        <UITimePickerInput
            className={props.className}
            form={props.form}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            isDisabled={props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            leftAddon={props.leftAddon}
            leftElement={props.leftElement}
            name={field.name}
            onBlur={onBlur}
            onChange={field.onChange}
            onFocus={props.onFocus}
            placeholder={props.placeholder}
            ref={field.ref}
            rightAddon={props.rightAddon}
            rightElement={props.rightElement}
            size={props.size}
            value={field.value}
            variant={props.variant}
        />
    );
}
