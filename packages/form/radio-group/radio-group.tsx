import { RadioGroup as UIRadioGroup, RadioGroupProps as UIRadioGroupProps } from "@uxf/ui/radio-group";
import React, { FocusEventHandler } from "react";
import { FieldValues, useController, UseControllerProps } from "react-hook-form";

export type RadioGroupProps<FormData extends FieldValues> = UseControllerProps<FormData> &
    Omit<UIRadioGroupProps, "isInvalid" | "name" | "onChange" | "value"> & {
        requiredMessage?: string;
    };

export function RadioGroup<FormData extends Record<string, any>>(props: RadioGroupProps<FormData>) {
    const { field, fieldState } = useController({
        control: props.control,
        defaultValue: props.defaultValue,
        name: props.name,
        rules: {
            required: props.isRequired ? props.requiredMessage || "Toto pole je povinné" : undefined,
            ...(props.rules ?? {}),
        },
        shouldUnregister: props.shouldUnregister,
    });

    const onBlur: FocusEventHandler<HTMLInputElement> = (event) => {
        field.onBlur();
        props.onBlur?.(event);
    };

    return (
        <UIRadioGroup
            className={props.className}
            forceColumn={props.forceColumn}
            helperText={fieldState.error?.message ?? props.helperText}
            hiddenLabel={props.hiddenLabel}
            id={props.id}
            isDisabled={props.options.length === 0 || props.isDisabled}
            isInvalid={!!fieldState.error}
            isReadOnly={props.isReadOnly}
            isRequired={props.isRequired}
            label={props.label}
            name={field.name}
            onBlur={onBlur}
            onChange={(value: any) => field.onChange(value)}
            onFocus={props.onFocus}
            options={props.options}
            ref={field.ref}
            style={props.style}
            value={field.value}
            variant={props.variant}
        />
    );
}
