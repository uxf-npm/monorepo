import { Button } from "@uxf/ui/button";
import React from "react";
import { Control } from "react-hook-form";
import { Form } from "../storybook/form";
import { RadioGroup } from "./index";

export default {
    title: "Form/RadioGroup",
    component: RadioGroup,
};

const options = [
    {
        value: "1",
        label: "Radio one",
    },
    {
        value: "2",
        label: "Radio two",
    },
    {
        value: "3",
        label: "Radio three-sixty",
    },
];

export function Default() {
    const storyFormRadioGroup = (control: Control) => (
        <div className="space-y-4">
            <RadioGroup
                control={control}
                id="radio-group"
                label="Radio group form"
                name="radio-group"
                options={options}
            />
            <RadioGroup
                control={control}
                id="radio-group"
                isDisabled
                label="Radio group form disabled"
                name="radio-group-disabled"
                options={options}
            />
            <RadioGroup
                control={control}
                helperText="Choose one option"
                id="radio-group"
                label="Radio group form with helper text"
                name="radio-group-helper-text"
                options={options}
            />
            <RadioGroup
                control={control}
                id="radio-group"
                label="Radio group form with variant button"
                name="radio-group-button"
                options={options}
                variant="radioButton"
            />
            <Button type="submit">Submit</Button>
        </div>
    );

    return (
        <Form>
            {({ control }) => (
                <div className="flex flex-col lg:flex-row">
                    <div className="light space-y-2 p-20 lg:w-1/2">{storyFormRadioGroup(control)}</div>
                    <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyFormRadioGroup(control)}</div>
                </div>
            )}
        </Form>
    );
}
