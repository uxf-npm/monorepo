import { Button } from "@uxf/ui/button";
import { useForm, UseFormReturn } from "react-hook-form";
import React, { ReactNode } from "react";
import { FormDataPrinter } from "./form-data-printer";

interface FormProps {
    children: (form: UseFormReturn) => ReactNode;
}

export function Form(props: FormProps) {
    const form = useForm();

    return (
        // eslint-disable-next-line no-console
        <form onSubmit={form.handleSubmit(console.log)}>
            {props.children(form)}
            <div className="my-4">
                <FormDataPrinter control={form.control} />
            </div>
            <Button type="submit">Submit</Button>
        </form>
    );
}
