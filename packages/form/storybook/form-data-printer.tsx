import { useWatch } from "react-hook-form";
import React from "react";

interface FormDataPrinterProps {
    control: any;
}

export function FormDataPrinter(props: FormDataPrinterProps) {
    const data = useWatch({ control: props.control });
    return <pre className="bg-gray-100 p-2 text-sm">{JSON.stringify(data, null, "    ")}</pre>;
}
