import React from "react";
import { ListItem } from "./index";
import { Icon } from "../icon";

export default {
    title: "UI/ListItem",
    component: ListItem,
};

export function Default() {
    const storyListItem = (
        <>
            <ListItem endIcon="chevronRight" onClick={() => void null}>
                Jméno příjmení
            </ListItem>
            <div className="border-gray500 border-b" />
            <ListItem endIcon="chevronRight" onClick={() => void null}>
                <div>Avatar</div>
                <div>
                    <div>Karel Vomáčka</div>
                    <div>Uživatel</div>
                </div>
            </ListItem>
            <div className="border-gray500 border-b" />
            <ListItem onClick={() => void null}>No end icon</ListItem>
            <div className="border-gray500 border-b" />
            <ListItem
                endElement={<Icon name="camera" className="text-success-500" size={24} />}
                onClick={() => void null}
            >
                End element
            </ListItem>
            <ListItem
                endElement={<Icon name="camera" className="text-success-500" size={24} />}
                endIcon="chevronRight"
                onClick={() => void null}
            >
                End element and end icon
            </ListItem>
        </>
    );

    return (
        <div className="flex">
            <div className="light grow gap-4 p-4">{storyListItem}</div>
            <div className="dark grow gap-4 bg-gray-900 p-4 text-gray-200">{storyListItem}</div>
        </div>
    );
}
