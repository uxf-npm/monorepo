import { forwardRef } from "@uxf/core/utils/forwardRef";
import { UseAnchorProps, useAnchorProps } from "@uxf/core/hooks/useAnchorProps";
import { cx } from "@uxf/core/utils/cx";
import React, { AnchorHTMLAttributes, ReactNode } from "react";
import { IconName } from "../icon/types";
import { Icon } from "../icon";

export interface ListItemProps extends Omit<AnchorHTMLAttributes<HTMLAnchorElement>, "type">, UseAnchorProps {
    endIcon?: IconName;
    endElement?: ReactNode;
}

export const ListItem = forwardRef<HTMLAnchorElement, ListItemProps>("ListItem", (props, ref) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { className, children, endElement, endIcon, ...restProps } = props;

    const anchorProps = useAnchorProps({
        ...restProps,
        className: cx("uxf-list-item", className),
    });

    return (
        <a ref={ref} {...anchorProps}>
            <span className="uxf-list-item__inner">{children}</span>
            {endElement && <span className="uxf-list-item__end-element-wrapper">{endElement}</span>}
            {endIcon && (
                <span className="uxf-list-item__end-icon-wrapper">
                    <Icon name={endIcon} size={24} />
                </span>
            )}
        </a>
    );
});
