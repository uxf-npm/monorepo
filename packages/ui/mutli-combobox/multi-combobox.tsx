import { Placement } from "@floating-ui/react";
import { Combobox as HUICombobox } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { useInputFocus } from "@uxf/core/hooks/useInputFocus";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { slugify } from "@uxf/core/utils/slugify";
import React, { ChangeEventHandler, KeyboardEventHandler, ReactNode, useId, useMemo, useRef, useState } from "react";
import { Checkbox } from "../checkbox";
import { Chip, ChipColor } from "../chip";
import { useDropdown } from "../hooks/use-dropdown";
import { Icon } from "../icon";
import { IconsSet } from "../icon/theme";
import { Label } from "../label";
import { FormControlProps } from "../types";

export type MultiComboboxValue = number | string;

export type MultiComboboxOption<T = MultiComboboxValue> = {
    color?: ChipColor;
    disabled?: boolean;
    id: T;
    label: string;
};

export interface MultiComboboxProps<Value = MultiComboboxValue[], Option = MultiComboboxOption>
    extends FormControlProps<Value | null> {
    className?: string;
    dropdownPlacement?: Placement;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    iconName?: keyof IconsSet;
    id?: string;
    keyExtractor?: (option: Option) => string | number;
    label: string;
    options: Option[];
    placeholder?: string;
    renderOption?: (option: Option) => ReactNode;
    withCheckboxes?: boolean;
}

type MultiComboboxTypeRef = HTMLDivElement;

export const MultiCombobox = forwardRef<MultiComboboxTypeRef, MultiComboboxProps>("MultiCombobox", (props, ref) => {
    const generatedId = useId();
    const id = props.id ?? generatedId;

    const [query, setQuery] = useState("");

    const selectedOptions = props.options.filter((option) => props.value?.includes(option.id));

    const filteredData = props.options.filter(
        (item) =>
            (!props.value?.includes(item.id) || props.withCheckboxes) &&
            (query === "" || slugify(item.label).includes(slugify(query))),
    );

    const innerRef = useRef<MultiComboboxTypeRef>(null);

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const input = useInputFocus(innerRef, props.onBlur, props.onFocus);

    const dropdown = useDropdown(props.dropdownPlacement ?? "bottom", true);

    const stableRef = useMemo(() => composeRefs(innerRef, ref, dropdown.reference), [ref, dropdown.reference]);

    const iconName = props.iconName ?? "chevronDown";

    const handleRemove = (value: MultiComboboxValue) => () => {
        props.onChange?.(props.value?.filter((v) => v !== value) ?? []);
    };

    const handleCheckboxChange = (value: MultiComboboxValue) => (checked: boolean) => {
        if (props.value?.includes(value) && !checked) {
            handleRemove(value)();
            return;
        }

        if (!props.value?.includes(value) && checked) {
            props.onChange?.([...(props.value ?? []), value]);
        }
    };

    const handleInputKeyDown: KeyboardEventHandler<HTMLInputElement> = (event) => {
        if (event.key === "Backspace" && query === "" && selectedOptions.length > 0) {
            handleRemove(selectedOptions[selectedOptions.length - 1].id)();
        }
    };

    const handleInputChange: ChangeEventHandler<HTMLInputElement> = (event) => setQuery(event.target.value);

    const handleComboboxValueChange = (v: MultiComboboxOption[]) => {
        props.onChange?.(v.map((option) => option.id));
        setQuery("");
    };

    return (
        <HUICombobox
            as="div"
            className={cx(
                "uxf-combobox",
                props.isInvalid && CLASSES.IS_INVALID,
                props.isRequired && CLASSES.IS_REQUIRED,
                props.isReadOnly && CLASSES.IS_READONLY,
                props.isDisabled && CLASSES.IS_DISABLED,
                props.className,
            )}
            disabled={props.isDisabled || props.isReadOnly}
            multiple
            onChange={handleComboboxValueChange}
            value={selectedOptions}
        >
            {(renderProps) => (
                <>
                    <HUICombobox.Label
                        as={Label}
                        isHidden={props.hiddenLabel}
                        onClick={props.isDisabled || props.isReadOnly ? undefined : input.focus}
                    >
                        {props.label}
                    </HUICombobox.Label>
                    <HUICombobox.Button
                        as="div"
                        className={cx(
                            "uxf-multi-combobox__button",
                            (renderProps.open || input.focused) && CLASSES.IS_FOCUSED,
                            renderProps.disabled && CLASSES.IS_DISABLED,
                            props.isReadOnly && CLASSES.IS_READONLY,
                            props.isInvalid && CLASSES.IS_INVALID,
                            dropdown.placement === "bottom" && "is-open--bottom",
                            dropdown.placement === "top" && "is-open--top",
                            renderProps.open && CLASSES.IS_OPEN,
                        )}
                        onBlur={input.onBlur}
                        onFocus={input.onFocus}
                        tabIndex={props.isDisabled || props.isReadOnly ? undefined : 0}
                        ref={stableRef}
                        aria-invalid={props.isInvalid}
                        aria-describedby={errorId}
                    >
                        {selectedOptions.map((item) => (
                            <Chip
                                className="mr-2 mt-2"
                                color={item.color}
                                key={item.id}
                                onClose={handleRemove(item.id)}
                                size="large"
                            >
                                {item.label}
                            </Chip>
                        ))}
                        <HUICombobox.Input
                            className={cx("uxf-multi-combobox__input", "mt-2")}
                            onBlur={props.onBlur}
                            onChange={handleInputChange}
                            onKeyDown={handleInputKeyDown}
                            placeholder={props.placeholder}
                            ref={stableRef}
                            type="text"
                            value={query}
                        />
                        <Icon
                            className={cx("uxf-select__button-icon", renderProps.open && "is-open")}
                            name={iconName}
                        />
                    </HUICombobox.Button>
                    <HUICombobox.Options
                        className={cx(
                            "uxf-dropdown",
                            dropdown.placement === "bottom" && "uxf-dropdown--bottom",
                            dropdown.placement === "top" && "uxf-dropdown--top",
                        )}
                        ref={dropdown.floating}
                    >
                        {filteredData.map((option) => {
                            const label = props.renderOption?.(option) ?? option.label;
                            return (
                                <HUICombobox.Option
                                    className={(optionState) =>
                                        cx(
                                            "uxf-dropdown__item",
                                            optionState.active && !props.withCheckboxes && CLASSES.IS_ACTIVE,
                                            optionState.disabled && CLASSES.IS_DISABLED,
                                            optionState.selected && CLASSES.IS_SELECTED,
                                        )
                                    }
                                    key={props.keyExtractor?.(option) ?? option.id}
                                    value={option}
                                >
                                    {(optionState) => (
                                        <>
                                            {props.withCheckboxes ? (
                                                <Checkbox
                                                    isDisabled={optionState.disabled}
                                                    isFocused={optionState.active}
                                                    label={label}
                                                    onChange={handleCheckboxChange(option.id)}
                                                    value={optionState.selected}
                                                />
                                            ) : (
                                                label
                                            )}
                                        </>
                                    )}
                                </HUICombobox.Option>
                            );
                        })}
                    </HUICombobox.Options>
                    {props.helperText && (
                        <div className={cx("uxf-helper-text", props.isInvalid && CLASSES.IS_INVALID)} id={errorId}>
                            {props.helperText}
                        </div>
                    )}
                </>
            )}
        </HUICombobox>
    );
});
