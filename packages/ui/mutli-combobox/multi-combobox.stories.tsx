import React, { useState } from "react";
import ComponentStructureAnalyzer from "../utils/component-structure-analyzer";
import { MultiCombobox, MultiComboboxOption } from "./index";
import { MultiComboboxValue } from "./multi-combobox";

export default {
    title: "UI/MultiCombobox",
    component: MultiCombobox,
};

const options: MultiComboboxOption[] = [
    { id: "one", label: "Option red", color: "red" },
    { id: "two", label: "Option blue", color: "blue" },
    { id: "three", label: "Option green", color: "green" },
    { id: "four", label: "Option four" },
    { id: "five", label: "Option five" },
    { id: "six", label: "Option with diacritics (ěščřžýáíé)" },
];

export function Default() {
    const [values, setValues] = useState<MultiComboboxValue[] | null | undefined>();
    const handleChange = (v: MultiComboboxValue[] | null) => {
        // eslint-disable-next-line no-console
        console.log("Select values: ", v);
        setValues(v);
    };

    const storyComboboxes = (
        <div className="space-y-8">
            <MultiCombobox
                id="multi-combobox-1"
                label="MultiCombobox"
                name="multi-combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={values}
            />
            <MultiCombobox
                id="multi-combobox-1"
                label="MultiCombobox with checkboxes"
                name="multi-combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={values}
                withCheckboxes
            />
            <MultiCombobox
                helperText="Start typing to see options..."
                id="multi-combobox-1"
                label="MultiCombobox with helper text"
                name="multi-combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={values}
            />
            <MultiCombobox
                helperText="Error message"
                id="multi-combobox-1"
                isInvalid
                isRequired
                label="MultiCombobox invalid"
                name="multi-combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={values}
            />
            <MultiCombobox
                dropdownPlacement="top"
                id="multi-combobox-1"
                label="MultiCombobox with dropdown top"
                name="multi-combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={values}
            />
        </div>
    );

    return (
        <div className="flex flex-col lg:flex-row">
            <div className="light space-y-2 p-20 lg:w-1/2">{storyComboboxes}</div>
            <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyComboboxes}</div>
        </div>
    );
}

export function ComponentStructure() {
    const [values, setValues] = useState<MultiComboboxValue[] | null | undefined>(null);
    const handleChange = (v: MultiComboboxValue[] | null) => {
        setValues(v);
    };

    return (
        <ComponentStructureAnalyzer>
            <MultiCombobox
                id="multi-combobox-structure"
                name="multi-combobox"
                label="MultiCombobox with helper text"
                options={options}
                onChange={handleChange}
                value={values}
                helperText="Start typing to see options..."
            />
        </ComponentStructureAnalyzer>
    );
}
