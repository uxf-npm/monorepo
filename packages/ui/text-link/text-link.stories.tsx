import React from "react";
import { TextLink } from "./index";

export default {
    title: "UI/TextLink",
    component: TextLink,
};

export function Default() {
    return (
        <div className="flex">
            <div className="light flex gap-4 p-20">
                <p>
                    Pokračujte <TextLink onClick={() => void null}>tímto odkazem</TextLink>
                </p>
            </div>
            <div className="dark flex gap-4 bg-gray-900 p-20 text-gray-200">
                <p>
                    Pokračujte <TextLink onClick={() => void null}>tímto odkazem</TextLink>
                </p>
            </div>
        </div>
    );
}
