import { forwardRef } from "@uxf/core/utils/forwardRef";
import { UseAnchorProps, useAnchorProps } from "@uxf/core/hooks/useAnchorProps";
import { cx } from "@uxf/core/utils/cx";
import React, { AnchorHTMLAttributes } from "react";

export interface TextLinkProps extends Omit<AnchorHTMLAttributes<HTMLAnchorElement>, "type">, UseAnchorProps {}

export const TextLink = forwardRef<HTMLAnchorElement, TextLinkProps>("TextLink", (props, ref) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { className, children, ...restProps } = props;

    const anchorProps = useAnchorProps({
        ...restProps,
        className: cx("uxf-text-link", className),
    });

    return (
        <a ref={ref} {...anchorProps}>
            {children}
        </a>
    );
});
