import React from "react";
import { snapTest } from "../utils/snap-test";
import { CheckboxButton } from "./checkbox-button";

snapTest("render basic", <CheckboxButton label="label" onChange={() => void null} />);
snapTest("render disabled", <CheckboxButton label="label" isDisabled onChange={() => void null} />);
