import { CheckboxButton } from "./index";
import React, { useState } from "react";

export default {
    title: "UI/CheckboxButton",
    component: CheckboxButton,
};

export function Default() {
    const [checked, setChecked] = useState<boolean>(true);

    return (
        <div className="flex">
            <div className="light flex flex-col gap-4 p-20">
                <CheckboxButton
                    label="Opravdu?"
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
                <CheckboxButton
                    label="Opravdu?"
                    isDisabled
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
                <CheckboxButton
                    label="Opravdu?"
                    isInvalid
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
            </div>
            <div className="dark flex flex-col gap-4 bg-gray-900 p-20">
                <CheckboxButton
                    label="Opravdu?"
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
                <CheckboxButton
                    label="Opravdu?"
                    isDisabled
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
                <CheckboxButton
                    label="Opravdu?"
                    isInvalid
                    onChange={() => {
                        setChecked((prev) => !prev);
                    }}
                    value={checked}
                />
            </div>
        </div>
    );
}
