import { Switch as HUISwitch } from "@headlessui/react";
import React, { CSSProperties, ReactNode } from "react";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";
import { FormControlProps } from "../types";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { Icon } from "../icon";

export interface CheckboxButtonProps extends FormControlProps<boolean> {
    className?: string;
    label: ReactNode;
    style?: Partial<CSSProperties>;
}

export const CheckboxButton = forwardRef<HTMLButtonElement, CheckboxButtonProps>("CheckboxButton", (props, ref) => {
    return (
        <HUISwitch
            checked={props.value}
            className={cx(
                "uxf-checkbox-button",
                props.className,
                props.isDisabled && CLASSES.IS_DISABLED,
                props.isInvalid && CLASSES.IS_INVALID,
                props.isReadOnly && CLASSES.IS_READONLY,
                props.value && CLASSES.IS_SELECTED,
            )}
            disabled={props.isDisabled}
            name={props.name}
            onChange={props.onChange}
            ref={ref}
            style={props.style}
        >
            <span className="uxf-checkbox-button__label">{props.label}</span>
            <span className="uxf-checkbox-button__icon">
                <Icon name="check" />
            </span>
        </HUISwitch>
    );
});
