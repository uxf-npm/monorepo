// this file is generated automatically, do not change anything manually in the contents of this file

export const twZIndex = {
    0: "0",
    1: "1",
    10: "10",
    20: "20",
    30: "30",
    40: "40",
    50: "50",
    auto: "auto",
    focus: "5",
    fixed: "10",
    menu: "25",
    header: "50",
    modal: "100",
    dropdown: "200",
    flashMessage: "250",
    tooltip: "300",
};

export type TwZIndex = typeof twZIndex;
