// this file is generated automatically, do not change anything manually in the contents of this file

export const twFontWeight = {
    thin: "100",
    extralight: "200",
    light: "300",
    normal: "400",
    medium: "500",
    semibold: "600",
    bold: "700",
    extrabold: "800",
    black: "900",
};

export type TwFontWeight = typeof twFontWeight;
