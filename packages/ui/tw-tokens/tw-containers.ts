// this file is generated automatically, do not change anything manually in the contents of this file

export const twContainers = {
    xs: "33.75rem",
    sm: "45rem",
    md: "58rem",
    lg: "64rem",
    xl: "76rem",
};

export type TwContainers = typeof twContainers;
