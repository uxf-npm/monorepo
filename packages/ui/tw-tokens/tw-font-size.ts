// this file is generated automatically, do not change anything manually in the contents of this file

export const twFontSize = {
    xs: ["0.75rem", { lineHeight: "1rem" }],
    sm: ["0.875rem", { lineHeight: "1.25rem" }],
    base: ["1rem", { lineHeight: "1.5rem" }],
    lg: ["1.125rem", { lineHeight: "1.75rem" }],
    xl: ["1.25rem", { lineHeight: "1.75rem" }],
    "2xl": ["1.5rem", { lineHeight: "2rem" }],
    "3xl": ["1.875rem", { lineHeight: "2.25rem" }],
    "4xl": ["2.25rem", { lineHeight: "2.5rem" }],
    "5xl": ["3rem", { lineHeight: "1" }],
    "6xl": ["3.75rem", { lineHeight: "1" }],
    "7xl": ["4.5rem", { lineHeight: "1" }],
    "8xl": ["6rem", { lineHeight: "1" }],
    "9xl": ["8rem", { lineHeight: "1" }],
    desktopH1: ["6rem", 1.1666666666666667],
    desktopH2: ["3.75rem", 1.2],
    desktopH3: ["3rem", 1.3333333333333333],
    desktopH4: ["2.125rem", 1.1764705882352942],
    desktopH5: ["1.5rem", 1.3333333333333333],
    desktopH6: ["1rem", 1.375],
    body: ["1rem", 1.5],
    body2: ["0.875rem", 1.4285714285714286],
    button: ["1rem", 1.5],
    caption: ["0.75rem", 1.3333333333333333],
    medium: ["1rem", 1.5],
    medium2: ["0.875rem", 1.4285714285714286],
    overline: ["0.625rem", 1.6],
};

export type TwFontSize = typeof twFontSize;
