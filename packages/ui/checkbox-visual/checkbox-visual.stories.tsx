import { CheckboxVisual } from "./index";
import React, { useState } from "react";

export default {
    title: "UI/CheckboxVisual",
    component: CheckboxVisual,
};

export function Default() {
    const [checked, setChecked] = useState<boolean>(true);

    const checkboxes = (
        <>
            <CheckboxVisual onChange={() => setChecked((prev) => !prev)} value={checked} />
            <CheckboxVisual isDisabled onChange={() => setChecked((prev) => !prev)} value={checked} />
            <CheckboxVisual isInvalid onChange={() => setChecked((prev) => !prev)} value={checked} />
            <CheckboxVisual onChange={() => setChecked((prev) => !prev)} value={checked} size="lg" />
        </>
    );

    return (
        <div className="flex">
            <div className="light flex flex-col gap-4 p-20">{checkboxes}</div>
            <div className="dark flex flex-col gap-4 bg-gray-900 p-20">{checkboxes}</div>
        </div>
    );
}
