import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React from "react";
import { Icon } from "../icon";
import { FormControlProps } from "../types";
import { CheckboxSizes } from "./theme";

export type CheckboxSize = keyof CheckboxSizes;

export interface CheckboxVisualProps extends FormControlProps<boolean> {
    indeterminate?: boolean;
    isFocused?: boolean; // TODO move to FormControlProps and implement for each component
    size?: CheckboxSize;
}

export const CheckboxVisual = forwardRef<HTMLDivElement, CheckboxVisualProps>("CheckboxVisual", (props, ref) => (
    <div
        className={cx(
            "uxf-checkbox",
            `uxf-checkbox--size-${props.size ?? "default"}`,
            props.indeterminate && CLASSES.IS_INDETERMINATE,
            props.isDisabled && CLASSES.IS_DISABLED,
            props.isFocused && CLASSES.IS_FOCUSED,
            props.isInvalid && CLASSES.IS_INVALID,
            props.isReadOnly && CLASSES.IS_READONLY,
            props.value && CLASSES.IS_SELECTED,
        )}
        onClick={() => props.onChange?.(!props.value)}
        ref={ref}
    >
        <span className="uxf-checkbox__icon">
            <Icon name="check" />
        </span>
    </div>
));
