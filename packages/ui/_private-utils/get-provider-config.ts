import { ICONS } from "../config/icons";
import { UiContextType } from "../context";
import { twScreens } from "../tw-tokens/tw-screens";

export const getProviderConfig = (): UiContextType => ({
    icon: { spriteFilePath: "/icons-generated/_icon-sprite.svg", iconsConfig: ICONS },
    rasterImage: {
        breakpoints: twScreens,
    },
});
