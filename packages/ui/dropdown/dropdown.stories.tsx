import { Menu as HUIMenu } from "@headlessui/react";
import React from "react";
import { Dropdown } from "./index";
import { Button } from "../button";
import { CLASSES } from "@uxf/core/constants/classes";

export default {
    title: "UI/Dropdown",
    component: Dropdown,
};

const testDropdownItems: { id: number; title: string }[] = [
    { id: 1, title: "Test 1" },
    { id: 2, title: "Test 2" },
    { id: 3, title: "Test 3" },
];

export function Default() {
    const storyDropdown = (
        <>
            <HUIMenu as="div" className="relative">
                <HUIMenu.Button as={Button} color="success" title="Test">
                    Click me
                </HUIMenu.Button>
                <HUIMenu.Items as={Dropdown.Items}>
                    {testDropdownItems.map((item) => (
                        <HUIMenu.Item key={item.id}>
                            {({ active }) => (
                                <Dropdown.Item className={`first-letter:uppercase ${active ? CLASSES.IS_ACTIVE : ""}`}>
                                    {item.title}
                                </Dropdown.Item>
                            )}
                        </HUIMenu.Item>
                    ))}
                </HUIMenu.Items>
            </HUIMenu>
        </>
    );

    return (
        <div className="flex">
            <div className="light grow gap-4 p-4">{storyDropdown}</div>
            <div className="dark grow gap-4 bg-gray-900 p-4 text-gray-200">{storyDropdown}</div>
        </div>
    );
}
