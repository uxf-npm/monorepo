import { useAnchorProps, UseAnchorProps } from "@uxf/core/hooks/useAnchorProps";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { AnchorHTMLAttributes, HTMLAttributes } from "react";

export interface ItemProps extends Omit<AnchorHTMLAttributes<HTMLAnchorElement>, "type">, UseAnchorProps {}

type ItemTypeRef = HTMLAnchorElement;

const Item = forwardRef<ItemTypeRef, ItemProps>("Item.", (props, ref) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { children, className, ...restProps } = props;
    const anchorProps = useAnchorProps<AnchorHTMLAttributes<HTMLAnchorElement>>({
        className: `uxf-dropdown__item ${className || ""}`,
        role: "menuitem",
        ...restProps,
    });

    return (
        <a ref={ref} {...anchorProps}>
            {children}
        </a>
    );
});

type ItemsProps = HTMLAttributes<HTMLDivElement>;
export type ItemsTypeProps = HTMLDivElement;

const Items = forwardRef<ItemsTypeProps, ItemsProps>("Items", (props, ref) => {
    return (
        <div className={cx("uxf-dropdown__items", props.className)} ref={ref} role={props.role}>
            {props.children}
        </div>
    );
});

export const Dropdown = {
    Item,
    Items,
};
