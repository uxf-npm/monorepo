import { cx } from "@uxf/core/utils/cx";
import React, { HTMLAttributes } from "react";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { Icon } from "../icon";

export interface AvatarProps extends HTMLAttributes<HTMLDivElement> {
    src?: string;
}

export const Avatar = forwardRef<HTMLDivElement, AvatarProps>("Avatar", (props, ref) => {
    const avatarClassName = cx("uxf-avatar", props.className);

    return (
        <div ref={ref} className={avatarClassName}>
            {props.src ? <img alt="" className="uxf-avatar__image" src={props.src} /> : <Icon name="user" size={16} />}
        </div>
    );
});
