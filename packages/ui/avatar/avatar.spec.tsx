import React from "react";
import { Avatar } from "./avatar";
import { snapTest } from "../utils/snap-test";

snapTest("render basic", <Avatar src="/image.jpg" />);
