import React from "react";
import { Avatar } from "./avatar";

export default {
    title: "UI/Avatar",
    component: Avatar,
};

export function Default() {
    const testAvatars = (
        <>
            <Avatar src="https://i.picsum.photos/id/1062/5092/3395.jpg?hmac=o9m7qeU51uOLfXvepXcTrk2ZPiSBJEkiiOp-Qvxja-k" />
            <Avatar />
        </>
    );

    return (
        <>
            <div className="light flex items-center space-x-4 rounded bg-white p-8">{testAvatars}</div>
            <div className="dark flex items-center space-x-4 rounded bg-gray-900 p-8 text-white">{testAvatars}</div>
        </>
    );
}
