import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import React from "react";
import { FormControlProps } from "../types";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { RadioSizes } from "./theme";

export type RadioSize = keyof RadioSizes;

export interface RadioProps extends FormControlProps<boolean> {
    label: string;
    size?: RadioSize;
}

export const Radio = forwardRef<HTMLDivElement, RadioProps>("Radio", (props, ref) => {
    return (
        <div
            className={cx(
                "uxf-radio",
                `uxf-radio--size-${props.size ?? "default"}`,
                props.isDisabled && CLASSES.IS_DISABLED,
                props.isInvalid && CLASSES.IS_INVALID,
                props.isReadOnly && CLASSES.IS_READONLY,
                props.value && CLASSES.IS_SELECTED,
            )}
            onClick={() => props.onChange?.(!props.value)}
            ref={ref}
        >
            <span className="uxf-radio__label">{props.label}</span>
            <div className="uxf-radio__inner" />
        </div>
    );
});
