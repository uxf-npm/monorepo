import React from "react";
import { snapTest } from "../utils/snap-test";
import { Radio } from "./radio";

snapTest("render basic", <Radio label="label" onChange={() => void null} />);
snapTest("render disabled", <Radio label="label" isDisabled onChange={() => void null} />);
