import { Radio } from "./index";
import React from "react";

export default {
    title: "UI/Radio",
    component: Radio,
};

export function Default() {
    const radios = (
        <>
            <div className="mt-4 mb-2 font-semibold">Default</div>
            <div className="flex flex-row gap-4">
                <Radio value={false} label="Vyber" />
                <Radio value={true} label="Vyber" />
            </div>
            <div className="mt-4 mb-2 font-semibold">Invalid</div>
            <div className="flex flex-row gap-4">
                <Radio value={false} label="Vyber" isInvalid />
                <Radio value={true} label="Vyber" isInvalid />
            </div>
            <div className="mt-4 mb-2 font-semibold">Disabled</div>
            <div className="flex flex-row gap-4">
                <Radio value={false} label="Vyber" isDisabled />
                <Radio value={true} label="Vyber" isDisabled />
            </div>
            <div className="mt-4 mb-2 font-semibold">size lg</div>
            <div className="flex flex-row gap-4">
                <Radio value={false} label="Vyber" size="lg" />
                <Radio value={true} label="Vyber" size="lg" />
            </div>
        </>
    );

    return (
        <div className="flex">
            <div className="light p-20 ">{radios}</div>
            <div className="dark bg-gray-900 p-20 text-white">{radios}</div>
        </div>
    );
}
