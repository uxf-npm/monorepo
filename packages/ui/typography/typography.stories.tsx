import React from "react";

export default {
    title: "UI/Typography",
    component: null,
};

export function Default() {
    return (
        <div className="space-y-4">
            <h1 className="uxf-typo-h1">.uxf-typo-h1</h1>
            <h2 className="uxf-typo-h2">.uxf-typo-h2</h2>
            <h3 className="uxf-typo-h3">.uxf-typo-h3</h3>
            <h4 className="uxf-typo-h4">.uxf-typo-h4</h4>
            <h5 className="uxf-typo-h5">.uxf-typo-h5</h5>
            <h6 className="uxf-typo-h6">.uxf.typo-h6</h6>
            <p className="uxf-typo-body">.uxf-typo-body</p>
            <p className="uxf-typo-body2">.uxf-typo-body2</p>
            <p className="uxf-typo-button">.uxf-typo-button</p>
            <p className="uxf-typo-caption">.uxf-typo-caption</p>
            <p className="uxf-typo-medium">.uxf-typo-medium</p>
            <p className="uxf-typo-medium2">.uxf-typo-medium2</p>
            <p className="uxf-typo-overline">.uxf-typo-overline</p>
        </div>
    );
}
