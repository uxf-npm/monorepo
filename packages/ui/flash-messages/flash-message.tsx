import { useClickableProps } from "@uxf/core/hooks/useClickableProps";
import React, { FC, HTMLAttributes, MouseEventHandler, ReactNode, useEffect } from "react";
import { FlashMessageVariants } from "@uxf/ui/flash-messages/theme";

export type FlashMessageVariant = keyof FlashMessageVariants;

export interface Notification {
    autoDismiss?: boolean;
    message: ReactNode;
    dismissTimeout?: number;
    id?: number;
    onClick?: () => void;
    variant?: FlashMessageVariant;
}

export interface FlashMessageProps {
    notification: Notification;
    onClose: MouseEventHandler<HTMLDivElement>;
}

// eslint-disable-next-line react/destructuring-assignment
export const FlashMessage: FC<FlashMessageProps> = ({ notification, onClose }) => {
    useEffect(() => {
        const { autoDismiss = true, dismissTimeout = 5000 } = notification;

        let timer: number;
        if (autoDismiss) {
            timer = window.setTimeout(onClose, dismissTimeout);
        }
        return () => clearTimeout(timer);
    }, [onClose, notification]);

    const clickableProps = useClickableProps<HTMLAttributes<HTMLDivElement>>({
        className: `uxf-flash-message uxf-flash-message--${notification.variant || "success"}`,
        onClick: onClose,
        role: "listitem alert",
    });

    return <div {...clickableProps}>{notification.message}</div>;
};
