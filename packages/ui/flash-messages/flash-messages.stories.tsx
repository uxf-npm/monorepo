import React from "react";
import { FlashMessages } from "./flash-messages";
import { flashMessage } from "./flash-messages-service";
import { Button } from "@uxf/ui/button";
import { Icon } from "@uxf/ui/icon";

export default {
    title: "UI/FlashMessages",
    component: FlashMessages,
};

export function Default() {
    return (
        <div className="light flex space-x-4">
            <Button
                onClick={() =>
                    flashMessage({
                        message: (
                            <div className="flex items-center space-x-4">
                                <Icon name="check" size={24} />
                                <span>Everything is alright.</span>
                            </div>
                        ),
                    })
                }
            >
                Click to fire Flash message
            </Button>
            <Button
                onClick={() =>
                    flashMessage({
                        message: (
                            <div className="flex items-center space-x-4">
                                <Icon name="xmarkLarge" size={24} />
                                <span>Seems like an error occurred :)</span>
                            </div>
                        ),
                        autoDismiss: false,
                        variant: "error",
                    })
                }
            >
                AutoDismiss: false Flash message, variant error
            </Button>
        </div>
    );
}
