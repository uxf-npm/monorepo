import { Notification } from "@uxf/ui/flash-messages/flash-message";
import { createRef } from "react";
import { FlashMessagesRef } from "@uxf/ui/flash-messages/flash-messages";

const flashMessagesRef = createRef<FlashMessagesRef>();

export function getFlashMessagesRef() {
    return flashMessagesRef;
}

export function flashMessage(notification: Notification) {
    if (flashMessagesRef.current) {
        flashMessagesRef.current.open(notification);
    }
}
