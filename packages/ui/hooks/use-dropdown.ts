import { autoUpdate, flip, Placement, shift, size, useFloating } from "@floating-ui/react";

export function useDropdown(placement: Placement, matchWidth = false) {
    return useFloating({
        placement,
        middleware: [
            flip(),
            shift(),
            size({
                apply({ availableHeight, availableWidth, elements, strategy, x, y }) {
                    Object.assign(elements.floating.style, {
                        left: x + "px",
                        maxHeight: Math.min(240, availableHeight) + "px",
                        maxWidth: matchWidth
                            ? Math.min(availableWidth, elements.reference.getBoundingClientRect().width) + "px"
                            : availableWidth,
                        position: strategy,
                        top: y + "px",
                    });
                },
            }),
        ],
        whileElementsMounted: (reference, floating, update) =>
            autoUpdate(reference, floating, update, {
                elementResize: typeof ResizeObserver !== "undefined",
            }),
    });
}
