import React from "react";
import { Badge } from "./badge";
import { Icon } from "../icon";

export default {
    title: "UI/Badge",
    component: Badge,
};

export function Default() {
    const testBadges = (
        <>
            <Badge size="small">5</Badge>
            <Badge size="small">12</Badge>
            <Badge size="small">123</Badge>
            <Badge size="small">1234</Badge>
            <Badge>5</Badge>
            <Badge>12</Badge>
            <Badge>123</Badge>
            <Badge>1234</Badge>
            <Badge size="large">5</Badge>
            <Badge size="large">12</Badge>
            <Badge size="large">123</Badge>
            <Badge size="large">1234</Badge>
            <>
                <Badge size="small">
                    <Icon name="check" size={12} />
                </Badge>
                <Badge>
                    <Icon name="check" size={16} />
                </Badge>
                <Badge size="large">
                    <Icon name="check" size={24} />
                </Badge>
            </>
        </>
    );

    return (
        <>
            <div className="light flex items-center space-x-4 rounded bg-white p-8">{testBadges}</div>
            <div className="dark flex items-center space-x-4 rounded bg-gray-900 p-8 text-white">{testBadges}</div>
        </>
    );
}
