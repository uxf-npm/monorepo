import { cx } from "@uxf/core/utils/cx";
import React, { HTMLAttributes } from "react";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { BadgeSizes } from "@uxf/ui/badge/theme";

export type BadgeSize = keyof BadgeSizes;

export interface BadgeProps extends HTMLAttributes<HTMLDivElement> {
    size?: BadgeSize;
}

export const Badge = forwardRef<HTMLDivElement, BadgeProps>("Badge", (props, ref) => {
    const badgeClassName = cx("uxf-badge", `uxf-badge--size-${props.size ?? "medium"}`, props.className);

    return (
        <div ref={ref} className={badgeClassName}>
            <span>{props.children}</span>
        </div>
    );
});
