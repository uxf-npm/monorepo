import React, { useState } from "react";
import { TimePickerInput } from "./time-picker-input";
import { Icon } from "../icon";
import { TimeType } from "@uxf/datepicker/hooks/use-time-picker";

export default {
    title: "UI/TimePickerInput",
    component: TimePickerInput,
};

export function Default() {
    const [time, setTime] = useState<TimeType | null>(null);

    const testTimePickers = (
        <>
            <TimePickerInput
                id="time-test"
                name="time"
                label="Čas čehokoliv"
                rightElement={<Icon name="clock" size={20} />}
                value={time}
                onChange={(data) => setTime(data)}
            />
            <TimePickerInput
                id="time-test-disabled"
                name="time-disabled"
                label="Čas disabled"
                rightElement={<Icon name="clock" size={20} />}
                value={time}
                onChange={(data) => setTime(data)}
                isDisabled
            />
            <TimePickerInput
                id="time-test-readonly"
                name="time-readonly"
                label="Čas readonly"
                rightElement={<Icon name="clock" size={20} />}
                value={time}
                onChange={(data) => setTime(data)}
                isReadOnly
            />
            <TimePickerInput
                id="time-test-invalid"
                name="time-invalid"
                label="Čas invalid"
                rightElement={<Icon name="clock" size={20} />}
                value={time}
                onChange={(data) => setTime(data)}
                isInvalid
            />
        </>
    );

    return (
        <>
            <div className="light max-w-[640px] space-y-4 rounded bg-white p-8">{testTimePickers}</div>
            <div className="dark max-w-[640px] space-y-4 rounded bg-gray-900 p-8 text-white">{testTimePickers}</div>
        </>
    );
}
