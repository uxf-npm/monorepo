import React, { FC, useContext, useRef } from "react";
import { TimePickerContext } from "@uxf/datepicker/contexts/time-picker-context";
import { useMinute } from "@uxf/datepicker/hooks/use-minute";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";
import { Button } from "../button";

export const TimePickerMinute: FC<{ minute: number; label: string }> = (props) => {
    const minuteRef = useRef(null);
    const { focusedMinute, isMinuteFocused, isMinuteSelected, onMinuteSelect, onMinuteFocus } =
        useContext(TimePickerContext);

    const { isSelected, onClick, onKeyDown, tabIndex } = useMinute({
        minute: props.minute,
        minuteRef,
        focusedMinute,
        isMinuteFocused,
        isMinuteSelected,
        onMinuteFocus,
        onMinuteSelect,
    });

    return (
        <Button
            variant="text"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            className={cx("uxf-time-picker__minute", isSelected && CLASSES.IS_SELECTED)}
            ref={minuteRef}
        >
            {props.label}
        </Button>
    );
};
