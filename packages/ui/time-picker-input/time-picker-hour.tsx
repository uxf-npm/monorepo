import React, { FC, useContext, useRef } from "react";
import { TimePickerContext } from "@uxf/datepicker/contexts/time-picker-context";
import { useHour } from "@uxf/datepicker/hooks/use-hour";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";
import { Button } from "../button";

export const TimePickerHour: FC<{ hour: number; label: string }> = (props) => {
    const hourRef = useRef(null);
    const { focusedHour, isHourFocused, isHourSelected, onHourSelect, onHourFocus } = useContext(TimePickerContext);

    const { onClick, onKeyDown, tabIndex, isSelected } = useHour({
        hour: props.hour,
        hourRef,
        isHourFocused,
        focusedHour,
        isHourSelected,
        onHourSelect,
        onHourFocus,
    });

    return (
        <Button
            variant="text"
            tabIndex={tabIndex}
            onKeyDown={onKeyDown}
            onClick={onClick}
            className={cx("uxf-time-picker__hour", isSelected && CLASSES.IS_SELECTED)}
            ref={hourRef}
        >
            {props.label}
        </Button>
    );
};
