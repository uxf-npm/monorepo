import React, { FC, useMemo, useState } from "react";
import { TimePickerHours } from "./time-picker-hours";
import { TimePickerMinutes } from "./time-picker-minutes";
import { OnTimeChangeType, TimeType, useTimePicker } from "@uxf/datepicker/hooks/use-time-picker";
import { Icon } from "../icon";
import { Button } from "../button";
import { TimePickerContext } from "@uxf/datepicker/contexts/time-picker-context";

export interface TimePickerProps {
    closePopoverHandler: () => void;
    onChange: (data: OnTimeChangeType) => void;
    selectedTime: TimeType | null;
}

export const TimePicker: FC<TimePickerProps> = (props) => {
    const [viewMode, setViewMode] = useState<"global" | "hour" | "minute">("global");

    const { goToNextMinute, goToPrevHour, goToPrevMinute, goToNextHour, ...contextProps } = useTimePicker({
        selectedTime: props.selectedTime,
        onTimeChange: props.onChange,
        onSelectCallback: () => setViewMode("global"),
    });

    const timePickerComponents = useMemo(
        () => ({
            global: (
                <div className="uxf-time-picker__global">
                    <div className="uxf-time-picker__global__column">
                        <Button isIconButton onClick={goToNextHour} title="Nadcházející" variant="text">
                            <Icon name="chevronUp" size={16} />
                        </Button>
                        <Button variant="text" onClick={() => setViewMode("hour")}>
                            {props.selectedTime?.hour ?? "0"}
                        </Button>
                        <Button isIconButton onClick={goToPrevHour} title="Předchozí" variant="text">
                            <Icon name="chevronDown" size={16} />
                        </Button>
                    </div>
                    <div>:</div>
                    <div className="uxf-time-picker__global__column">
                        <Button isIconButton onClick={goToNextMinute} title="Nadcházející" variant="text">
                            <Icon name="chevronUp" size={16} />
                        </Button>
                        <Button variant="text" onClick={() => setViewMode("minute")}>
                            {props.selectedTime
                                ? props.selectedTime.minute < 10
                                    ? "0" + props.selectedTime.minute
                                    : props.selectedTime.minute
                                : "00"}
                        </Button>
                        <Button isIconButton onClick={goToPrevMinute} title="Předchozí" variant="text">
                            <Icon name="chevronDown" size={16} />
                        </Button>
                    </div>
                </div>
            ),
            hour: <TimePickerHours />,
            minute: <TimePickerMinutes />,
        }),
        [goToPrevHour, props.selectedTime, goToNextHour, goToPrevMinute, goToNextMinute],
    );

    return (
        <TimePickerContext.Provider value={contextProps}>
            <div>{timePickerComponents[viewMode]}</div>
        </TimePickerContext.Provider>
    );
};
