import React, { FC } from "react";
import { TimePickerMinute } from "./time-picker-minute";
import { useMinutes } from "@uxf/datepicker/hooks/use-minutes";

export const TimePickerMinutes: FC = () => {
    const { minutes } = useMinutes({});

    return (
        <div className="uxf-time-picker__minutes">
            {minutes.map((minute) => (
                <TimePickerMinute minute={minute.value} label={minute.label} key={minute.value} />
            ))}
        </div>
    );
};
