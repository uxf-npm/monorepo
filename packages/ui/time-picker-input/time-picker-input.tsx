import { autoUpdate, flip, offset, shift, size, useFloating } from "@floating-ui/react";
import { Popover, Transition } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { TimeType } from "@uxf/datepicker/hooks/use-time-picker";
import React, { FocusEventHandler, Fragment, ReactNode, useId, useMemo, useState } from "react";
import { FormControl } from "../form-control";
import { InputProps } from "../input/input";
import { FormControlProps } from "../types";
import { TimePicker } from "./time-picker";

export interface TimePickerInputProps
    extends Omit<FormControlProps<TimeType | null>, "value">,
        Pick<InputProps, "size" | "variant"> {
    className?: string;
    defaultValue?: string;
    error?: ReactNode;
    errorId?: string;
    form?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    id?: string;
    label?: ReactNode;
    leftAddon?: ReactNode;
    leftElement?: ReactNode;
    placeholder?: TimeType;
    rightAddon?: ReactNode;
    rightElement?: ReactNode;
    value?: TimeType | null;
}

export const TimePickerInput = forwardRef<HTMLInputElement, TimePickerInputProps>("TimePickerInput", (props, ref) => {
    const [isFocused, setIsFocused] = useState(false);
    const generatedId = useId();
    const id = props.id ?? generatedId;

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const rootClassName = cx(
        "uxf-time-picker-input",
        isFocused && CLASSES.IS_FOCUSED,
        props.isDisabled && CLASSES.IS_DISABLED,
        props.isInvalid && CLASSES.IS_INVALID,
        props.leftElement && "uxf-time-picker-input--has-left-element",
        props.rightElement && "uxf-time-picker-input--has-right-element",
        props.isRequired && CLASSES.IS_REQUIRED,
        props.isReadOnly && CLASSES.IS_READONLY,
    );

    const onFocus: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(true);
        props.onFocus?.(e);
    };

    const onBlur: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(false);
        props.onBlur?.(e);
    };

    const floatingTimePicker = useFloating({
        placement: "bottom",
        middleware: [
            offset(8),
            flip(),
            shift(),
            size({
                apply({ availableHeight, availableWidth, elements, strategy, x, y }) {
                    Object.assign(elements.floating.style, {
                        left: x + "px",
                        maxHeight: Math.max(50, availableHeight) + "px",
                        maxWidth: availableWidth,
                        position: strategy,
                        top: y + "px",
                    });
                },
            }),
        ],

        whileElementsMounted: (reference, floating, update) =>
            autoUpdate(reference, floating, update, {
                elementResize: typeof ResizeObserver !== "undefined",
            }),
    });

    const stableRef = useMemo(
        () => composeRefs(ref, floatingTimePicker.reference),
        [ref, floatingTimePicker.reference],
    );

    return (
        <FormControl
            className={rootClassName}
            errorId={errorId}
            form={props.form}
            helperText={props.helperText}
            hiddenLabel={props.hiddenLabel}
            inputId={id}
            isRequired={props.isRequired}
            label={props.label}
        >
            <Popover className="relative">
                <Popover.Button as="div" className="uxf-time-picker-input__wrapper" ref={stableRef}>
                    <span className="uxf-time-picker-input__left-element">{props.leftElement}</span>
                    <input
                        aria-describedby={errorId}
                        aria-errormessage={props.error && errorId ? `${errorId}__error-message` : undefined}
                        aria-invalid={!!props.error}
                        aria-live="polite"
                        autoComplete="off"
                        className="uxf-time-picker-input__element"
                        disabled={props.isDisabled}
                        form={props.form}
                        id={id}
                        inputMode="text"
                        name={props.name}
                        onBlur={onBlur}
                        onChange={() => props.onChange}
                        onFocus={onFocus}
                        placeholder="Vyberte čas..."
                        readOnly={props.isReadOnly}
                        ref={ref}
                        required={props.isRequired}
                        tabIndex={props.isReadOnly ? -1 : undefined}
                        value={
                            props.value
                                ? `${props.value.hour}:${
                                      props.value.minute < 10 ? "0" + props.value.minute : props.value.minute
                                  }`
                                : ""
                        }
                    />
                    <span className="uxf-time-picker-input__right-element">{props.rightElement}</span>
                </Popover.Button>
                {!props.isDisabled && !props.isReadOnly && (
                    <Transition
                        as={Fragment}
                        enter="transition duration-200 ease-out"
                        enterFrom="origin-top scale-50 opacity-0"
                        enterTo="origin-top scale-100 opacity-100"
                        leave="transition duration-200 ease-out"
                        leaveFrom="origin-top scale-50 opacity-100"
                        leaveTo="origin-top scale-0 opacity-0"
                    >
                        <Popover.Panel
                            className="uxf-time-picker-input__popover"
                            ref={floatingTimePicker.floating}
                            static
                        >
                            {({ close }) =>
                                props.onChange ? (
                                    <TimePicker
                                        closePopoverHandler={close}
                                        onChange={props.onChange}
                                        selectedTime={props.value ?? null}
                                    />
                                ) : (
                                    <></>
                                )
                            }
                        </Popover.Panel>
                    </Transition>
                )}
            </Popover>
        </FormControl>
    );
});
