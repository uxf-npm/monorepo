import React, { FC } from "react";
import { TimePickerHour } from "./time-picker-hour";
import { useHours } from "@uxf/datepicker/hooks/use-hours";

export const TimePickerHours: FC = () => {
    const { hours } = useHours({});

    return (
        <div className="uxf-time-picker__hours">
            {hours.map((hour) => (
                <TimePickerHour hour={hour.value} label={hour.label} key={hour.value} />
            ))}
        </div>
    );
};
