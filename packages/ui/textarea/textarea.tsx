import { CLASSES } from "@uxf/core/constants/classes";
import { useLatest } from "@uxf/core/hooks/useLatest";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { FocusEventHandler, ReactNode, useEffect, useId, useRef, useState } from "react";
import { FormControl } from "../form-control";
import { FormControlProps } from "../types";

function autoHeightHandler(element: HTMLTextAreaElement, rows = 4) {
    element.style.height = "auto";
    if (rows > 1) {
        const contentHeight = element.scrollHeight;
        const lineHeight = parseInt(window.getComputedStyle(element).getPropertyValue("line-height"), 10);
        const fontSize = parseInt(window.getComputedStyle(element).getPropertyValue("font-size"), 10);
        const targetHeight = rows * fontSize * (lineHeight / fontSize);
        element.style.height = Math.max(targetHeight, contentHeight) + "px";
    }
}

const EVENTS = ["input", "cut", "paste", "drop"];

export interface TextareaProps extends FormControlProps<string, HTMLTextAreaElement> {
    autoComplete?: string;
    className?: string;
    disableAutoHeight?: boolean;
    form?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    id?: string;
    label?: ReactNode;
    maxLength?: number | undefined;
    minLength?: number | undefined;
    name?: string;
    placeholder?: string;
    rows?: number;
}

export const Textarea = forwardRef<HTMLTextAreaElement, TextareaProps>("Textarea", (props, ref) => {
    const innerRef = useRef<HTMLTextAreaElement>(null);

    const [isFocused, setIsFocused] = useState(false);

    const generatedId = useId();
    const id = props.id ?? generatedId;

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const rootClassName = cx(
        "uxf-textarea",
        isFocused && CLASSES.IS_FOCUSED,
        props.isDisabled && CLASSES.IS_DISABLED,
        props.isInvalid && CLASSES.IS_INVALID,
        props.isRequired && CLASSES.IS_REQUIRED,
        props.isReadOnly && CLASSES.IS_READONLY,
    );

    const onFocus: FocusEventHandler<HTMLTextAreaElement> = (e) => {
        setIsFocused(true);
        props.onFocus?.(e);
    };

    const onBlur: FocusEventHandler<HTMLTextAreaElement> = (e) => {
        setIsFocused(false);
        props.onBlur?.(e);
    };

    const latestRows = useLatest(props.rows);

    useEffect(() => {
        const node = innerRef.current;
        if (!node || props.disableAutoHeight) {
            return;
        }
        const handler = () => autoHeightHandler(node, latestRows.current);
        handler();
        EVENTS.map((e) => node.addEventListener(e, handler));
        window.addEventListener("resize", handler);
        return () => {
            EVENTS.map((e) => node.removeEventListener(e, handler));
            window.removeEventListener("resize", handler);
        };
    }, [props.disableAutoHeight, latestRows]);

    return (
        <FormControl
            className={rootClassName}
            errorId={errorId}
            form={props.form}
            helperText={props.helperText}
            hiddenLabel={props.hiddenLabel}
            inputId={id}
            isRequired={props.isRequired}
            label={props.label}
        >
            {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
            <div className="uxf-textarea__wrapper" onClick={() => innerRef.current?.focus()}>
                <textarea
                    aria-describedby={errorId}
                    aria-invalid={props.isInvalid}
                    autoComplete={props.autoComplete}
                    className="uxf-textarea__element"
                    disabled={props.isDisabled}
                    form={props.form}
                    id={id}
                    maxLength={props.maxLength}
                    minLength={props.minLength}
                    name={props.name}
                    onBlur={onBlur}
                    onChange={(event) => props.onChange?.(event.target.value, event)}
                    onFocus={onFocus}
                    placeholder={props.placeholder}
                    readOnly={props.isReadOnly}
                    ref={composeRefs(innerRef, ref)}
                    required={props.isRequired}
                    value={props.value}
                />
            </div>
        </FormControl>
    );
});
