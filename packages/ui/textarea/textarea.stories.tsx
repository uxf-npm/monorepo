import { Textarea } from "./index";
import React, { useState } from "react";

export default {
    title: "UI/Textarea",
    component: Textarea,
};

export function Default() {
    const [value, onChange] = useState("test");

    const storyTextAreas = (
        <div className="space-y-2">
            <Textarea
                id="textarea"
                label="Textarea"
                onChange={onChange}
                placeholder="Placeholder text...."
                value={value}
            />
            <Textarea id="textarea-readonly" label="Readonly" isReadOnly={true} onChange={onChange} value={value} />
            <Textarea
                isDisabled={true}
                id="textarea-disabled"
                label="Textarea disabled"
                onChange={onChange}
                value={value}
            />
            <Textarea
                id="textarea-rows10"
                isInvalid
                label="Textarea rows = 10, invalid"
                placeholder="Placeholder text...."
                rows={10}
                onChange={onChange}
                value={value}
            />
        </div>
    );

    return (
        <>
            <div className="light space-y-2 p-20">{storyTextAreas}</div>
            <div className="dark space-y-2 bg-gray-900 p-20">{storyTextAreas}</div>
        </>
    );
}
