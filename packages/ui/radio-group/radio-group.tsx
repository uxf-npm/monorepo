import { RadioGroup as HUIRadioGroup } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { CSSProperties, ReactNode, useRef } from "react";
import { useInputSubmit } from "../hooks/use-input-submit";
import { Label } from "../label";
import { Radio, RadioSize } from "../radio";
import { FormControlProps } from "../types";
import { RadioGroupVariants } from "./theme";

export type RadioGroupVariant = keyof RadioGroupVariants;

export type RadioGroupOptionValue = string | number;

export interface RadioGroupOption {
    disabled?: boolean;
    label: string;
    value: RadioGroupOptionValue;
}

export interface RadioGroupProps extends FormControlProps<RadioGroupOptionValue | null> {
    className?: string;
    helperText?: ReactNode;
    forceColumn?: boolean;
    hiddenLabel?: boolean;
    id: string;
    label: string;
    options: RadioGroupOption[];
    radioSize?: RadioSize;
    style?: CSSProperties;
    variant?: RadioGroupVariant;
}

export const RadioGroup = forwardRef<HTMLLabelElement, RadioGroupProps>("RadioGroup", (props, ref) => {
    const innerRef = useRef<HTMLDivElement>(null);

    useInputSubmit(innerRef, "radio-group", props.isDisabled);

    const errorId = props.isInvalid ? `${props.id}--errormessage` : undefined;
    const variant = props.variant ?? "list";
    const radioSize = props.radioSize ?? "default";

    const rootClassName = cx(
        "uxf-radio-group",
        `uxf-radio-group--${variant}`,
        props.isDisabled && CLASSES.IS_DISABLED,
        props.isInvalid && CLASSES.IS_INVALID,
        props.className,
    );

    return (
        <HUIRadioGroup
            className={rootClassName}
            disabled={props.isDisabled}
            id={props.id}
            onChange={props.onChange}
            style={props.style}
            value={props.value}
        >
            <HUIRadioGroup.Label as={Label} className="uxf-radio-group__label" isHidden={props.hiddenLabel} ref={ref}>
                {props.label}
            </HUIRadioGroup.Label>

            <div className="uxf-radio-group__options-wrapper">
                {props.options.map((option) => (
                    <HUIRadioGroup.Option
                        className={cx("uxf-radio-group__option", props.isDisabled && CLASSES.IS_DISABLED)}
                        disabled={option.disabled}
                        key={option.value.toString()}
                        value={option.value}
                    >
                        {(o) => (
                            <div className={cx("uxf-radio-group__option__wrapper", o.checked && CLASSES.IS_SELECTED)}>
                                <span className="uxf-radio-group__option__label">{option.label}</span>
                                <Radio
                                    isDisabled={props.isDisabled || o.disabled}
                                    isInvalid={props.isInvalid}
                                    label={option.label}
                                    ref={o.checked ? innerRef : undefined}
                                    size={radioSize}
                                    value={o.checked}
                                />
                            </div>
                        )}
                    </HUIRadioGroup.Option>
                ))}
            </div>
            {props.helperText && (
                <div className={cx("uxf-helper-text", props.isInvalid && CLASSES.IS_INVALID)} id={errorId}>
                    {props.helperText}
                </div>
            )}
        </HUIRadioGroup>
    );
});
