export interface RadioGroupVariants {
    list: true;
    radioButton: true;
    row: true;
}
