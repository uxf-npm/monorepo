import React from "react";
import { snapTest } from "../utils/snap-test";
import { RadioGroup } from "./radio-group";

const options = [
    {
        value: "1",
        label: "Radio one",
    },
    {
        value: "2",
        label: "Radio two",
    },
    {
        value: "3",
        label: "Radio three-sixty",
    },
];

snapTest("render basic", <RadioGroup id="basic" options={options} label="label" onChange={() => void null} />);
snapTest(
    "render disabled",
    <RadioGroup id="disabled" options={options} label="label" isDisabled onChange={() => void null} />,
);
