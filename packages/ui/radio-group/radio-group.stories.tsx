import { RadioGroup } from "./index";
import React, { useState } from "react";
import { RadioGroupOptionValue } from "./radio-group";

export default {
    title: "UI/RadioGroup",
    component: RadioGroup,
};

const options = [
    {
        value: "1",
        label: "Radio one",
    },
    {
        value: "2",
        label: "Radio two",
    },
    {
        value: "3",
        label: "Radio three-sixty",
    },
];

export function Default() {
    const [value, setValue] = useState<RadioGroupOptionValue | null>(options[0].value);

    const testRadioGroups = (
        <div className="space-y-10">
            <RadioGroup
                id="radiogroup"
                label="Light Radio group"
                onChange={(v) => setValue(v)}
                options={options}
                value={value}
            />
            <RadioGroup
                id="radiogroup"
                label="Radio group variant radio button"
                onChange={(v) => setValue(v)}
                options={options}
                value={value}
                variant="radioButton"
            />
            <RadioGroup
                hiddenLabel
                id="radiogroup"
                label="Radio group variant row with hidden label"
                onChange={(v) => setValue(v)}
                options={options}
                value={value}
                variant="row"
            />
            <RadioGroup
                id="radiogroup"
                label="Radio group variant row, radioSize lg"
                onChange={(v) => setValue(v)}
                options={options}
                value={value}
                variant="row"
                radioSize="lg"
            />
        </div>
    );

    return (
        <>
            <div className="light rounded bg-white p-8">{testRadioGroups}</div>
            <div className="dark rounded bg-gray-900 p-8 text-white">{testRadioGroups}</div>
        </>
    );
}
