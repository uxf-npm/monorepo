import { ClassValue, cx } from "@uxf/core/utils/cx";
import { getImgQuality, getSvgImgUrl, ImgSources, Quality } from "@uxf/core/utils/image";
import { ImageSource, ResizerImageProps, resizerImageUrl } from "@uxf/core/utils/resizer";
import React, { FC, ImgHTMLAttributes, ReactNode } from "react";
import { useComponentContext } from "../context";

type ImgProps = ImgHTMLAttributes<HTMLImageElement>;

type Props = {
    alt: string;
    breakpoints?: Record<string, string>;
    className?: string;
    heights?: number[];
    imgClassName?: string;
    loading?: ImgProps["loading"];
    mode?: "contain" | "cover" | "responsive";
    noImageContent?: ReactNode;
    options?: Omit<ResizerImageProps, "quality" | "toFormat">;
    quality?: Quality;
    role?: ImgProps["role"];
    src: ImageSource | null | undefined;
    widths?: number[];
} & (
    | {
          width: number;
          height: number;
      }
    | {
          width?: never;
          height: number;
      }
    | {
          width: number;
          height?: never;
      }
);

export const RasterImage: FC<Props> = (props) => {
    const componentContext = useComponentContext("rasterImage");

    const className: ClassValue[] = [
        "uxf-raster-image",
        props.mode === "contain" && "uxf-raster-image--contain",
        props.mode === "cover" && "uxf-raster-image--cover",
        props.className,
    ];

    const resizerSrc = resizerImageUrl(props.src, props.width ?? "auto", props.height ?? "auto", {
        ...(props.options ?? {}),
        quality: getImgQuality(props.quality, "original"),
    });

    if (!resizerSrc) {
        return <div className={cx("uxf-raster-image--no-image", ...className)}>{props.noImageContent}</div>;
    }

    const imgClassName: ClassValue[] = [
        "uxf-raster-image__img",
        props.mode === "contain" && "uxf-raster-image__img--contain",
        props.mode === "cover" && "uxf-raster-image__img--cover",
        props.imgClassName,
    ];

    const svgUrl = props.src && getSvgImgUrl(props.src);

    if (svgUrl === "svg") {
        return (
            <picture className={cx(className)}>
                <img
                    alt={props.alt}
                    className={cx(imgClassName)}
                    height={props.height}
                    loading={props.loading}
                    src={svgUrl}
                    width={props.width}
                />
            </picture>
        );
    }

    const breakpoints = ["0em", ...Object.values(props.breakpoints ?? componentContext.breakpoints)];

    return (
        <picture className={cx(className)}>
            {props.widths
                ?.map(
                    (w, i) =>
                        i < breakpoints.length && (
                            <ImgSources
                                breakpointIndex={i}
                                breakpoints={breakpoints}
                                height={
                                    props.heights && props.heights[i]
                                        ? props.heights[i]
                                        : props.width && props.height
                                        ? Math.ceil((props.height / props.width) * w)
                                        : "auto"
                                }
                                key={w}
                                options={props.options}
                                quality={props.quality}
                                src={props.src!} // eslint-disable-line @typescript-eslint/no-non-null-assertion
                                width={w}
                            />
                        ),
                )
                .reverse() ?? (
                <ImgSources
                    height={props.height ?? "auto"}
                    options={props.options}
                    quality={props.quality}
                    src={props.src!} // eslint-disable-line @typescript-eslint/no-non-null-assertion
                    width={props.width ?? "auto"}
                />
            )}
            <img
                alt={props.alt}
                className={cx(imgClassName)}
                height={props.height}
                loading={props.loading}
                src={resizerSrc}
                width={props.width}
            />
        </picture>
    );
};
