import React from "react";
import { RasterImage } from "./index";

export default {
    title: "UI/RasterImage",
    component: RasterImage,
};

export function Default() {
    return (
        <div className="space-y-2">
            <RasterImage alt="Alt text" height={100} width={300} mode="cover" src={undefined} />
        </div>
    );
}
