import React, { FC, Fragment, ReactNode, useState } from "react";
import { Transition, Dialog } from "@headlessui/react";
import { Icon } from "../icon";
import { UxfLogo } from "./uxf-logo";

export interface LayoutProps {
    children?: ReactNode;
    logo?: ReactNode;
    subtitle?: ReactNode;
    toolbarRight?: ReactNode;
    sidebar?: ReactNode;
}

export const Layout: FC<LayoutProps> = (props) => {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    return (
        <div className="uxf-layout">
            <div className="uxf-sidebar">
                <div className="uxf-sidebar__content">{props.sidebar}</div>
            </div>
            <div className="uxf-app-bar">
                <div className="uxf-app-bar__left-container">
                    <button type="button" className="uxf-app-bar__menu-button" onClick={() => setSidebarOpen(true)}>
                        <Icon name="bars" className="text-white" size={26} />
                    </button>
                    <div className="uxf-app-bar__logo">{props.logo ?? <UxfLogo />}</div>
                </div>
                <div className="uxf-app-bar__right-container">{props.toolbarRight}</div>
            </div>
            <Transition.Root show={sidebarOpen} as={Fragment}>
                <Dialog as="div" className="relative z-40 md:hidden" onClose={setSidebarOpen}>
                    <Transition.Child
                        as={Fragment}
                        enter="transition-opacity ease-linear duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="transition-opacity ease-linear duration-300"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-gray-600 bg-opacity-75" />
                    </Transition.Child>

                    <div className="fixed inset-0 z-40 flex">
                        <Transition.Child
                            as={Fragment}
                            enter="transition ease-in-out duration-300 transform"
                            enterFrom="-translate-x-full"
                            enterTo="translate-x-0"
                            leave="transition ease-in-out duration-300 transform"
                            leaveFrom="translate-x-0"
                            leaveTo="-translate-x-full"
                        >
                            <Dialog.Panel className="relative flex w-full max-w-xs flex-1 flex-col bg-white pt-5 pb-4">
                                <Transition.Child
                                    as={Fragment}
                                    enter="ease-in-out duration-300"
                                    enterFrom="opacity-0"
                                    enterTo="opacity-100"
                                    leave="ease-in-out duration-300"
                                    leaveFrom="opacity-100"
                                    leaveTo="opacity-0"
                                >
                                    <div className="absolute top-0 right-0 -mr-12 pt-2">
                                        <button
                                            type="button"
                                            className="ml-1 flex h-10 w-10 items-center justify-center rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                                            onClick={() => setSidebarOpen(false)}
                                        >
                                            <span className="sr-only">Close sidebar</span>
                                            <Icon name="xmarkLarge" size={24} className="text-white" />
                                        </button>
                                    </div>
                                </Transition.Child>
                                <div className="uxf-drawer__logo">{props.logo ?? <UxfLogo isDark />}</div>
                                <div className="uxf-drawer__content">{props.sidebar}</div>
                            </Dialog.Panel>
                        </Transition.Child>
                        <div className="w-14 flex-shrink-0" aria-hidden="true">
                            {/* Dummy element to force sidebar to shrink to fit close icon */}
                        </div>
                    </div>
                </Dialog>
            </Transition.Root>
            <main className="uxf-layout__content">{props.children}</main>
        </div>
    );
};
