import React from "react";
import { Layout } from "./layout";

export default {
    title: "Admin/Layout",
    component: Layout,
    layout: "fulscreen",
};

export function Default() {
    return (
        <Layout
            sidebar={
                <div className="p-8">
                    <div className="flex h-64 items-center justify-center rounded-xl border-4 border-dashed border-gray-200 text-xl font-semibold text-gray-300">
                        sidebar
                    </div>
                </div>
            }
            toolbarRight={
                <div className="p-8">
                    <div className="flex items-center justify-center rounded-xl border-4 border-dashed border-gray-200 px-5 text-xl font-semibold text-gray-300">
                        toolbarRight
                    </div>
                </div>
            }
        >
            <div className="p-8">
                <div className="flex h-64 items-center justify-center rounded-xl border-4 border-dashed border-gray-200 text-xl font-semibold text-gray-300">
                    children
                </div>
            </div>
        </Layout>
    );
}
