import React, { FC } from "react";
import { Icon } from "../icon";
import { cx } from "@uxf/core/utils/cx";
import { usePagination } from "@uxf/core/hooks/usePagination";
import { PaginationItem } from "@uxf/core/utils/pagination";

export interface PaginationProps {
    count: number;
    page: number;
    onPageChange: (index: number) => void;
}

export const Pagination: FC<PaginationProps> = (props) => {
    const isCurrentPageFirst = props.page === 1;
    const isCurrentPageLast = props.page === props.count;

    const pagination = usePagination({
        count: props.count,
        page: props.page,
        showFirstButton: true,
        showLastButton: true,
    });

    const onClick = (item: PaginationItem) => {
        if (typeof item === "number") {
            return props.onPageChange(item);
        }

        switch (item) {
            case "first":
                return props.onPageChange(1);
            case "previous":
                return props.onPageChange(props.page - 1);
            case "next":
                return props.onPageChange(props.page + 1);
            case "last":
                return props.onPageChange(props.count);
        }
    };

    return (
        <nav className="uxf-pagination">
            {pagination.map((item, index) => {
                const isDisabled =
                    ((item === "first" || item === "previous") && isCurrentPageFirst) ||
                    ((item === "last" || item === "next") && isCurrentPageLast);

                return (
                    <a
                        key={item}
                        onClick={isDisabled ? undefined : () => onClick(item)}
                        className={cx(
                            "uxf-pagination__button",
                            isDisabled && "is-disabled",
                            (item === "start-ellipsis" || item === "end-ellipsis") && "is-ellipsis",
                            item === props.page && "is-selected",
                            index === 0 && "is-first",
                            index === pagination.length - 1 && "is-last",
                        )}
                    >
                        {item === "first" ? (
                            <>
                                <span className="sr-only">First</span>
                                <Icon name="chevronsLeft" size={12} />
                            </>
                        ) : item === "previous" ? (
                            <>
                                <span className="sr-only">Previous</span>
                                <Icon name="chevronLeft" size={12} />
                            </>
                        ) : item === "start-ellipsis" || item === "end-ellipsis" ? (
                            <>...</>
                        ) : typeof item === "number" ? (
                            <>{item}</>
                        ) : item === "next" ? (
                            <>
                                <span className="sr-only">Next</span>
                                <Icon name="chevronRight" size={12} />
                            </>
                        ) : (
                            <>
                                <span className="sr-only">Last</span>
                                <Icon name="chevronsRight" size={12} />
                            </>
                        )}
                    </a>
                );
            })}
        </nav>
    );
};
