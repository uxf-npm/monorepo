import { Pagination } from "./pagination";
import React, { useState } from "react";

export default {
    title: "UI/Pagination",
    component: Pagination,
};

export function Default() {
    const [page, setPage] = useState(1);

    return (
        <div className="flex flex-col gap-4">
            <p>
                Je potřeba ikona <b>chevronsLeft</b>, <b>chevronLeft</b>, <b>chevronRight</b> a <b>chevronsRight</b>
            </p>
            <Pagination count={10} page={page} onPageChange={setPage} />
        </div>
    );
}
