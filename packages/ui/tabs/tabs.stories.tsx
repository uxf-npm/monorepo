import React from "react";
import { Tabs } from "./tabs";

export default {
    title: "UI/Tabs",
    component: Tabs,
};

export function Default() {
    const testTabs = (
        <>
            <div className="flex items-center gap-6">
                <div>
                    <p className="mb-4 text-gray-500">Default</p>
                    <Tabs>
                        <Tabs.Panel title="Tab 1 title" className="grid h-12 w-56 place-items-center bg-red-300">
                            content of the Tab 1
                        </Tabs.Panel>
                        <Tabs.Panel title="Tab 2 title" className="grid h-12 w-56 place-items-center bg-green-300">
                            content of the Tab 2
                        </Tabs.Panel>
                        <Tabs.Panel title="Tab 3 title" className="grid h-12 w-56 place-items-center bg-amber-300">
                            content of the Tab 3
                        </Tabs.Panel>
                        <Tabs.Panel
                            className="grid h-12 w-56 place-items-center bg-gray-300"
                            disabled
                            title="Tab 4 title - disabled"
                        >
                            content of the Tab 4 which is disabled
                        </Tabs.Panel>
                    </Tabs>
                </div>
                <div>
                    <p className="mb-4 text-gray-500">Segmented</p>
                    <Tabs variant="segmented">
                        <Tabs.Panel title="Tab 1 title" className="grid h-12 w-56 place-items-center bg-red-300">
                            content of the Tab 1
                        </Tabs.Panel>
                        <Tabs.Panel title="Tab 2 title" className="grid h-12 w-56 place-items-center bg-green-300">
                            content of the Tab 2
                        </Tabs.Panel>
                        <Tabs.Panel title="Tab 3 title" className="grid h-12 w-56 place-items-center bg-amber-300">
                            content of the Tab 3
                        </Tabs.Panel>
                        <Tabs.Panel
                            className="grid h-12 w-56 place-items-center bg-gray-300"
                            disabled
                            title="Tab 4 title - disabled"
                        >
                            content of the Tab 4 which is disabled
                        </Tabs.Panel>
                    </Tabs>
                </div>
            </div>
            <div className="w-[600px]">
                <p className="mb-4 text-gray-500">Default grow</p>
                <Tabs grow>
                    <Tabs.Panel title="Tab 1 title" className="grid h-12 w-56 place-items-center bg-red-300">
                        content of the Tab 1
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 2 title" className="grid h-12 w-56 place-items-center bg-green-300">
                        content of the Tab 2
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 3 title" className="grid h-12 w-56 place-items-center bg-amber-300">
                        content of the Tab 3
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 4 title" className="grid h-12 w-56 place-items-center bg-gray-300">
                        content of the Tab 4
                    </Tabs.Panel>
                </Tabs>
            </div>
            <div className="w-[600px]">
                <p className="mb-4 text-gray-500">Segmented grow</p>
                <Tabs variant="segmented" grow>
                    <Tabs.Panel title="Tab 1 title" className="grid h-12 w-56 place-items-center bg-red-300">
                        content of the Tab 1
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 2 title" className="grid h-12 w-56 place-items-center bg-green-300">
                        content of the Tab 2
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 3 title" className="grid h-12 w-56 place-items-center bg-amber-300">
                        content of the Tab 3
                    </Tabs.Panel>
                    <Tabs.Panel title="Tab 4 title" className="grid h-12 w-56 place-items-center bg-gray-300">
                        content of the Tab 4
                    </Tabs.Panel>
                </Tabs>
            </div>
        </>
    );

    return (
        <>
            <div className="light rounded bg-white p-8">{testTabs}</div>
            <div className="dark rounded bg-gray-900 p-8 text-white">{testTabs}</div>
        </>
    );
}
