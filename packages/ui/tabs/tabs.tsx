import { Tab } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { useMouseDragToScroll } from "@uxf/core/hooks/useMouseDragToScroll";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, {
    Children,
    FC,
    isValidElement,
    PropsWithChildren,
    ReactElement,
    ReactNode,
    useEffect,
    useRef,
    useState,
} from "react";

export interface TabsPanelProps {
    className?: string;
    disabled?: boolean;
    title: ReactNode;
}

const Panel: FC<PropsWithChildren<TabsPanelProps>> = (props) => <div className={props.className}>{props.children}</div>;

export interface TabsProps {
    className?: string;
    defaultIndex?: number;
    grow?: boolean;
    onChange?: (index: number) => void;
    tabListClassName?: string;
    variant?: "default" | "segmented";
}

const TabsRoot = forwardRef<HTMLDivElement, PropsWithChildren<TabsProps>>("Tabs", (props, ref) => {
    const tabsClassName = cx("uxf-tabs", props.grow && "uxf-tabs--grow", props.className);

    const containerRef = useRef<HTMLDivElement>(null);

    const dragStyle = useMouseDragToScroll(containerRef);

    const [hasOverflow, setHasOverflow] = useState<boolean>();
    useEffect(() => {
        const node = containerRef.current;
        if (!node) {
            return;
        }
        const handler = () => setHasOverflow(node.scrollWidth > node.clientWidth);
        handler();
        window.addEventListener("resize", handler);
        return () => {
            window.removeEventListener("resize", handler);
        };
    }, []);

    if (Children.count(props.children) === 0) {
        return <div className={tabsClassName} ref={ref} />;
    }

    const tabPanels = Children.toArray(props.children).filter(
        (child) => isValidElement(child) && child.props.title,
    ) as ReactElement[];

    const tabs = tabPanels.map((c) => ({
        title: (c as ReactElement).props.title,
        disabled: (c as ReactElement).props.disabled,
    }));

    return (
        <Tab.Group
            as="div"
            className={tabsClassName}
            defaultIndex={props.defaultIndex}
            onChange={props.onChange as any}
            ref={ref}
        >
            <Tab.List
                className={cx(
                    "uxf-tabs__tab-list__wrapper",
                    `uxf-tabs__tab-list__wrapper--${props.variant ?? "default"}`,
                    props.tabListClassName,
                )}
            >
                <div
                    className={cx("uxf-tabs__tab-list", `uxf-tabs__tab-list--${props.variant ?? "default"}`)}
                    ref={containerRef}
                    style={{ justifyContent: hasOverflow ? "flex-start" : undefined, ...dragStyle }}
                >
                    {tabs.map((tab, index) => (
                        <Tab
                            disabled={tab.disabled}
                            className={({ selected }) =>
                                cx(
                                    "uxf-tabs__tab",
                                    selected && CLASSES.IS_ACTIVE,
                                    tab.disabled && CLASSES.IS_DISABLED,
                                    `uxf-tabs__tab--${props.variant ?? "default"}`,
                                )
                            }
                            key={`${tab.title}--${index}`}
                        >
                            {tab.title}
                        </Tab>
                    ))}
                </div>
            </Tab.List>

            <Tab.Panels className="uxf-tabs__panels">
                {tabPanels.map((tab, index) => (
                    <Tab.Panel className="outline-none" key={`${tab}--${index}`}>
                        {tab}
                    </Tab.Panel>
                ))}
            </Tab.Panels>
        </Tab.Group>
    );
});

export const Tabs = Object.assign(TabsRoot, { Panel });
