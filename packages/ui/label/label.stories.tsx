import { Label } from "./index";
import React from "react";

export default {
    title: "UI/Label",
    component: Label,
};

export function Default() {
    return (
        <div className="space-y-2">
            <Label>Label</Label>
        </div>
    );
}
