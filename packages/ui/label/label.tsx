import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { MouseEventHandler, ReactNode } from "react";

export interface LabelProps {
    children?: ReactNode;
    className?: string;
    form?: string;
    htmlFor?: string;
    id?: string;
    isHidden?: boolean;
    isRequired?: boolean;
    onClick?: MouseEventHandler<HTMLLabelElement>;
}

type LabelTypeRef = HTMLLabelElement;

export const Label = forwardRef<LabelTypeRef, LabelProps>("Label", (props, ref) => {
    const className = cx(
        "uxf-label",
        props.isHidden && "uxf-label--hidden",
        props.isRequired && CLASSES.IS_REQUIRED,
        props.className,
    );
    return (
        <label
            className={className}
            form={props.form}
            htmlFor={props.htmlFor}
            id={props.id}
            onClick={props.onClick}
            ref={ref}
        >
            {props.children}
        </label>
    );
});
