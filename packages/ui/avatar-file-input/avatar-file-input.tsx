import { CLASSES } from "@uxf/core/constants/classes";
import { useInputFocus } from "@uxf/core/hooks/useInputFocus";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { ChangeEvent, CSSProperties, FocusEventHandler, useCallback, useRef, useState } from "react";
import { ErrorMessage } from "../error-message";
import { Icon } from "../icon";
import { RasterImage } from "../raster-image";

interface FileResponse {
    extension: string;
    id: number;
    name: string;
    namespace?: string | null;
    uuid: string;
}

export type AvatarFileInputChangeEventHandler = (file: FileResponse | null) => Promise<void>;
export type AvatarFileInputUploadHandler = (file: File, namespace?: string) => Promise<FileResponse>;

export interface AvatarFileInputProps {
    className?: string;
    disabled?: boolean;
    error?: string;
    form?: string;
    id: string;
    invalid?: boolean;
    label: string;
    name?: string;
    onBlur?: FocusEventHandler<HTMLInputElement>;
    onChange: AvatarFileInputChangeEventHandler;
    onFocus?: FocusEventHandler<HTMLInputElement>;
    onUploadError: (err: unknown) => void;
    readOnly?: boolean;
    required?: boolean;
    style?: CSSProperties;
    uploadHandler: AvatarFileInputUploadHandler;
    value?: FileResponse | null | undefined;
    namespace?: "default" | "user-files";
}

type AvatarFileInputTypeRef = HTMLInputElement;

export const AvatarFileInput = forwardRef<AvatarFileInputTypeRef, AvatarFileInputProps>(
    "AvatarFileInput",
    (props, ref) => {
        const innerRef = useRef<HTMLInputElement>(null);

        const input = useInputFocus(innerRef, props.onBlur, props.onFocus);
        const errorId = props.invalid && props.id ? `${props.id}--errormessage` : undefined;

        const [uploading, setUploading] = useState(false);

        const _onChange = useCallback<(e: ChangeEvent<HTMLInputElement>) => Promise<void>>(
            async (e) => {
                e.persist();
                const files = e.target.files ?? [];
                if (files.length === 1) {
                    const file = files[0];
                    try {
                        setUploading(true);
                        const response = await props.uploadHandler(file);
                        await props.onChange(response);
                    } catch (err) {
                        props.onUploadError(err);
                    } finally {
                        setUploading(false);
                    }
                }
            },
            [props],
        );

        return (
            <div className={`_form-field ${props.className || ""}`} style={props.style}>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cx(
                        "uxf-avatar-file-input",
                        input.focused && CLASSES.IS_FOCUSED,
                        props.disabled && CLASSES.IS_DISABLED,
                        props.readOnly && CLASSES.IS_READONLY,
                        props.invalid && CLASSES.IS_INVALID,
                        props.value && "is-disabled:opacity-50",
                    )}
                >
                    <label className="sr-only" htmlFor={props.id}>
                        {props.label}
                    </label>
                    <div className="relative flex h-full w-full flex-col items-center justify-center rounded-inherit">
                        <Icon name="camera" size={36} className="text-gray-800" />
                        <p className="mt-1 text-xs text-gray-800">
                            {uploading ? "nahrávám..." : props.value ? "změnit foto" : "přidat foto"}
                        </p>
                    </div>
                    {!!props.value && (
                        <RasterImage
                            alt=""
                            className={cx(
                                "absolute left-0 top-0 block h-full w-full rounded-inherit object-cover",
                                uploading && "opacity-0",
                            )}
                            height={96}
                            src={props.value}
                            width={96}
                        />
                    )}
                    <input
                        accept="image/*"
                        aria-describedby={errorId}
                        aria-invalid={props.invalid}
                        className="input-reset absolute left-0 top-0 h-full w-full cursor-pointer rounded-inherit opacity-0"
                        disabled={props.disabled}
                        form={props.form}
                        id={props.id}
                        name={props.name}
                        onBlur={input.onBlur}
                        onChange={_onChange}
                        onFocus={input.onFocus}
                        readOnly={props.readOnly}
                        ref={composeRefs(innerRef, ref)}
                        required={props.required}
                        tabIndex={props.readOnly ? -1 : undefined}
                        type="file"
                    />
                </div>
                {props.invalid && props.error && <ErrorMessage id={errorId}>{props.error}</ErrorMessage>}
            </div>
        );
    },
);
