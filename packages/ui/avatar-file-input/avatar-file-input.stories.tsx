import { AvatarFileInput } from "./index";
import React from "react";

export default {
    title: "UI/AvatarFileInput",
    component: AvatarFileInput,
};

export function Default() {
    return (
        <div className="space-y-2">
            <AvatarFileInput
                id="afi"
                label="Vyberte Avatar"
                onChange={(value) => {
                    return new Promise(() => {
                        // eslint-disable-next-line no-console
                        console.log("Changed:", value);
                    });
                }}
                onUploadError={() => {
                    // eslint-disable-next-line no-console
                    console.log("Upload error");
                }}
                uploadHandler={() => {
                    return new Promise(() => {
                        // eslint-disable-next-line no-console
                        console.log("Upload handler");
                    });
                }}
            />
        </div>
    );
}
