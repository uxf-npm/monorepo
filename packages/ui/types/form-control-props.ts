import { ChangeEvent, FocusEventHandler } from "react";

export interface FormControlProps<ValueType, ElementType = HTMLInputElement, RestParams extends unknown[] = []> {
    value?: ValueType;
    onChange?: (value: ValueType, event?: ChangeEvent<ElementType>, ...args: RestParams) => void;
    onBlur?: FocusEventHandler<ElementType>;
    onFocus?: FocusEventHandler<ElementType>;
    name?: string;
    /**
     * @about
     * The input element will be disabled.
     */
    isDisabled?: boolean;
    /**
     * @about
     * It prevents the user from changing the value of the field (not from interacting with the field).
     */
    isReadOnly?: boolean;

    isInvalid?: boolean;
    isRequired?: boolean;
}

export type OptionType<ValueType = string, LabelType = string> = {
    value: ValueType;
    label: LabelType;
};
