import React, { useState } from "react";
import { TextInput } from "@uxf/ui/text-input";
import ComponentStructureAnalyzer from "@uxf/ui/utils/component-structure-analyzer";

export default {
    title: "UI/TextInput",
    component: TextInput,
};

export function Full() {
    const [value, onChange] = useState("");
    const [timeValue, onTimeChange] = useState("");

    const storyTextInputs = (
        <div className="space-y-4">
            <TextInput
                label="Default input"
                value={value}
                onChange={onChange}
                helperText="Helper text"
                leftAddon="https://"
                rightAddon=".cz"
                leftElement="🌷"
                rightElement="🔔"
                placeholder="Some beautiful placeholder..."
            />
            <TextInput
                label="Error input"
                value={value}
                onChange={onChange}
                helperText="Error helper text"
                leftAddon="https://"
                rightAddon=".cz"
                leftElement="🌷"
                rightElement="🔔"
                placeholder="Some beautiful placeholder..."
                isInvalid
            />
            <TextInput
                label="Read only input"
                value={value}
                onChange={onChange}
                helperText="Error helper text"
                leftAddon="https://"
                rightAddon=".cz"
                leftElement="🌷"
                rightElement="🔔"
                placeholder="Some beautiful placeholder..."
                isReadOnly
            />
            <TextInput
                label="Disabled input"
                value={value}
                onChange={onChange}
                helperText="Error helper text"
                leftAddon="https://"
                rightAddon=".cz"
                leftElement="🌷"
                rightElement="🔔"
                placeholder="Some beautiful placeholder..."
                isDisabled
            />
            <TextInput
                label="Default tml time input"
                value={timeValue}
                onChange={onTimeChange}
                placeholder="Some beautiful placeholder..."
                type="time"
            />
        </div>
    );

    return (
        <>
            <div className="light space-y-2 p-20">{storyTextInputs}</div>
            <div className="dark space-y-2 bg-gray-900 p-20">{storyTextInputs}</div>
        </>
    );
}

export function ComponentStructure() {
    const [value, onChange] = useState("");

    return (
        <ComponentStructureAnalyzer>
            <TextInput
                label="Label"
                placeholder="Placeholder"
                leftElement="🌷"
                rightElement="🔔"
                leftAddon="https://"
                rightAddon=".cz"
                value={value}
                onChange={onChange}
                isInvalid={false}
                helperText="Helper text"
            />
        </ComponentStructureAnalyzer>
    );
}
