import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { Input } from "@uxf/ui/input";
import { InputProps } from "@uxf/ui/input/input";
import { FormControlProps } from "@uxf/ui/types";
import React, { FocusEventHandler, KeyboardEventHandler, ReactNode, useId, useState } from "react";
import { FormControl } from "../form-control";

type EnterKeyHint = "enter" | "done" | "go" | "next" | "previous" | "search" | "send" | undefined;

type InputMode = "none" | "text" | "tel" | "url" | "email" | "numeric" | "decimal" | "search" | undefined;

export interface TextInputProps extends FormControlProps<string>, Pick<InputProps, "size" | "variant"> {
    autoComplete?: string;
    className?: string;
    enterKeyHint?: EnterKeyHint;
    form?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    id?: string;
    inputMode?: InputMode;
    label?: ReactNode;
    leftAddon?: ReactNode;
    leftElement?: ReactNode;
    onKeyDown?: KeyboardEventHandler<HTMLInputElement>;
    placeholder?: string;
    rightAddon?: ReactNode;
    rightElement?: ReactNode;
    type?: "email" | "number" | "password" | "search" | "tel" | "text" | "url" | "time";

    // type: "password" | "search" | "tel" | "text" | "url"
    maxLength?: number | undefined;
    minLength?: number | undefined;

    // type: "number"
    max?: number | string;
    min?: number | string;
    pattern?: string;
    step?: number | string;
}

export const TextInput = forwardRef<HTMLInputElement, TextInputProps>("TextInput", (props, ref) => {
    const [isFocused, setIsFocused] = useState(false);

    const generatedId = useId();
    const id = props.id ?? generatedId;

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const rootClassName = cx(
        "uxf-text-input",
        isFocused && CLASSES.IS_FOCUSED,
        props.isDisabled && CLASSES.IS_DISABLED,
        props.isInvalid && CLASSES.IS_INVALID,
        props.isRequired && CLASSES.IS_REQUIRED,
        props.className,
    );

    const onFocus: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(true);
        props.onFocus?.(e);
    };

    const onBlur: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(false);
        props.onBlur?.(e);
    };

    return (
        <FormControl
            className={rootClassName}
            errorId={errorId}
            form={props.form}
            helperText={props.helperText}
            hiddenLabel={props.hiddenLabel}
            inputId={id}
            isRequired={props.isRequired}
            label={props.label}
        >
            <Input size={props.size} variant={props.variant}>
                {props.leftAddon && <Input.LeftAddon>{props.leftAddon}</Input.LeftAddon>}
                {props.leftElement && <Input.LeftElement>{props.leftElement}</Input.LeftElement>}
                <Input.Element
                    aria-describedby={errorId}
                    form={props.form}
                    id={id}
                    inputMode={props.inputMode}
                    isDisabled={props.isDisabled}
                    isInvalid={props.isInvalid}
                    isReadOnly={props.isReadOnly}
                    isRequired={props.isRequired}
                    max={props.max}
                    maxLength={props.maxLength}
                    min={props.min}
                    minLength={props.minLength}
                    name={props.name}
                    onBlur={onBlur}
                    onChange={props.onChange}
                    onFocus={onFocus}
                    onKeyDown={props.onKeyDown}
                    pattern={props.pattern}
                    placeholder={props.placeholder}
                    ref={ref}
                    step={props.step}
                    type={props.type}
                    value={props.value}
                />
                {props.rightElement && <Input.RightElement>{props.rightElement}</Input.RightElement>}
                {props.rightAddon && <Input.RightAddon>{props.rightAddon}</Input.RightAddon>}
            </Input>
        </FormControl>
    );
});
