import React from "react";
import { TextInput } from "./text-input";
import { snapTest } from "../utils/snap-test";

snapTest(
    "render basic",
    <TextInput
        helperText="helper text"
        label="label"
        leftAddon="left addon"
        rightAddon="right addon"
        leftElement="left element"
        rightElement="right element"
    />,
);
