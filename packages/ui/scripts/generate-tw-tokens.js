#!/usr/bin/env node

const fs = require("fs");
const resolveConfig = require("tailwindcss/resolveConfig");
const prettier = require("prettier");
const path = require("path");

function camelToSnakeCase(str) {
    return str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`);
}

function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function resolveProperty(property, theme) {
    const entry = theme[property];

    if (typeof entry === "undefined") {
        return;
    }

    const propertyValue = JSON.stringify(entry);
    const propertyKey = capitalizeFirstLetter(property);

    return `
// this file is generated automatically, do not change anything manually in the contents of this file

export const tw${propertyKey} = ${propertyValue};

export type Tw${propertyKey} = typeof tw${propertyKey};
`;
}

const TW_TOKENS = [
    "boxShadow",
    "colors",
    "containers",
    "fontSize",
    "fontWeight",
    "letterSpacings",
    "lineHeight",
    "screens",
    "spacing",
    "zIndex",
];

let error = false;

try {
    const mode = process.argv.filter((c) => c.includes("--mode="))[0]?.split("=")[1];
    const twConfig = process.argv.filter((c) => c.includes("--twConfig="))[0]?.split("=")[1];
    const outputPath = process.argv.filter((c) => c.includes("--outputPath="))[0]?.split("=")[1];

    if (!twConfig) {
        throw Error(`Provide valid path to tailwind config. Use "--twConfig" argument.`);
    }

    if (!outputPath) {
        throw Error(`Provide valid output path to tailwind tokens. Use "--outputPath" argument.`);
    }

    const loadedTwConfig = resolveConfig(require(path.resolve(process.cwd(), twConfig)));

    TW_TOKENS.forEach((property) => {
        const result = resolveProperty(property, loadedTwConfig.theme) || "";

        const filePath = path.resolve(process.cwd(), `${outputPath}tw-${camelToSnakeCase(property)}.ts`);

        if (!result && mode !== "check" && fs.existsSync(filePath)) {
            fs.rmSync(filePath);
        }

        const newContent = prettier.format(result, {
            parser: "babel",
            tabWidth: 4,
        });

        if (mode === "check") {
            const oldContent = fs.existsSync(filePath) ? fs.readFileSync(filePath).toString() : "";

            if (newContent !== oldContent) {
                throw new Error("Generated tailwind tokens doesn't match with current tailwind config.");
            }
        } else if (newContent) {
            fs.writeFileSync(filePath, newContent, "utf-8");
        }
    });
} catch (err) {
    console.error(err.message);
    error = true;
}

process.exit(error ? 1 : 0);
