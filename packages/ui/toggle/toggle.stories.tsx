import { Toggle } from "./index";
import React, { useState } from "react";

export default {
    title: "UI/Toggle",
    component: Toggle,
};

export function Default() {
    const [checked, setChecked] = useState<boolean>(true);

    const storyToggles = (
        <>
            <Toggle
                label="Opravdu?"
                onChange={() => {
                    setChecked((prev) => !prev);
                }}
                value={checked}
            />
            <Toggle
                label="Opravdu?"
                onChange={() => {
                    setChecked((prev) => !prev);
                }}
                value={checked}
                isDisabled
            />
            <Toggle
                label="Opravdu?"
                onChange={() => {
                    setChecked((prev) => !prev);
                }}
                value={checked}
                hiddenLabel
            />
            <Toggle
                label="Opravdu? (reversed)"
                onChange={() => {
                    setChecked((prev) => !prev);
                }}
                value={checked}
                variant="reversed"
            />
        </>
    );

    return (
        <div className="flex">
            <div className="light w-1/2 gap-4 p-20">{storyToggles}</div>
            <div className="dark w-1/2 gap-4 bg-gray-900 p-20 text-white">{storyToggles}</div>
        </div>
    );
}
