export interface ToggleVariants {
    default: true;
    reversed: true;
}
