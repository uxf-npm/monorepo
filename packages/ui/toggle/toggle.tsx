import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { CSSProperties } from "react";
import { FormControlProps } from "../types";
import { Switch as HUISwitch } from "@headlessui/react";
import { ToggleVariants } from "./theme";

export type ToggleVariant = keyof ToggleVariants;

export interface ToggleProps extends FormControlProps<boolean> {
    className?: string;
    hiddenLabel?: boolean;
    label: string;
    style?: Partial<CSSProperties>;
    variant?: ToggleVariant;
}

export const Toggle = forwardRef<HTMLButtonElement, ToggleProps>("Toggle", (props, ref) => {
    return (
        <HUISwitch.Group>
            <div
                className={cx(
                    "uxf-toggle__wrapper",
                    `uxf-toggle__wrapper--${props.variant ?? "default"}`,
                    props.hiddenLabel && "uxf-toggle__wrapper--hiddenLabel",
                    props.className,
                )}
            >
                <HUISwitch
                    checked={props.value}
                    className={cx(
                        props.isDisabled && CLASSES.IS_DISABLED,
                        props.isInvalid && CLASSES.IS_INVALID,
                        props.isReadOnly && CLASSES.IS_READONLY,
                        props.value && CLASSES.IS_SELECTED,
                        "uxf-toggle",
                        `uxf-toggle--${props.variant ?? "default"}`,
                    )}
                    disabled={props.isDisabled}
                    name={props.name}
                    onChange={props.onChange}
                    style={props.style}
                    ref={ref}
                >
                    <span className="uxf-toggle__inner" />
                </HUISwitch>
                <HUISwitch.Label hidden={props.hiddenLabel} className="uxf-toggle__label">
                    {props.label}
                </HUISwitch.Label>
            </div>
        </HUISwitch.Group>
    );
});
