import React, { ReactNode } from "react";

interface ComponentStructureAnalyzerProps {
    children: ReactNode;
}
function ComponentStructureAnalyzer(props: ComponentStructureAnalyzerProps) {
    return <div className="uxf-component-structure-analyzer">{props.children}</div>;
}

export default ComponentStructureAnalyzer;
