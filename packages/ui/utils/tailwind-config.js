const plugin = require("tailwindcss/plugin");
const { em } = require("@uxf/styles/units/em");
const { rem } = require("@uxf/styles/units/rem");

/** @type {Record<string, string>} */
const CONTAINERS = {
    xs: rem(540),
    sm: rem(720),
    md: rem(928),
    lg: rem(1024),
    xl: rem(1216),
};

/** @type {Record<string, string>} */
const SCREENS = {
    xs: em(576),
    sm: em(768),
    md: em(992),
    lg: em(1200),
    xl: em(1440),
};

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [],
    theme: {
        containers: CONTAINERS,
        screens: SCREENS,
        inputSize: {
            xs: "30px",
            sm: "34px",
            default: "38px",
            lg: "42px",
            xl: "46px",
        },
        extend: {
            borderRadius: {
                inherit: "inherit",
            },
            colors: {
                inherit: "inherit",
                lightHigh: "#111827",
                lightMedium: "#4b5563",
                lightLow: "#9ca3af",
                darkHigh: "#ffffff",
                darkMedium: "#ffffffcc",
                darkLow: "#ffffff80",
                lightBorder: "#e5e7eb",
                darkBorder: "#374151",
                primary: {
                    50: "#eff6ff",
                    100: "#dbeafe",
                    200: "#bfdbfe",
                    300: "#93c5fd",
                    400: "#60a5fa",
                    500: "#3b82f6",
                    600: "#2563eb",
                    700: "#1d4ed8",
                    800: "#1e40af",
                    900: "#1e3a8a",
                },
                success: {
                    50: "#f0fdf4",
                    100: "#dcfce7",
                    200: "#bbf7d0",
                    300: "#86efac",
                    400: "#4ade80",
                    500: "#22c55e",
                    600: "#16a34a",
                    700: "#15803d",
                    800: "#166534",
                    900: "#14532d",
                },
                warning: {
                    50: "#fff7ed",
                    100: "#ffedd5",
                    200: "#fed7aa",
                    300: "#fdba74",
                    400: "#fb923c",
                    500: "#f97316",
                    600: "#ea580c",
                    700: "#c2410c",
                    800: "#9a3412",
                    900: "#7c2d12",
                },
                error: {
                    50: "#fef2f2",
                    100: "#fee2e2",
                    200: "#fecaca",
                    300: "#fca5a5",
                    400: "#f87171",
                    500: "#ef4444",
                    600: "#dc2626",
                    700: "#b91c1c",
                    800: "#991b1b",
                    900: "#7f1d1d",
                },
            },
            boxShadow: {
                paper: "0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.06)",
            },
            fontSize: {
                desktopH1: [rem(96), 112 / 96],
                desktopH2: [rem(60), 72 / 60],
                desktopH3: [rem(48), 64 / 48],
                desktopH4: [rem(34), 40 / 34],
                desktopH5: [rem(24), 32 / 24],
                desktopH6: [rem(16), 22 / 16],
                body: [rem(16), 24 / 16],
                body2: [rem(14), 20 / 14],
                button: [rem(16), 24 / 16],
                caption: [rem(12), 16 / 12],
                medium: [rem(16), 24 / 16],
                medium2: [rem(14), 20 / 14],
                overline: [rem(10), 16 / 10],
            },
            spacing: {
                inherit: "inherit",
            },
            zIndex: {
                1: "1",
                focus: "5",
                fixed: "10",
                menu: "25",
                header: "50",
                modal: "100",
                dropdown: "200",
                flashMessage: "250",
                tooltip: "300",
            },
        },
    },
    corePlugins: {
        aspectRatio: false,
        container: false,
    },
    plugins: [
        require("@tailwindcss/aspect-ratio"),
        require("@tailwindcss/line-clamp"),
        plugin(
            ({ addComponents, theme }) => {
                const screens = theme("screens", SCREENS);
                const containers = theme("containers", CONTAINERS);

                const mqs = Object.entries(screens);
                const widths = mqs
                    .map((item) => {
                        const key = item[0];
                        const value = item[1];
                        const width = containers[key];
                        if (width) {
                            return { [`@media (min-width: ${value})`]: { ".container": { "max-width": width } } };
                        }
                    })
                    .filter((i) => i);

                addComponents([
                    {
                        ".container": {
                            "margin-left": "auto",
                            "margin-right": "auto",
                            width: "100%",
                        },
                    },
                    ...widths,
                ]);
            },
            {
                content: [],
                theme: {
                    containers: CONTAINERS,
                },
            },
        ),
        plugin(({ addVariant }) => {
            addVariant("is-active", "&.is-active");
            addVariant("is-selected", "&.is-selected");
            addVariant("is-hoverable", ["&.is-hoverable:active", "&.is-hoverable:hover"]);
            addVariant("is-focused", "&.is-focused");
            addVariant("is-required", ["&.is-required", "&:required"]);
            addVariant("is-invalid", "&.is-invalid");
            addVariant("is-loading", ["&.is-loading"]);
            addVariant("is-readonly", ["&.is-readonly", "&:readonly"]);
            addVariant("is-disabled", ["&.is-disabled", "&:disabled"]);
            addVariant("is-empty", ["&.is-empty"]);
        }),
    ],
};
