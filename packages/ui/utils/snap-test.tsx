import { create } from "react-test-renderer";

import React, { ReactNode } from "react";
import { UiContextProvider } from "../context";
import { getProviderConfig } from "../_private-utils/get-provider-config";

export const snapTest = (name: string, component: ReactNode) => {
    it(name, () => {
        const tree = create(<UiContextProvider value={getProviderConfig()}>{component}</UiContextProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });
};
