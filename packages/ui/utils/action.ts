import { action as storybookAction, HandlerFunction } from "@storybook/addon-actions";

export function action(name: string, handler: HandlerFunction): HandlerFunction {
    return function (...args) {
        storybookAction(name)(...args);
        handler(...args);
    };
}
