import React, { createContext, ReactNode, useContext } from "react";
import { ButtonColor, ButtonSize, ButtonVariant } from "@uxf/ui/button";
import { ChipColor } from "../chip/chip";
import { InputGroupSizes } from "../input/theme";

export const defaultConfig: StorybookConfig = {
    Button: {
        sizes: ["xs", "sm", "default", "lg", "xl"],
        colors: ["default", "success", "error"],
        variants: ["default", "outlined", "white", "text"],
    },
    Chip: {
        colors: ["default", "red", "yellow", "green", "blue", "indigo", "purple", "pink"],
    },
    Input: {
        sizes: ["small", "default", "large"],
    },
    Select: {},
};

export interface StorybookConfig {
    Button: {
        sizes: ButtonSize[];
        variants: ButtonVariant[];
        colors: ButtonColor[];
    };
    Chip: {
        colors: ChipColor[];
    };
    Input: {
        sizes: (keyof InputGroupSizes)[];
    };
    Select: any;
}

// Context
const Context = createContext(defaultConfig);

// consumer
export function useStorybookConfig<T extends keyof StorybookConfig>(key: T): StorybookConfig[T] {
    return useContext(Context)[key];
}

// Provider
interface StorybookContextProviderProps {
    config: StorybookConfig;
    children: ReactNode;
}

export function StorybookContextProvider(props: StorybookContextProviderProps) {
    return <Context.Provider value={props.config}>{props.children}</Context.Provider>;
}
