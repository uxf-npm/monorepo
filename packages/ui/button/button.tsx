import { UseAnchorProps, useAnchorProps } from "@uxf/core/hooks/useAnchorProps";
import { cx } from "@uxf/core/utils/cx";
import { ButtonColors, ButtonSizes, ButtonVariants } from "@uxf/ui/button/theme";
import React, { AnchorHTMLAttributes } from "react";
import { forwardRef } from "@uxf/core/utils/forwardRef";

export type ButtonVariant = keyof ButtonVariants;
export type ButtonSize = keyof ButtonSizes;
export type ButtonColor = keyof ButtonColors;

export interface ButtonProps extends Omit<AnchorHTMLAttributes<HTMLAnchorElement>, "type">, UseAnchorProps {
    color?: ButtonColor;
    isFullWidth?: boolean;
    isIconButton?: boolean;
    size?: ButtonSize;
    variant?: ButtonVariant;
}

export const Button = forwardRef<HTMLAnchorElement, ButtonProps>("Button", (props, ref) => {
    // eslint-disable-next-line react/destructuring-assignment
    const { color, isIconButton, isFullWidth, size, variant, className, ...restProps } = props;

    const anchorProps = useAnchorProps({
        ...restProps,
        className: cx(
            "uxf-button",
            `uxf-button--color-${color ?? "default"}`,
            `uxf-button--size-${size ?? "default"}`,
            `uxf-button--variant-${variant ?? "default"}`,
            isIconButton && "uxf-button--icon-button",
            isFullWidth && "uxf-button--full-width",
            className,
        ),
    });

    return (
        <a ref={ref} {...anchorProps}>
            {typeof props.children === "string" ? (
                <span className="uxf-button__text">{props.children}</span>
            ) : (
                props.children
            )}
            <div className="loader" />
        </a>
    );
});
