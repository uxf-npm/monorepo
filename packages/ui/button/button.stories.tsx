import React, { Fragment } from "react";
import { Button } from "@uxf/ui/button";
import { useStorybookConfig } from "@uxf/ui/utils/storybook-config";
import { Icon } from "@uxf/ui/icon";
import Docs from "./button.docs.mdx";

export default {
    title: "UI/Button",
    component: Button,
    parameters: {
        docs: {
            page: Docs,
        },
    },
};

export function Default() {
    const config = useStorybookConfig("Button");

    return (
        <>
            {config.variants.map((variant) => (
                <Fragment key={variant}>
                    {config.colors.map((color) => (
                        <Fragment key={color}>
                            {config.sizes.map((size) => (
                                <div key={`${size}${color}${variant}`} className="space-y-2 pb-4">
                                    <div className="text-sm">
                                        Variant: <span className="font-semibold">{variant}</span>, Color:{" "}
                                        <span className="font-semibold">{color}</span>, Size:{" "}
                                        <span className="font-semibold">{size}</span>
                                    </div>
                                    <div className="flex flex-row items-start gap-2">
                                        <Button color={color} onClick={() => void null} size={size} variant={variant}>
                                            Default
                                        </Button>
                                        <Button
                                            color={color}
                                            loading
                                            onClick={() => void null}
                                            size={size}
                                            variant={variant}
                                        >
                                            Loading
                                        </Button>
                                        <Button
                                            color={color}
                                            disabled
                                            onClick={() => void null}
                                            size={size}
                                            variant={variant}
                                        >
                                            Disabled
                                        </Button>
                                        <Button
                                            color={color}
                                            isIconButton
                                            onClick={() => void null}
                                            size={size}
                                            variant={variant}
                                        >
                                            <Icon name="check" />
                                        </Button>
                                        <Button
                                            color={color}
                                            isIconButton
                                            onClick={() => void null}
                                            size={size}
                                            variant={variant}
                                            loading
                                        >
                                            <Icon name="check" />
                                        </Button>
                                        <Button
                                            className="w-[200px]"
                                            color={color}
                                            onClick={() => void null}
                                            size={size}
                                            variant={variant}
                                        >
                                            Custom width
                                        </Button>
                                    </div>
                                    <Button
                                        color={color}
                                        isFullWidth
                                        onClick={() => void null}
                                        size={size}
                                        variant={variant}
                                    >
                                        Full width
                                    </Button>
                                    <hr />
                                </div>
                            ))}
                        </Fragment>
                    ))}
                </Fragment>
            ))}
        </>
    );
}
