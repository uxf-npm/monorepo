export interface ButtonVariants {
    default: true;
    white: true;
    outlined: true;
    text: true;
}

export interface ButtonSizes {
    xs: true;
    sm: true;
    default: true;
    lg: true;
    xl: true;
}

export interface ButtonColors {
    default: true;
    success: true;
    error: true;
}
