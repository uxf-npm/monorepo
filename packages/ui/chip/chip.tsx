import { cx } from "@uxf/core/utils/cx";
import React, { HTMLAttributes } from "react";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { ChipColors, ChipSizes } from "@uxf/ui/chip/theme";

export type ChipColor = keyof ChipColors;
export type ChipSize = keyof ChipSizes;

export interface ChipProps extends HTMLAttributes<HTMLDivElement> {
    color?: ChipColor;
    size?: ChipSize;
    onClose?: () => void;
}

export const Chip = forwardRef<HTMLDivElement, ChipProps>("Chip", (props, ref) => {
    const chipClassName = cx(
        "uxf-chip",
        `uxf-chip--color-${props.color ?? "default"}`,
        `uxf-chip--size-${props.size ?? "default"}`,
        props.onClose && "has-button",
        props.className,
    );

    return (
        <div ref={ref} className={chipClassName}>
            {typeof props.children === "string" ? (
                <span className="uxf-chip__text">{props.children}</span>
            ) : (
                props.children
            )}
            {props.onClose && (
                <button type="button" onClick={props.onClose} className="uxf-chip__button">
                    <span className="sr-only">Remove option</span>
                    <svg stroke="currentColor" fill="none" viewBox="0 0 8 8">
                        <path strokeLinecap="round" strokeWidth="1.5" d="M1 1l6 6m0-6L1 7" />
                    </svg>
                </button>
            )}
        </div>
    );
});
