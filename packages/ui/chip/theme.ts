export interface ChipColors {
    default: true;
    red: true;
    yellow: true;
    green: true;
    blue: true;
    indigo: true;
    purple: true;
    pink: true;
}

export interface ChipSizes {
    default: true;
    large: true;
}
