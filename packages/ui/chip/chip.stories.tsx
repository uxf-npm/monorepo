import React from "react";
import { Chip } from "@uxf/ui/chip";
import { useStorybookConfig } from "../utils/storybook-config";
import Docs from "./chip.docs.mdx";

export default {
    title: "UI/Chip",
    component: Chip,
    parameters: {
        docs: {
            page: Docs,
        },
    },
};

export function Default() {
    const config = useStorybookConfig("Chip");

    const allChips = config.colors.map((color) => (
        <Chip key={color} color={color} onClose={() => null}>
            {color}
        </Chip>
    ));
    const allChipsLarge = config.colors.map((color) => (
        <Chip key={color} color={color} size="large">
            {color}
        </Chip>
    ));

    return (
        <div className="flex flex-col lg:flex-row">
            <div className="light space-y-4 rounded bg-white p-8">
                <div className="space-x-4">{allChips}</div>
                <div className="space-x-4">{allChipsLarge}</div>
            </div>
            <div className="dark space-y-4 rounded bg-gray-900 p-8">
                <div className="space-x-4">{allChips}</div>
                <div className="space-x-4">{allChipsLarge}</div>
            </div>
        </div>
    );
}
