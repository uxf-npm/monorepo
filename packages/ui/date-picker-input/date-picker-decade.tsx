import dayjs from "dayjs";
import React, { memo, MouseEventHandler, useCallback, useContext } from "react";
import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { useDecade } from "@uxf/datepicker/hooks/use-decade";
import { Button } from "../button";
import { Icon } from "../icon";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";

interface DatePickerDecadeProps {
    onYearSelect: (date: Date) => void;
}

export const DatePickerDecade = memo<DatePickerDecadeProps>((props) => {
    const { onYearSelect } = props;

    const { canGoToYear, goToNextYear, goToPrevYear, activeMonths } = useContext(DatePickerContext);
    const currentYear = activeMonths[0].year;

    const { decadeLabel, years } = useDecade({
        year: currentYear,
    });

    const canGoToPrevDecade = canGoToYear(dayjs(new Date(currentYear, 0)).subtract(4, "years").toDate());
    const canGoToNextDecade = canGoToYear(dayjs(new Date(currentYear, 0)).add(4, "years").toDate());

    const handleGoToPrevDecadeClick = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        if (canGoToPrevDecade) {
            goToPrevYear(9);
        }
    }, [canGoToPrevDecade, goToPrevYear]);

    const handleGoToNextDecadeClick = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        if (canGoToNextDecade) {
            goToNextYear(9);
        }
    }, [canGoToNextDecade, goToNextYear]);

    const handleSelectYear = useCallback<(date: Date) => MouseEventHandler<HTMLButtonElement>>(
        (date) => () => {
            if (canGoToYear(date)) {
                onYearSelect(date);
            }
        },
        [canGoToYear, onYearSelect],
    );

    return (
        <>
            <div className="uxf-date-picker__decade-select">
                <Button isIconButton onClick={handleGoToPrevDecadeClick} title="Zpět" variant="text">
                    <Icon name="chevronLeft" size={16} />
                </Button>
                <p className="uxf-date-picker__decade-label">{decadeLabel}</p>
                <Button isIconButton onClick={handleGoToNextDecadeClick} title="Vpřed" variant="text">
                    <Icon name="chevronRight" size={16} />
                </Button>
            </div>
            <div className="uxf-date-picker__decade-calendar">
                {years.map((year, index) => (
                    <button
                        className={cx(
                            "uxf-date-picker__cell uxf-date-picker__cell__year",
                            !canGoToYear(year.date) && CLASSES.IS_DISABLED,
                            dayjs(year.date).year() === currentYear && "uxf-date-picker__cell__year--current",
                        )}
                        key={year.yearLabel + index}
                        onClick={handleSelectYear(year.date)}
                    >
                        {year.yearLabel}
                    </button>
                ))}
            </div>
        </>
    );
});

DatePickerDecade.displayName = "DatePickerDecade";
