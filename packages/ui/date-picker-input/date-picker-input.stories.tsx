import React, { useState } from "react";
import { DatePickerInput } from "./date-picker-input";
import { Icon } from "../icon";
import Docs from "./date-picker.docs.mdx";

export default {
    title: "UI/DatePickerInput",
    component: DatePickerInput,
    parameters: {
        docs: {
            page: Docs,
        },
    },
};

export function Default() {
    const [date, setDate] = useState<Date | null>(null);

    const testDatePickers = (
        <>
            <DatePickerInput
                id="date-test"
                name="date"
                label="Datum čehokoliv"
                rightElement={<Icon name="calendar" size={20} />}
                value={date}
                onChange={(data) => setDate(data ?? null)}
            />
            <DatePickerInput
                id="date-test-disabled"
                name="date-disabled"
                label="Datum disabled"
                rightElement={<Icon name="calendar" size={20} />}
                value={date}
                onChange={(data) => setDate(data ?? null)}
                isDisabled
            />
            <DatePickerInput
                id="date-test-readonly"
                name="date-readonly"
                label="Datum readonly"
                rightElement={<Icon name="calendar" size={20} />}
                value={date}
                onChange={(data) => setDate(data ?? null)}
                isReadOnly
            />
            <DatePickerInput
                id="date-test-invalid"
                name="date-invalid"
                label="Datum invalid"
                rightElement={<Icon name="calendar" size={20} />}
                value={date}
                onChange={(data) => setDate(data ?? null)}
                isInvalid
            />
        </>
    );

    return (
        <>
            <div className="light max-w-[640px] space-y-4 rounded bg-white p-8">{testDatePickers}</div>
            <div className="dark max-w-[640px] space-y-4 rounded bg-gray-900 p-8 text-white">{testDatePickers}</div>
        </>
    );
}
