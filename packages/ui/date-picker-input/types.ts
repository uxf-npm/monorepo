import { ReactNode } from "react";

export interface DatePickerProps {
    children?: ReactNode;
}

export type ViewModeType = "month" | "year" | "decade";
