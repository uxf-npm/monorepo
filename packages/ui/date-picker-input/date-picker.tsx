import React, { FC, useCallback, useContext, useMemo, useState } from "react";
import { DatePickerYear } from "./date-picker-year";
import { DatePickerDecade } from "./date-picker-decade";
import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { DatePickerMonth } from "./date-picker-month";
import { DatePickerProps, ViewModeType } from "./types";

export const DatePicker: FC<DatePickerProps> = (props) => {
    const { goToDate, activeMonths } = useContext(DatePickerContext);

    const [viewMode, setViewMode] = useState<ViewModeType>("month");

    const onYearSelect = useCallback(() => setViewMode("decade"), [setViewMode]);

    const onMonthSelect = useCallback(
        (date: Date) => {
            goToDate(date);
            setViewMode("month");
        },
        [goToDate, setViewMode],
    );

    const onDecadeYearSelect = useCallback(
        (date: Date) => {
            goToDate(date);
            setViewMode("year");
        },
        [goToDate, setViewMode],
    );

    const datePickerComponents = useMemo(
        () => ({
            month: activeMonths.map((month) => (
                <DatePickerMonth key={`${month.year}-${month.month}`} setViewMode={setViewMode} />
            )),
            year: <DatePickerYear onMonthSelect={(date) => onMonthSelect(date)} onYearSelect={onYearSelect} />,
            decade: <DatePickerDecade onYearSelect={(date) => onDecadeYearSelect(date)} />,
        }),
        [onMonthSelect, onYearSelect, onDecadeYearSelect, setViewMode, activeMonths],
    );

    return (
        <div className="uxf-date-picker">
            {props.children}
            {datePickerComponents[viewMode]}
        </div>
    );
};
