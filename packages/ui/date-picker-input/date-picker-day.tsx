import React, { memo, useContext, useRef } from "react";
import { useDay } from "@uxf/datepicker/hooks/use-day";
import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { CalendarDay } from "@uxf/datepicker/utils/get-days";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";
import { useOnMount } from "@uxf/core/hooks/useOnMount";

interface DatePickerDayProps {
    currentMonth: boolean;
    day: CalendarDay;
}

export const DatePickerDay = memo<DatePickerDayProps>((props) => {
    const { day } = props;

    const dayCellRef = useRef<HTMLButtonElement>(null);

    const innerRef = useRef<HTMLButtonElement>(null);

    const {
        focusedDate,
        isDateBlocked,
        isDateFocused,
        isDateHovered,
        isDateSelected,
        onDateFocus,
        onDateHover,
        onDateSelect,
    } = useContext(DatePickerContext);

    const { disabledDate, isSelected, onClick, onKeyDown, onMouseEnter, tabIndex, isToday } = useDay({
        date: day.date,
        focusedDate,
        isDateBlocked,
        isDateFocused,
        isDateHovered,
        isDateSelected,
        onDateFocus,
        onDateHover,
        onDateSelect,
        dayRef: dayCellRef,
    });

    const isDisabled = disabledDate || !day.currentMonth;

    useOnMount(() => {
        const node = innerRef.current;

        if (node && (isToday || isSelected)) {
            node.focus();
        }
    });

    if (!day.dayLabel) {
        return <div />;
    }

    return (
        <button
            className={cx(
                "uxf-date-picker__cell uxf-date-picker__cell__day",
                isDisabled && CLASSES.IS_DISABLED,
                isToday && "uxf-date-picker__cell__day--today",
                isSelected && "uxf-date-picker__cell__day--selected",
                !props.currentMonth && "uxf-date-picker__cell__day--not-current-month",
            )}
            ref={isDisabled ? undefined : composeRefs(dayCellRef, innerRef)}
            onClick={onClick}
            onKeyDown={onKeyDown}
            onMouseEnter={onMouseEnter}
            tabIndex={tabIndex}
        >
            {day.dayLabel}
        </button>
    );
});

DatePickerDay.displayName = "DatePickerDay";
