import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { useYear } from "@uxf/datepicker/hooks/use-year";
import dayjs from "dayjs";
import React, { memo, MouseEventHandler, useCallback, useContext } from "react";
import { Button } from "../button";
import { Icon } from "../icon";

interface DatePickerYearProps {
    onMonthSelect: (date: Date) => void;
    onYearSelect: () => void;
}

export const DatePickerYear = memo<DatePickerYearProps>((props) => {
    const { canGoToNextYear, canGoToPrevYear, goToNextYear, goToPrevYear, canGoToMonth, activeMonths } =
        useContext(DatePickerContext);
    const currentMonth = activeMonths[0].month;
    const currentYear = activeMonths[0].year;

    const { months, yearLabel } = useYear({
        year: currentYear,
        yearLabelFormat: (date) => dayjs(date).format("YYYY"),
        monthLabelFormat: (date) => dayjs(date).format("MMMM"),
    });

    const handleGoToNextYear = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        if (canGoToNextYear) {
            goToNextYear(1);
        }
    }, [canGoToNextYear, goToNextYear]);

    const handleGoToPrevYear = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        if (canGoToPrevYear) {
            goToPrevYear(1);
        }
    }, [canGoToPrevYear, goToPrevYear]);

    const handleYearClick = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        props.onYearSelect();
    }, [props]);

    const handleMonthClick = useCallback<(date: Date) => MouseEventHandler<HTMLButtonElement>>(
        (date) => () => {
            if (canGoToMonth(date)) {
                props.onMonthSelect(date);
            }
        },
        [canGoToMonth, props],
    );

    return (
        <>
            <div className="uxf-date-picker__year-select">
                <Button
                    disabled={!canGoToPrevYear}
                    isIconButton
                    onClick={canGoToPrevYear ? handleGoToPrevYear : undefined}
                    title="Předchozí rok"
                    variant="text"
                >
                    <Icon name="chevronLeft" size={16} />
                </Button>
                <Button variant="text" onClick={handleYearClick}>
                    {yearLabel}
                </Button>
                <Button
                    disabled={!canGoToNextYear}
                    isIconButton
                    onClick={canGoToNextYear ? handleGoToNextYear : undefined}
                    title="Nadcházející rok"
                    variant="text"
                >
                    <Icon name="chevronRight" size={16} />
                </Button>
            </div>
            <div className="uxf-date-picker__year-calendar">
                {months.map((month, index) => (
                    <button
                        className={cx(
                            "uxf-date-picker__cell uxf-date-picker__cell__month",
                            !canGoToMonth(month.date) && CLASSES.IS_DISABLED,
                            dayjs(month.date).month() === currentMonth && "uxf-date-picker__cell__month--current",
                        )}
                        key={month.monthLabel + index}
                        onClick={handleMonthClick(month.date)}
                    >
                        {month.monthLabel}
                    </button>
                ))}
            </div>
        </>
    );
});

DatePickerYear.displayName = "DatePickerYear";
