import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { useMonth } from "@uxf/datepicker/hooks/use-month";
import dayjs from "dayjs";
import React, { Dispatch, memo, MouseEventHandler, SetStateAction, useCallback, useContext } from "react";
import { Button } from "../button";
import { Icon } from "../icon";
import { DatePickerDay } from "./date-picker-day";
import { ViewModeType } from "./types";

interface DatePickerMonthProps {
    setViewMode: Dispatch<SetStateAction<ViewModeType>>;
}

export const DatePickerMonth = memo<DatePickerMonthProps>((props) => {
    const { setViewMode } = props;

    const {
        activeMonths,
        canGoToNextMonth,
        canGoToPrevMonth,
        firstDayOfWeek,
        goToNextMonthsByOneMonth,
        goToPrevMonthsByOneMonth,
    } = useContext(DatePickerContext);

    const currentMonth = activeMonths[0].month;
    const currentYear = activeMonths[0].year;

    const { days, monthLabel, weekdayLabels } = useMonth({
        dayLabelFormat: (date) => dayjs(date).format("D"),
        firstDayOfWeek,
        month: currentMonth,
        monthLabelFormat: (date) => dayjs(date).format("MMMM YYYY"),
        weekdayLabelFormat: (date) => dayjs(date).format("dd"),
        year: currentYear,
    });

    const handleGoToPrevMonth = useCallback<MouseEventHandler<HTMLAnchorElement>>(
        () => goToPrevMonthsByOneMonth(),
        [goToPrevMonthsByOneMonth],
    );

    const handleGoToNextMonth = useCallback<MouseEventHandler<HTMLAnchorElement>>(
        () => goToNextMonthsByOneMonth(),
        [goToNextMonthsByOneMonth],
    );

    const handleMonthClick = useCallback<MouseEventHandler<HTMLAnchorElement>>(() => {
        setViewMode("year");
    }, [setViewMode]);

    return (
        <>
            <div className="uxf-date-picker__month-select">
                <Button
                    disabled={!canGoToPrevMonth}
                    isIconButton
                    onClick={canGoToPrevMonth ? handleGoToPrevMonth : undefined}
                    title="Zpět"
                    variant="text"
                >
                    <Icon name="chevronLeft" size={16} />
                </Button>
                <Button variant="text" onClick={handleMonthClick}>
                    {monthLabel}
                </Button>
                <Button
                    disabled={!canGoToNextMonth}
                    isIconButton
                    onClick={canGoToNextMonth ? handleGoToNextMonth : undefined}
                    title="Vpřed"
                    variant="text"
                >
                    <Icon name="chevronRight" size={16} />
                </Button>
            </div>
            <div className="uxf-date-picker__month-calendar">
                {weekdayLabels.map((weekdayLabel, index) => (
                    <span key={`${index}`} className="uxf-date-picker__weekday-label">
                        {weekdayLabel}
                    </span>
                ))}
                {days.map((day, index) => (
                    <DatePickerDay currentMonth={day.currentMonth} day={day} key={day.dayLabel + index} />
                ))}
            </div>
        </>
    );
});

DatePickerMonth.displayName = "DatePickerMonth";
