import React, { FC } from "react";
import { DatePickerContext } from "@uxf/datepicker/contexts/date-picker-context";
import { OnDateChangeType, useDatePicker } from "@uxf/datepicker/hooks/use-date-picker";
import { DatePicker } from "./date-picker";

export interface DatePickerProviderProps {
    closePopoverHandler: () => void;
    onChange?: (data: OnDateChangeType) => void;
    selectedDate?: Date | null;
}

export const DatePickerProvider: FC<DatePickerProviderProps> = (props) => {
    const datePickerProps = useDatePicker({
        firstDayOfWeek: 1,
        onDateChange: (data) => {
            if (props.onChange) {
                props.onChange(data);
            }
            props.closePopoverHandler();
        },
        selectedDate: props.selectedDate ?? null,
    });

    return (
        <DatePickerContext.Provider value={datePickerProps}>
            <DatePicker />
        </DatePickerContext.Provider>
    );
};
