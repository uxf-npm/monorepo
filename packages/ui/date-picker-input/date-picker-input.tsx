import { autoUpdate, flip, offset, shift, size, useFloating } from "@floating-ui/react";
import { Popover, Transition } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import dayjs from "dayjs";
import React, { FocusEventHandler, Fragment, ReactNode, useId, useMemo, useState } from "react";
import { FormControl } from "../form-control";
import { InputProps } from "../input/input";
import { FormControlProps } from "../types";
import { DatePickerProvider } from "./date-picker-provider";

export interface DatePickerInputProps
    extends Omit<FormControlProps<Date | null>, "value">,
        Pick<InputProps, "size" | "variant"> {
    className?: string;
    defaultValue?: string;
    error?: ReactNode;
    errorId?: string;
    form?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    id?: string;
    label?: ReactNode;
    leftAddon?: ReactNode;
    leftElement?: ReactNode;
    placeholder?: Date;
    rightAddon?: ReactNode;
    rightElement?: ReactNode;
    value?: Date | null;
}

export const DatePickerInput = forwardRef<HTMLInputElement, DatePickerInputProps>("DatePickerInput", (props, ref) => {
    const [isFocused, setIsFocused] = useState(false);
    const generatedId = useId();
    const id = props.id ?? generatedId;

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const rootClassName = cx(
        "uxf-date-picker-input",
        isFocused && CLASSES.IS_FOCUSED,
        props.isDisabled && CLASSES.IS_DISABLED,
        props.isInvalid && CLASSES.IS_INVALID,
        props.leftElement && "uxf-date-picker-input--has-left-element",
        props.rightElement && "uxf-date-picker-input--has-right-element",
        props.isRequired && CLASSES.IS_REQUIRED,
        props.isReadOnly && CLASSES.IS_READONLY,
    );

    const onFocus: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(true);
        props.onFocus?.(e);
    };

    const onBlur: FocusEventHandler<HTMLInputElement> = (e) => {
        setIsFocused(false);
        props.onBlur?.(e);
    };

    const floatingDatePicker = useFloating({
        placement: "bottom",
        middleware: [
            offset(8),
            flip(),
            shift(),
            size({
                apply({ availableHeight, availableWidth, elements, strategy, x, y }) {
                    Object.assign(elements.floating.style, {
                        left: x + "px",
                        maxHeight: Math.max(50, availableHeight) + "px",
                        maxWidth: availableWidth,
                        position: strategy,
                        top: y + "px",
                    });
                },
            }),
        ],

        whileElementsMounted: (reference, floating, update) =>
            autoUpdate(reference, floating, update, {
                elementResize: typeof ResizeObserver !== "undefined",
            }),
    });

    const stableRef = useMemo(
        () => composeRefs(ref, floatingDatePicker.reference),
        [ref, floatingDatePicker.reference],
    );

    return (
        <FormControl
            className={rootClassName}
            errorId={errorId}
            form={props.form}
            helperText={props.helperText}
            hiddenLabel={props.hiddenLabel}
            inputId={id}
            isRequired={props.isRequired}
            label={props.label}
        >
            <Popover className="relative">
                <Popover.Button as="div" className="uxf-date-picker-input__wrapper" ref={stableRef}>
                    <span className="uxf-date-picker-input__left-element">{props.leftElement}</span>
                    <input
                        aria-describedby={errorId}
                        aria-errormessage={props.error && errorId ? `${errorId}__error-message` : undefined}
                        aria-invalid={!!props.error}
                        aria-live="polite"
                        autoComplete="off"
                        className="uxf-date-picker-input__element"
                        disabled={props.isDisabled}
                        form={props.form}
                        id={id}
                        inputMode="none"
                        name={props.name}
                        onBlur={onBlur}
                        onChange={() => props.onChange}
                        onFocus={onFocus}
                        placeholder="Vyberte datum..."
                        readOnly={props.isReadOnly}
                        ref={ref}
                        required={props.isRequired}
                        tabIndex={props.isReadOnly ? -1 : undefined}
                        value={props.value ? dayjs(props.value).format("l") : ""}
                    />
                    <span className="uxf-date-picker-input__right-element">{props.rightElement}</span>
                </Popover.Button>
                {!props.isDisabled && !props.isReadOnly && (
                    <Transition
                        as={Fragment}
                        enter="transition duration-200 ease-out"
                        enterFrom="origin-top scale-50 opacity-0"
                        enterTo="origin-top scale-100 opacity-100"
                        leave="transition duration-200 ease-out"
                        leaveFrom="origin-top scale-50 opacity-100"
                        leaveTo="origin-top scale-0 opacity-0"
                    >
                        <Popover.Panel
                            className="uxf-date-picker-input__popover"
                            ref={floatingDatePicker.floating}
                            static
                        >
                            {({ close }) => (
                                <DatePickerProvider
                                    closePopoverHandler={close}
                                    onChange={props.onChange}
                                    selectedDate={props.value}
                                />
                            )}
                        </Popover.Panel>
                    </Transition>
                )}
            </Popover>
        </FormControl>
    );
});
