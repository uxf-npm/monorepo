import React from "react";
import { FormControl } from "./form-control";

export default {
    title: "UI/FormControl",
    component: FormControl,
    parameters: { actions: { argTypesRegex: "^on.*" } },
};

export function Default() {
    return (
        <div className="space-y-4">
            <FormControl inputId="default" helperText="helper text" label="Default">
                <div>Default</div>
            </FormControl>

            <FormControl inputId="default" helperText="helper text" label="Default" isRequired>
                <div>Is required</div>
            </FormControl>

            <FormControl inputId="error" helperText="helper text" label="Default" errorId="error--error" isRequired>
                <div>Is invalid</div>
            </FormControl>
            <FormControl inputId="error" helperText="helper text" label="Default" errorId="error--error" hiddenLabel>
                <div>hidden label</div>
            </FormControl>
        </div>
    );
}
