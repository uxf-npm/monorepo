import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { ReactElement, ReactNode } from "react";
import { Label } from "../label";

export interface FormControlProps {
    children?: ReactElement;
    className?: string;
    errorId?: string;
    form?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    inputId: string;
    isRequired?: boolean;
    label?: ReactNode;
}

// TODO - tohle asi ještě nějak předělám (ten interface je trochu nelogickej)
export const FormControl = forwardRef<HTMLDivElement, FormControlProps>("FormControl", (props, ref) => {
    const helperTextClassName = cx("uxf-helper-text", props.errorId && CLASSES.IS_INVALID);

    return (
        <div className={props.className} ref={ref}>
            <Label isHidden={props.hiddenLabel} isRequired={props.isRequired} htmlFor={props.inputId} form={props.form}>
                {props.label}
            </Label>
            {props.children}
            {props.helperText && (
                <div className={helperTextClassName} id={props.errorId}>
                    {props.helperText}
                </div>
            )}
        </div>
    );
});
