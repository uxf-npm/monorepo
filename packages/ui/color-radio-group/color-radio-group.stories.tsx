import { ColorRadioGroup } from "./index";
import React, { useState } from "react";
import { ColorRadioGroupOptionValue } from "./color-radio-group";
import { HexColor } from "../types";

export default {
    title: "UI/ColorRadioGroup",
    component: ColorRadioGroup,
};

const options: { value: HexColor; label: string }[] = [
    {
        value: "#ff0000",
        label: "Red",
    },
    {
        value: "#00ff00",
        label: "Green",
    },
    {
        value: "#ff00ff",
        label: "Purple",
    },
    {
        value: "#000000",
        label: "Red",
    },
    {
        value: "#ffff00",
        label: "Green",
    },
    {
        value: "#00ffff",
        label: "Purple",
    },
    {
        value: "#cccccc",
        label: "Red",
    },
    {
        value: "#3b1298",
        label: "Green",
    },
    {
        value: "#0000ff",
        label: "Purple",
    },
];

export function Default() {
    const [value, setValue] = useState<ColorRadioGroupOptionValue | null>(options[0].value);
    return (
        <div className="flex">
            <div className="light w-1/2 p-20">
                <ColorRadioGroup
                    id="radiogroup"
                    label="Light Color Radio group"
                    onChange={(v) => setValue(v)}
                    options={options}
                    value={value}
                />
            </div>
            <div className="dark w-1/2 bg-gray-900 p-20">
                <ColorRadioGroup
                    id="radiogroup"
                    label="Dark Color Radio group with helper text"
                    helperText="Some helper text"
                    onChange={(v) => setValue(v)}
                    options={options}
                    value={value}
                />
            </div>
        </div>
    );
}
