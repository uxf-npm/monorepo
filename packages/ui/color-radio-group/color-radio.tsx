import { CLASSES } from "@uxf/core/constants/classes";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { readableColorIsBlack } from "color2k";
import React, { CSSProperties } from "react";
import { FormControlProps, HexColor } from "../types";

export interface ColorRadioProps extends FormControlProps<boolean> {
    color: HexColor;
    colorLabel: string;
}

function CheckIcon() {
    return (
        <svg fill="none" viewBox="0 0 16 16">
            <path
                d="M13.333 4 6 11.333 2.667 8"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
            />
        </svg>
    );
}

export const ColorRadio = forwardRef<HTMLDivElement, ColorRadioProps>("ColorRadio", (props, ref) => {
    const colorRadioStyle = { "--bg-color": props.color } as Partial<CSSProperties>;

    return (
        <div
            className={cx("uxf-color-radio", props.value && CLASSES.IS_SELECTED)}
            onClick={() => props.onChange?.(!props.value)}
            ref={ref}
            style={colorRadioStyle}
        >
            <span className="uxf-color-radio__label">{props.colorLabel}</span>
            <span
                className={`uxf-color-radio__icon ${
                    readableColorIsBlack(props.color) ? "uxf-color-radio__icon--dark" : ""
                }`}
            >
                <CheckIcon />
            </span>
        </div>
    );
});
