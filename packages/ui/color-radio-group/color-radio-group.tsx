import { RadioGroup as HUIColorRadioGroup } from "@headlessui/react";
import React, { CSSProperties, ReactNode, useRef } from "react";
import { useInputSubmit } from "../hooks/use-input-submit";
import { Label } from "../label";
import { FormControlProps, HexColor } from "../types";
import { cx } from "@uxf/core/utils/cx";
import { CLASSES } from "@uxf/core/constants/classes";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { ColorRadio } from "./color-radio";

export type ColorRadioGroupOptionValue = HexColor;

export interface ColorRadioGroupOption {
    label: string;
    value: ColorRadioGroupOptionValue;
}

export interface ColorRadioGroupProps extends FormControlProps<ColorRadioGroupOptionValue | null> {
    className?: string;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    id: string;
    label: string;
    options: ColorRadioGroupOption[];
    style?: CSSProperties;
}

export const ColorRadioGroup = forwardRef<HTMLLabelElement, ColorRadioGroupProps>("ColorRadioGroup", (props, ref) => {
    const innerRef = useRef<HTMLDivElement>(null);

    useInputSubmit(innerRef, "radio-group", props.isDisabled);

    const errorId = props.isInvalid ? `${props.id}--errormessage` : undefined;

    const rootClassName = cx("uxf-color-radio-group-group", props.className);

    return (
        <HUIColorRadioGroup
            className={rootClassName}
            id={props.id}
            onChange={props.onChange}
            style={props.style}
            value={props.value}
        >
            <HUIColorRadioGroup.Label
                as={Label}
                className="uxf-color-radio-group__label"
                isHidden={props.hiddenLabel}
                ref={ref}
            >
                {props.label}
            </HUIColorRadioGroup.Label>

            <div className="uxf-color-radio-group__options-wrapper">
                {props.options.map((option) => {
                    const optionStyle = { "--option-color": option.value } as Partial<CSSProperties>;

                    return (
                        <HUIColorRadioGroup.Option
                            className={cx("uxf-color-radio-group__option", props.isDisabled && CLASSES.IS_DISABLED)}
                            key={option.value.toString()}
                            value={option.value}
                            style={optionStyle}
                        >
                            {(o) => (
                                <ColorRadio
                                    isDisabled={props.isDisabled || o.disabled}
                                    colorLabel={option.label}
                                    color={option.value}
                                    ref={o.checked ? innerRef : undefined}
                                    value={o.checked}
                                />
                            )}
                        </HUIColorRadioGroup.Option>
                    );
                })}
            </div>
            {props.helperText && (
                <div className={cx("uxf-helper-text", props.isInvalid && CLASSES.IS_INVALID)} id={errorId}>
                    {props.helperText}
                </div>
            )}
        </HUIColorRadioGroup>
    );
});
