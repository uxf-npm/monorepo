import React, { useState } from "react";
import ComponentStructureAnalyzer from "../utils/component-structure-analyzer";
import { ComboboxValue } from "./combobox";
import { Combobox } from "./index";

export default {
    title: "UI/Combobox",
    component: Combobox,
};

const options = [
    { id: "one", label: "Option one" },
    { id: "two", label: "Option two" },
    { id: "three", label: "Option three" },
    { id: "four", label: "Option with diacritics (ěščřžýáíé)" },
];

export function Default() {
    const [value, setValue] = useState<ComboboxValue | null | undefined>();
    const handleChange = (v: ComboboxValue | null) => {
        // eslint-disable-next-line no-console
        console.log("Select value: ", v);
        setValue(v);
    };

    const storyComboboxes = (
        <div className="space-y-8">
            <Combobox
                id="combobox-1"
                label="Combobox"
                name="combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Combobox
                helperText="Start typing to see options..."
                id="combobox-1"
                label="Combobox with helper text"
                name="combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Combobox
                helperText="Error message"
                id="combobox-1"
                isInvalid
                isRequired
                label="Combobox invalid"
                name="combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Combobox
                dropdownPlacement="top"
                id="combobox-1"
                label="Combobox with dropdown top"
                name="combobox"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
        </div>
    );

    return (
        <div className="flex flex-col lg:flex-row">
            <div className="light space-y-2 p-20 lg:w-1/2">{storyComboboxes}</div>
            <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storyComboboxes}</div>
        </div>
    );
}

export function ComponentStructure() {
    const [value, setValue] = useState<ComboboxValue | null | undefined>(null);
    const handleChange = (v: ComboboxValue | null) => {
        setValue(v);
    };

    return (
        <ComponentStructureAnalyzer>
            <Combobox
                id="combobox-structure"
                name="combobox"
                label="Combobox with helper text"
                options={options}
                onChange={handleChange}
                value={value}
                helperText="Start typing to see options..."
            />
        </ComponentStructureAnalyzer>
    );
}
