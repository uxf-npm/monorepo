import { Placement } from "@floating-ui/react";
import { Combobox as HUICombobox } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { useInputFocus } from "@uxf/core/hooks/useInputFocus";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { slugify } from "@uxf/core/utils/slugify";
import React, { ReactNode, useId, useMemo, useRef, useState } from "react";
import { useDropdown } from "../hooks/use-dropdown";
import { Icon } from "../icon";
import { IconsSet } from "../icon/theme";
import { Label } from "../label";
import { FormControlProps } from "../types";

export type ComboboxValue = number | string;

export type ComboboxOption<T = ComboboxValue> = {
    disabled?: boolean;
    id: T;
    label: string;
};

export interface ComboboxProps<Value = ComboboxValue, Option = ComboboxOption<Value>>
    extends FormControlProps<Value | null> {
    className?: string;
    dropdownPlacement?: Placement;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    iconName?: keyof IconsSet;
    id?: string;
    keyExtractor?: (option: Option) => string | number;
    label: string;
    options: Option[];
    placeholder?: string;
    renderOption?: (option: Option) => ReactNode;
}

type ComboboxTypeRef = HTMLDivElement;

export const Combobox = forwardRef<ComboboxTypeRef, ComboboxProps>("Combobox", (props, ref) => {
    const generatedId = useId();
    const id = props.id ?? generatedId;

    const [query, setQuery] = useState("");

    const selectedOption = props.options.find((option) => option.id === props.value);

    const filteredData =
        query === "" ? props.options : props.options.filter((item) => slugify(item.label).includes(slugify(query)));

    const innerRef = useRef<ComboboxTypeRef>(null);

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const input = useInputFocus(innerRef, props.onBlur, props.onFocus);

    const dropdown = useDropdown(props.dropdownPlacement ?? "bottom", true);

    const stableRef = useMemo(() => composeRefs(innerRef, ref, dropdown.reference), [ref, dropdown.reference]);

    const iconName = props.iconName ?? "chevronDown";

    return (
        <HUICombobox
            as="div"
            className={cx(
                "uxf-combobox",
                props.isInvalid && CLASSES.IS_INVALID,
                props.isRequired && CLASSES.IS_REQUIRED,
                props.isReadOnly && CLASSES.IS_READONLY,
                props.isDisabled && CLASSES.IS_DISABLED,
                props.className,
            )}
            onChange={(v: ComboboxOption) => props.onChange?.(v.id)}
            value={selectedOption}
            disabled={props.isDisabled || props.isReadOnly}
        >
            {(renderProps) => (
                <>
                    <HUICombobox.Label
                        as={Label}
                        isHidden={props.hiddenLabel}
                        onClick={props.isDisabled || props.isReadOnly ? undefined : input.focus}
                    >
                        {props.label}
                    </HUICombobox.Label>
                    <HUICombobox.Button
                        as="div"
                        className={cx(
                            "uxf-combobox__button",
                            (renderProps.open || input.focused) && CLASSES.IS_FOCUSED,
                            renderProps.disabled && CLASSES.IS_DISABLED,
                            props.isReadOnly && CLASSES.IS_READONLY,
                            props.isInvalid && CLASSES.IS_INVALID,
                            dropdown.placement === "bottom" && "is-open--bottom",
                            dropdown.placement === "top" && "is-open--top",
                            renderProps.open && CLASSES.IS_OPEN,
                        )}
                        onBlur={input.onBlur}
                        onFocus={input.onFocus}
                        tabIndex={props.isDisabled || props.isReadOnly ? undefined : 0}
                        ref={stableRef}
                        aria-invalid={props.isInvalid}
                        aria-describedby={errorId}
                    >
                        <HUICombobox.Input
                            className={cx("uxf-combobox__input")}
                            displayValue={(item: ComboboxOption | null) => item?.label ?? ""}
                            onBlur={props.onBlur}
                            onChange={(event) => setQuery(event.target.value)}
                            placeholder={props.placeholder}
                            ref={stableRef}
                            type="text"
                        />
                        <Icon
                            className={cx("uxf-select__button-icon", renderProps.open && "is-open")}
                            name={iconName}
                        />
                    </HUICombobox.Button>
                    <HUICombobox.Options
                        className={cx(
                            "uxf-dropdown",
                            dropdown.placement === "bottom" && "uxf-dropdown--bottom",
                            dropdown.placement === "top" && "uxf-dropdown--top",
                        )}
                        ref={dropdown.floating}
                    >
                        {filteredData.map((option) => (
                            <HUICombobox.Option
                                key={props.keyExtractor?.(option) ?? option.id}
                                value={option}
                                className={(optionState) =>
                                    cx(
                                        "uxf-dropdown__item",
                                        optionState.active && CLASSES.IS_ACTIVE,
                                        optionState.disabled && CLASSES.IS_DISABLED,
                                        optionState.selected && CLASSES.IS_SELECTED,
                                    )
                                }
                            >
                                {props.renderOption?.(option) ?? option.label}
                            </HUICombobox.Option>
                        ))}
                    </HUICombobox.Options>
                    {props.helperText && (
                        <div className={cx("uxf-helper-text", props.isInvalid && CLASSES.IS_INVALID)} id={errorId}>
                            {props.helperText}
                        </div>
                    )}
                </>
            )}
        </HUICombobox>
    );
});
