import { Input as RootInput } from "./input";
import { InputElement } from "./input-element";
import { InputLeftAddon } from "./input-left-addon";
import { InputLeftElement } from "./input-left-element";
import { InputRightAddon } from "./input-right-addon";
import { InputRightElement } from "./input-right-element";

export const Input = Object.assign(RootInput, {
    Element: InputElement,
    LeftElement: InputLeftElement,
    RightElement: InputRightElement,
    LeftAddon: InputLeftAddon,
    RightAddon: InputRightAddon,
});
