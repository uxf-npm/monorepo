import React, { ReactNode } from "react";

export interface InputRightAddonProps {
    children: ReactNode;
}

export function InputRightAddon(props: InputRightAddonProps) {
    return <div className="uxf-input__right-addon">{props.children}</div>;
}

InputRightAddon.displayName = "InputRightAddon";
