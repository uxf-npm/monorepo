export interface InputGroupVariants {
    default: true;
}

export interface InputGroupSizes {
    small: true;
    default: true;
    large: true;
}
