import React, { ReactNode } from "react";

interface InputLeftElementProps {
    children?: ReactNode;
}

export function InputLeftElement(props: InputLeftElementProps) {
    return <div className="uxf-input__left-element">{props.children}</div>;
}

InputLeftElement.displayName = "InputLeftElement";
