import React, { ReactNode } from "react";

interface InputRightElementProps {
    children?: ReactNode;
}

export function InputRightElement(props: InputRightElementProps) {
    return <div className="uxf-input__right-element">{props.children}</div>;
}

InputRightElement.displayName = "InputRightElement";
