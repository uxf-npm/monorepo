import { forwardRef } from "@uxf/core/utils/forwardRef";
import { FormControlProps } from "@uxf/ui/types/form-control-props";
import React, { KeyboardEventHandler } from "react";

type InputMode = "none" | "text" | "tel" | "url" | "email" | "numeric" | "decimal" | "search" | undefined;

export interface InputElementProps extends FormControlProps<string> {
    "aria-describedby"?: string;
    "aria-invalid"?: boolean;
    form?: string;
    id?: string;
    inputMode?: InputMode;
    onKeyDown?: KeyboardEventHandler<HTMLInputElement>;
    placeholder?: string;
    type?: "email" | "number" | "password" | "search" | "tel" | "text" | "url" | "time";

    // type: "password" | "search" | "tel" | "text" | "url"
    maxLength?: number | undefined;
    minLength?: number | undefined;

    // type: "number"
    max?: number | string;
    min?: number | string;
    pattern?: string;
    step?: number | string;
}

type InputTypeRef = HTMLInputElement;

export const InputElement = forwardRef<InputTypeRef, InputElementProps>("InputElement", (props, ref) => {
    return (
        <input
            aria-describedby={props["aria-describedby"]}
            aria-invalid={props.isInvalid}
            className="uxf-input__element"
            disabled={props.isDisabled}
            form={props.form}
            id={props.id}
            inputMode={props.inputMode}
            max={props.max}
            maxLength={props.maxLength}
            min={props.min}
            minLength={props.minLength}
            onBlur={props.onBlur}
            onChange={(event) => props.onChange?.(event.target.value, event)}
            onFocus={props.onFocus}
            onKeyDown={props.onKeyDown}
            pattern={props.pattern}
            placeholder={props.placeholder}
            readOnly={props.isReadOnly}
            ref={ref}
            step={props.step}
            tabIndex={props.isReadOnly ? -1 : undefined}
            type={props.type}
            value={props.value}
        />
    );
});
