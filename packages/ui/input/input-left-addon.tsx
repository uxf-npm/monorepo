import React, { ReactNode } from "react";

interface InputLeftAddonProps {
    children: ReactNode;
}

export function InputLeftAddon(props: InputLeftAddonProps) {
    return <div className="uxf-input__left-addon">{props.children}</div>;
}

InputLeftAddon.displayName = "InputLeftAddon";
