import { composeRefs } from "@uxf/core/utils/composeRefs";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, {
    ReactElement,
    ReactNode,
    useRef,
    useState,
    Children,
    isValidElement,
    cloneElement,
    FocusEventHandler,
} from "react";
import { cx } from "@uxf/core/utils/cx";
import { InputGroupSizes, InputGroupVariants } from "@uxf/ui/input/theme";
import { CLASSES } from "@uxf/core/constants/classes";

export interface InputProps {
    children: ReactNode;
    variant?: keyof InputGroupVariants;
    size?: keyof InputGroupSizes;
}

function getChildrenById(children: ReactNode, componentName: string): ReactElement | undefined {
    return Children.toArray(children)
        .filter(isValidElement)
        .find((ch) => (ch.type as any).displayName === componentName);
}

type InputTypeRef = HTMLInputElement;

export const Input = forwardRef<InputTypeRef, InputProps>("Input", (props, ref) => {
    const inputWrapperRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<InputTypeRef>(null);
    const [focused, setFocused] = useState(false);

    const mainInput = getChildrenById(props.children, "InputElement");
    const leftAddon = getChildrenById(props.children, "InputLeftAddon");
    const rightAddon = getChildrenById(props.children, "InputRightAddon");
    const leftElement = getChildrenById(props.children, "InputLeftElement");
    const rightElement = getChildrenById(props.children, "InputRightElement");

    const inputGroupClasses = cx(
        "uxf-input",
        leftAddon && "uxf-input--has-left-addon",
        leftElement && "uxf-input--has-left-element",
        rightElement && "uxf-input--has-right-element",
        rightAddon && "uxf-input--has-right-addon",
        focused && CLASSES.IS_FOCUSED,
        mainInput?.props.isInvalid && CLASSES.IS_INVALID,
        mainInput?.props.isDisabled && CLASSES.IS_DISABLED,
        mainInput?.props.isReadOnly && CLASSES.IS_READONLY,
        `uxf-input--size-${props.size ?? "default"}`,
        `uxf-input--variant-${props.variant ?? "default"}`,
    );

    const onFocus: FocusEventHandler<HTMLElement> = (e) => {
        setFocused(true);
        mainInput?.props.onFocus?.(e);
    };

    const onBlur: FocusEventHandler<HTMLElement> = (e) => {
        setFocused(false);
        mainInput?.props.onBlur?.(e);
    };

    return (
        <div className={inputGroupClasses}>
            {leftAddon}
            <div className="uxf-input__wrapper" ref={inputWrapperRef} onClick={() => inputRef.current?.focus()}>
                {leftElement}
                {mainInput ? cloneElement(mainInput, { ref: composeRefs(ref, inputRef), onFocus, onBlur }) : null}
                {rightElement}
            </div>
            {rightAddon}
        </div>
    );
});
