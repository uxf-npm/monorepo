import React, { useState } from "react";
import ComponentStructureAnalyzer from "@uxf/ui/utils/component-structure-analyzer";
import { action } from "@uxf/ui/utils/action";
import { Input } from "@uxf/ui/input";
import { useStorybookConfig } from "../utils/storybook-config";

export default {
    title: "UI/Input",
    component: Input,
};

export function Default() {
    const [value, onChange] = useState("");
    const [isInvalid, setInvalid] = useState(false);

    const storyInputs = (
        <>
            <Input>
                <Input.LeftAddon>InputLeftAddon</Input.LeftAddon>
                <Input.LeftElement>InputLeftElement</Input.LeftElement>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Placeholder"
                    isInvalid={isInvalid}
                />
                <Input.RightElement>InputRightElement</Input.RightElement>
                <Input.RightAddon>InputRightAddon</Input.RightAddon>
            </Input>
            <Input>
                <Input.LeftAddon>https://</Input.LeftAddon>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Placeholder"
                    isInvalid={isInvalid}
                />
                <Input.RightAddon>.uxf.cz</Input.RightAddon>
            </Input>
            <Input>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Placeholder"
                    isInvalid={isInvalid}
                />
                <Input.RightAddon>.test.cz</Input.RightAddon>
            </Input>
            <Input>
                <Input.LeftElement>🌷</Input.LeftElement>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Placeholder"
                    isInvalid={isInvalid}
                />
                <Input.RightElement>🔔</Input.RightElement>
            </Input>
            <Input>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Placeholder"
                    isInvalid={isInvalid}
                />
            </Input>
            <Input>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Disabled"
                    isInvalid={isInvalid}
                    isDisabled
                />
            </Input>
            <Input>
                <Input.Element
                    value={value}
                    onChange={action("onChange", onChange)}
                    placeholder="Read Only"
                    isInvalid={isInvalid}
                    isReadOnly
                />
            </Input>
        </>
    );

    return (
        <>
            <input type="checkbox" checked={isInvalid} onChange={(e) => setInvalid(e.target.checked)} />
            <div className="light space-y-2 p-20">{storyInputs}</div>
            <div className="dark space-y-2 bg-gray-900 p-20">{storyInputs}</div>
        </>
    );
}

export function Sizes() {
    const config = useStorybookConfig("Input");

    const [value, setValue] = useState("");

    return (
        <div className="space-y-4">
            {config.sizes.map((size) => (
                <Input key={size} size={size}>
                    <Input.LeftAddon>LeftAddon</Input.LeftAddon>
                    <Input.LeftElement>🌷</Input.LeftElement>
                    <Input.Element value={value} onChange={action("onChange", setValue)} placeholder={size} />
                    <Input.RightElement>🔔</Input.RightElement>
                    <Input.RightAddon>RightAddon</Input.RightAddon>
                </Input>
            ))}
        </div>
    );
}

export function ComponentStructure() {
    const [value, onChange] = useState("");
    return (
        <ComponentStructureAnalyzer>
            <Input>
                <Input.LeftAddon>Left addon</Input.LeftAddon>
                <Input.LeftElement>Left element</Input.LeftElement>
                <Input.Element value={value} onChange={onChange} />
                <Input.RightElement>Right element</Input.RightElement>
                <Input.RightAddon>Right addon</Input.RightAddon>
            </Input>
        </ComponentStructureAnalyzer>
    );
}
