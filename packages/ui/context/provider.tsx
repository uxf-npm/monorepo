import { extend } from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
import React, { FC, ReactNode } from "react";
import { UiContext, UiContextType } from "./context";

extend(localizedFormat);

type UiContextProviderProps = {
    value: UiContextType;
    children: ReactNode;
};
export const UiContextProvider: FC<UiContextProviderProps> = (props) => (
    <UiContext.Provider value={props.value}>{props.children}</UiContext.Provider>
);
