import { useContext } from "react";
import { UiContext, UiContextType } from "./context";

export type UiContextComponent = keyof UiContextType;

export const useComponentContext = <T extends UiContextComponent>(componentName: T): UiContextType[T] => {
    const context = useContext(UiContext);

    if (typeof context === "undefined") {
        throw new Error(
            `If you want to use the "${componentName}" UI component, you have to wrap your code in the "UiContextProvider"`,
        );
    }

    return context[componentName];
};
