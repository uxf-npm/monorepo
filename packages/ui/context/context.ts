import { createContext } from "react";
import { IconName } from "../icon/types";

export type UiContextType = {
    icon: {
        spriteFilePath: string;
        iconsConfig: Record<IconName, { w: number; h: number }>;
    };
    rasterImage: {
        breakpoints: Record<string, string>;
    };
};

export const UiContext = createContext<UiContextType | undefined>(undefined);
