import React, { ReactNode, useCallback, useState } from "react";
import { ImageGalleryProvider } from "./context";
import { ImageGalleryImageProps } from "./types";
import Gallery from "./components/gallery";

interface ImageGalleryProviderProps {
    children: ReactNode;
}

function ImageGallery(props: ImageGalleryProviderProps) {
    const [images, setImages] = useState<ImageGalleryImageProps[]>([]);
    const [imageIndex, setImageIndex] = useState<null | number>(null);

    const registerImage = useCallback((image: ImageGalleryImageProps) => setImages((v) => [...v, image]), []);

    const unregisterImage = useCallback(
        (image: ImageGalleryImageProps) => setImages((v) => v.filter((u) => u.src !== image.src)),
        [],
    );

    const openGallery = useCallback(
        (image: ImageGalleryImageProps) => setImageIndex(images.findIndex((i) => i.src === image.src)),
        [images],
    );

    const onPrevious = useCallback(() => {
        setImageIndex((v) => (v === null ? null : v - 1));
    }, []);

    const onNext = useCallback(() => {
        setImageIndex((v) => (v === null ? null : v + 1));
    }, []);

    // modulo bug https://stackoverflow.com/a/4467559
    const moduloImageIndex =
        imageIndex === null
            ? null
            : imageIndex < 0
            ? ((imageIndex % images.length) + images.length) % images.length
            : imageIndex % images.length;

    const contextValue = { registerImage, unregisterImage, openGallery };

    return (
        <>
            <ImageGalleryProvider value={contextValue}>{props.children}</ImageGalleryProvider>
            {typeof moduloImageIndex === "number" && (
                <Gallery
                    onClose={() => setImageIndex(null)}
                    onNext={onNext}
                    onPrevious={onPrevious}
                    imageIndex={moduloImageIndex}
                    images={images}
                />
            )}
        </>
    );
}

export default ImageGallery;
