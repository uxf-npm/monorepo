import { ImageSource } from "@uxf/core/utils/resizer";

export interface ImageGalleryImageProps {
    src: ImageSource;
    title?: string;
    alt?: string;
    className?: string;
}
