import React from "react";
import { useImage } from "./use-image";
import { ImageGalleryImageProps } from "@uxf/ui/image-gallery/types";
import { resizerImageUrl } from "@uxf/core/utils/resizer";

function Image(props: ImageGalleryImageProps) {
    const openGallery = useImage(props);

    return (
        <img
            src={typeof props.src === "string" ? props.src : resizerImageUrl(props.src) ?? ""}
            alt={props.alt}
            title={props.title}
            className={props.className}
            onClick={openGallery}
        />
    );
}

export default Image;
