import { createContext, useContext } from "react";
import { ImageGalleryImageProps } from "./types";

type ContextType = {
    registerImage: (props: ImageGalleryImageProps) => void;
    unregisterImage: (props: ImageGalleryImageProps) => void;
    openGallery: (props: ImageGalleryImageProps) => void;
};

const ImageGalleryContext = createContext<ContextType>({
    registerImage: () => null,
    unregisterImage: () => null,
    openGallery: () => null,
});

export const ImageGalleryProvider = ImageGalleryContext.Provider;
export const useImageGalleryContext = () => useContext(ImageGalleryContext);
