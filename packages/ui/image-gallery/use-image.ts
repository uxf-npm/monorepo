import { useCallback, useEffect } from "react";
import { useImageGalleryContext } from "./context";
import { ImageGalleryImageProps } from "./types";

export function useImage(props: ImageGalleryImageProps) {
    const { registerImage, unregisterImage, openGallery } = useImageGalleryContext();

    useEffect(() => {
        registerImage(props);
        return () => {
            unregisterImage(props);
        };
    }, [props, registerImage, unregisterImage]);

    return useCallback(() => openGallery(props), [props, openGallery]);
}
