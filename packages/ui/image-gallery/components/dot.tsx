import { cx } from "@uxf/core/utils/cx";
import React from "react";

interface DotProps {
    active?: boolean;
}

function Dot(props: DotProps) {
    const className = cx("h-2 w-2 rounded-full", props.active ? "bg-gray-500" : "bg-white");

    return <div className={className} />;
}

export default Dot;
