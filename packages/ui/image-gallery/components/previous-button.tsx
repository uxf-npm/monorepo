import React from "react";
import { Icon } from "../../icon";

interface PreviousButtonProps {
    onClick: () => void;
}

function PreviousButton(props: PreviousButtonProps) {
    return (
        <button className="pointer-events-auto rounded-full bg-black bg-opacity-50 p-3" onClick={props.onClick}>
            <Icon className="h-8 w-8 text-white" name="chevronLeft" />
        </button>
    );
}

export default PreviousButton;
