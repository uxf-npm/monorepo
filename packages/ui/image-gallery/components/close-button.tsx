import React from "react";
import { Icon } from "../../icon";

interface CloseButtonProps {
    onClick: () => void;
}

function CloseButton(props: CloseButtonProps) {
    return (
        <button className="bg-black bg-opacity-50 p-5" onClick={() => props.onClick()}>
            <Icon className="h-6 w-6 text-white" name="xmarkLarge" />
        </button>
    );
}

export default CloseButton;
