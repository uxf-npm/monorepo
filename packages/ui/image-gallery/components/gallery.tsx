import { getImgUniqueIdentifier } from "@uxf/core/utils/image";
import { resizerImageUrl } from "@uxf/core/utils/resizer";
import React, { useEffect } from "react";
import { ImageGalleryImageProps } from "../types";
import CloseButton from "./close-button";
import Dot from "./dot";
import NextButton from "./next-button";
import PreviousButton from "./previous-button";

interface GalleryProps {
    onClose: () => void;
    onNext: () => void;
    onPrevious: () => void;
    imageIndex: number;
    images: ImageGalleryImageProps[];
}

function Gallery(props: GalleryProps) {
    useEffect(() => {
        const onKeyDown = (event: KeyboardEvent) => {
            switch (event.key) {
                case "ArrowRight":
                    props.onNext();
                    break;
                case "ArrowLeft":
                    props.onPrevious();
                    break;
                case "Escape":
                    props.onClose();
                    break;
            }
        };

        document.body.style.overflow = "hidden";
        window.addEventListener("keydown", onKeyDown);

        return () => {
            document.body.style.overflow = "auto";
            window.removeEventListener("keydown", onKeyDown);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const imageSrc = props.images[props.imageIndex].src;

    return (
        <div className="fixed inset-0 top-0 bottom-0 z-50 flex flex-col bg-black bg-opacity-75 transition-opacity">
            <div className="flex justify-end">
                <CloseButton onClick={props.onClose} />
            </div>
            <div className="container mx-auto flex h-full flex-1 flex-col">
                <div className="relative flex flex-1 items-center justify-center">
                    <img
                        alt=""
                        className="absolute left-0 right-0 top-0 h-full w-full object-contain"
                        src={typeof imageSrc === "string" ? imageSrc : resizerImageUrl(imageSrc) ?? ""}
                    />
                    <div className="pointer-events-none absolute inset-0 flex items-center justify-between p-2">
                        <PreviousButton onClick={props.onPrevious} />
                        <NextButton onClick={props.onNext} />
                    </div>
                </div>
            </div>
            <div className="row mx-auto flex space-x-2 p-4">
                {props.images.map((image, index) => (
                    <Dot key={getImgUniqueIdentifier(image.src) || index} active={index === props.imageIndex} />
                ))}
            </div>
        </div>
    );
}

export default Gallery;
