import { getImgUniqueIdentifier } from "@uxf/core/utils/image";
import { ImageSource } from "@uxf/core/utils/resizer";
import React from "react";
import Image from "./image";
import ImageGallery from "./image-gallery";

export default {
    title: "Lab/ImageGallery",
    component: ImageGallery,
    parameters: { actions: { argTypesRegex: "^on.*" } },
};

const images: Array<ImageSource> = [
    "https://opentopo.app/hero_image.jpeg",
    "https://opentopo.app/generated/area-18/d/9/d9033d93-ea48-4f66-b0c1-6045c091b326_1024_1024_c.jpg",
    "https://opentopo.app/generated/area-16/f/4/f4a4ca79-ea9e-44d1-a4ec-3c8ccfbcc218_1024_1024_c.jpg",
    "https://opentopo.app/generated/area-1/8/b/8b84e378-1f55-4ce7-b750-b504091cb833_1024_1024_c.jpg",
    "https://opentopo.app/generated/area-2/e/7/e7ec532b-20c2-407e-9305-fa7cfb71f01f_1024_1024_c.jpg",
];

export function Default() {
    return (
        <ImageGallery>
            <div className="flex-direction flex gap-2">
                {images.map((src, index) => (
                    <Image
                        key={getImgUniqueIdentifier(src) || index}
                        src={src}
                        className="h-56 w-56 cursor-pointer object-cover transition-transform hover:scale-105"
                    />
                ))}
            </div>
        </ImageGallery>
    );
}
