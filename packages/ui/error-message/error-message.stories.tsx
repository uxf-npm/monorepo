import { ErrorMessage } from "./index";
import React from "react";

export default {
    title: "UI/ErrorMessage",
    component: ErrorMessage,
};

export function Default() {
    return (
        <div className="space-y-2">
            <ErrorMessage>This field is required.</ErrorMessage>
        </div>
    );
}
