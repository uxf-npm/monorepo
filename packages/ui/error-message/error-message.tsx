import React, { ReactNode } from "react";

interface Props {
    id?: string | undefined;
    children?: ReactNode;
}

export function ErrorMessage(props: Props) {
    return (
        <p id={props.id} className="uxf-error-message">
            {props.children}
        </p>
    );
}
