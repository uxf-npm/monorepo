import { Select } from "./index";
import { SelectValue } from "./select";
import React, { useState } from "react";
import ComponentStructureAnalyzer from "../utils/component-structure-analyzer";

export default {
    title: "UI/Select",
    component: Select,
};

const options = [
    { id: "one", label: "Option one" },
    { id: "two", label: "Option two" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
    { id: "three", label: "Option three" },
];

export function Default() {
    const [value, setValue] = useState<number | string | undefined | null>();
    const handleChange = (v: SelectValue | null) => {
        // eslint-disable-next-line no-console
        console.log("Select value: ", v);
        setValue(v);
    };

    const storySelects = (
        <>
            <Select
                helperText="Helper text"
                label="Default select"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                helperText="Helper text"
                isInvalid
                label="Invalid select"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                helperText="Helper text"
                isDisabled
                label="Disabled select"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                helperText="Helper text"
                isReadOnly
                label="Read only select"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                helperText="Helper text"
                isRequired
                label="Required select"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                dropdownPlacement="top"
                isRequired
                label="Select with dropdown top"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
            <Select
                helperText="Helper text of Select with hidden label"
                hiddenLabel
                label="Hidden label"
                onChange={handleChange}
                options={options}
                placeholder="Vyberte .."
                value={value}
            />
        </>
    );

    return (
        <div className="flex flex-col lg:flex-row">
            <div className="light space-y-2 p-20 lg:w-1/2">{storySelects}</div>
            <div className="dark space-y-2 bg-gray-900 p-20 lg:w-1/2">{storySelects}</div>
        </div>
    );
}

export function ComponentStructure() {
    const [value, setValue] = useState<SelectValue | null | undefined>(null);
    const handleChange = (v: SelectValue | null) => {
        // eslint-disable-next-line no-console
        console.log("Select value: ", v);
        setValue(v);
    };

    return (
        <ComponentStructureAnalyzer>
            <Select
                id="select-1"
                name="select"
                label="Choose option..."
                options={options}
                onChange={handleChange}
                value={value}
            />
        </ComponentStructureAnalyzer>
    );
}
