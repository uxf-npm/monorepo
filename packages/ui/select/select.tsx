import type { Placement } from "@floating-ui/react";
import { Listbox } from "@headlessui/react";
import { CLASSES } from "@uxf/core/constants/classes";
import { useInputFocus } from "@uxf/core/hooks/useInputFocus";
import { composeRefs } from "@uxf/core/utils/composeRefs";
import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { ReactNode, useId, useMemo, useRef } from "react";
import { useDropdown } from "../hooks/use-dropdown";
import { Icon } from "../icon";
import { IconsSet } from "../icon/theme";
import { Label } from "../label";
import { FormControlProps } from "../types";

export type SelectValue = number | string;

export type SelectOption<T = SelectValue> = {
    disabled?: boolean;
    id: T;
    label: ReactNode;
};

export interface SelectProps<Value = SelectValue, Option = SelectOption<Value>> extends FormControlProps<Value | null> {
    className?: string;
    dropdownPlacement?: Placement;
    helperText?: ReactNode;
    hiddenLabel?: boolean;
    iconName?: keyof IconsSet;
    id?: string;
    keyExtractor?: (option: Option) => string | number;
    label?: ReactNode;
    options: Option[];
    placeholder?: string;
    renderOption?: (option: Option) => ReactNode;
}
type SelectTypeRef = HTMLDivElement;

export const Select = forwardRef<SelectTypeRef, SelectProps>("Select", (props, ref) => {
    const generatedId = useId();
    const id = props.id ?? generatedId;

    const selectedOption = props.options.find((option) => option.id === props.value);

    const innerRef = useRef<SelectTypeRef>(null);

    const errorId = props.isInvalid ? `${id}--error-message` : undefined;

    const input = useInputFocus(innerRef, props.onBlur, props.onFocus);

    const dropdown = useDropdown(props.dropdownPlacement ?? "bottom", true);

    const stableRef = useMemo(() => composeRefs(innerRef, ref, dropdown.reference), [ref, dropdown.reference]);

    const iconName = props.iconName ?? "chevronDown";

    return (
        <Listbox
            as="div"
            className={cx(
                "uxf-select",
                props.isInvalid && CLASSES.IS_INVALID,
                props.isRequired && CLASSES.IS_REQUIRED,
                props.isReadOnly && CLASSES.IS_READONLY,
                props.isDisabled && CLASSES.IS_DISABLED,
                props.className,
            )}
            onChange={(v) => props.onChange?.(v.id)}
            value={selectedOption}
            disabled={props.isDisabled || props.isReadOnly}
        >
            {(renderProps) => (
                <>
                    <Listbox.Label
                        as={Label}
                        isHidden={props.hiddenLabel}
                        onClick={props.isDisabled || props.isReadOnly ? undefined : input.focus}
                    >
                        {props.label}
                    </Listbox.Label>
                    <Listbox.Button
                        as="div"
                        className={cx(
                            "uxf-select__button",
                            (renderProps.open || input.focused) && CLASSES.IS_FOCUSED,
                            renderProps.disabled && CLASSES.IS_DISABLED,
                            props.isReadOnly && CLASSES.IS_READONLY,
                            props.isInvalid && CLASSES.IS_INVALID,
                            dropdown.placement === "bottom" && "is-open--bottom",
                            dropdown.placement === "top" && "is-open--top",
                            renderProps.open && "is-open",
                        )}
                        onBlur={input.onBlur}
                        onFocus={input.onFocus}
                        tabIndex={props.isDisabled || props.isReadOnly ? undefined : 0}
                        ref={stableRef}
                        aria-invalid={props.isInvalid}
                        aria-describedby={errorId}
                    >
                        <div className={cx("uxf-select__button-text", !selectedOption?.label && "is-empty")}>
                            {selectedOption?.label || props.placeholder}
                        </div>
                        <Icon
                            className={cx("uxf-select__button-icon", renderProps.open && "is-open")}
                            name={iconName}
                        />
                    </Listbox.Button>
                    <Listbox.Options
                        ref={dropdown.floating}
                        className={cx(
                            "uxf-dropdown",
                            dropdown.placement === "bottom" && "uxf-dropdown--bottom",
                            dropdown.placement === "top" && "uxf-dropdown--top",
                        )}
                    >
                        {props.options.map((option) => (
                            <Listbox.Option
                                key={props.keyExtractor?.(option) ?? option.id}
                                value={option}
                                className={(optionState) =>
                                    cx(
                                        "uxf-dropdown__item",
                                        optionState.active && CLASSES.IS_ACTIVE,
                                        optionState.disabled && CLASSES.IS_DISABLED,
                                        optionState.selected && CLASSES.IS_SELECTED,
                                    )
                                }
                            >
                                {props.renderOption?.(option) ?? option.label}
                            </Listbox.Option>
                        ))}
                    </Listbox.Options>
                    {props.helperText && (
                        <div className={cx("uxf-helper-text", props.isInvalid && CLASSES.IS_INVALID)} id={errorId}>
                            {props.helperText}
                        </div>
                    )}
                </>
            )}
        </Listbox>
    );
});
