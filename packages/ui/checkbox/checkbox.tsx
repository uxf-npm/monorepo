import { Switch as HUISwitch } from "@headlessui/react";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import React, { CSSProperties, ReactNode } from "react";
import { CheckboxVisual } from "../checkbox-visual";
import { CheckboxSizes } from "../checkbox-visual/theme";
import { FormControlProps } from "../types";

export type CheckboxSize = keyof CheckboxSizes;

export interface CheckboxProps extends FormControlProps<boolean> {
    className?: string;
    indeterminate?: boolean;
    isFocused?: boolean; // TODO move to FormControlProps and implement for each component
    label: ReactNode;
    size?: CheckboxSize;
    style?: Partial<CSSProperties>;
}

export const Checkbox = forwardRef<HTMLButtonElement, CheckboxProps>("Checkbox", (props, ref) => {
    return (
        <HUISwitch.Group>
            <div className={`uxf-checkbox__wrapper ${props.className ?? ""}`}>
                <HUISwitch
                    checked={props.value}
                    className="uxf-checkbox-visual-wrapper"
                    disabled={props.isDisabled}
                    name={props.name}
                    onChange={props.onChange}
                    ref={ref}
                    style={props.style}
                >
                    <CheckboxVisual
                        indeterminate={props.indeterminate}
                        isDisabled={props.isDisabled}
                        isFocused={props.isFocused}
                        isInvalid={props.isInvalid}
                        isReadOnly={props.isReadOnly}
                        isRequired={props.isRequired}
                        size={props.size}
                        value={props.value}
                    />
                </HUISwitch>
                <HUISwitch.Label className="uxf-checkbox__label">{props.label}</HUISwitch.Label>
            </div>
        </HUISwitch.Group>
    );
});
