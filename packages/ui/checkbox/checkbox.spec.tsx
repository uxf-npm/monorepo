import React from "react";
import { Checkbox } from "./checkbox";
import { snapTest } from "../utils/snap-test";

snapTest("render basic", <Checkbox label="label" onChange={() => void null} />);
snapTest("render disabled", <Checkbox isDisabled label="label" onChange={() => void null} />);
snapTest("render size lg", <Checkbox size="lg" label="label" onChange={() => void null} />);
