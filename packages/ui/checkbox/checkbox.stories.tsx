import { Checkbox } from "./index";
import React, { useState } from "react";

export default {
    title: "UI/Checkbox",
    component: Checkbox,
};

export function Default() {
    const [checked, setChecked] = useState<boolean>(true);

    const checkboxes = (
        <>
            <Checkbox label="Opravdu?" onChange={() => setChecked((prev) => !prev)} value={checked} />
            <Checkbox label="Opravdu?" isDisabled onChange={() => setChecked((prev) => !prev)} value={checked} />
            <Checkbox label="Opravdu?" isInvalid onChange={() => setChecked((prev) => !prev)} value={checked} />
            <Checkbox
                label="Opravdu? (size lg)"
                onChange={() => setChecked((prev) => !prev)}
                value={checked}
                size="lg"
            />
        </>
    );

    return (
        <div className="flex">
            <div className="light flex flex-col gap-4 p-20">{checkboxes}</div>
            <div className="dark flex flex-col gap-4 bg-gray-900 p-20">{checkboxes}</div>
        </div>
    );
}
