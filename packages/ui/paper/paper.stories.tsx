import { Paper } from "./paper";
import React from "react";

export default {
    title: "UI/Paper",
    component: Paper,
};

export function Default() {
    return (
        <div className="flex items-center justify-center bg-gray-100">
            <Paper>
                <div className="flex items-center justify-center p-24">Paper default</div>
            </Paper>
        </div>
    );
}
