import { cx } from "@uxf/core/utils/cx";
import React, { HTMLAttributes } from "react";
import { forwardRef } from "@uxf/core/utils/forwardRef";

export interface BadgeProps extends HTMLAttributes<HTMLDivElement> {
    className?: string;
}

export const Paper = forwardRef<HTMLDivElement, BadgeProps>("Paper", (props, ref) => {
    const paperClassName = cx("uxf-paper", props.className);

    return (
        <div ref={ref} className={paperClassName}>
            {props.children}
        </div>
    );
});
