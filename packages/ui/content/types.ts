import { ReactNode, FC } from "react";
import { ContentSchema } from "@uxf/ui/content/content-schema";
import { UseControllerProps } from "react-hook-form";

export type ContentType = keyof ContentSchema;

// backoffice
interface ContentFormProps<TYPE extends ContentType> extends UseControllerProps<ContentSchema[TYPE]> {
    type: TYPE;
    index: number;
    onRemove: () => void;
    TypeInputProps: any; // TODO
}
export type ContentFormComponent<TYPE extends ContentType> = FC<ContentFormProps<TYPE>> & {
    getConfig: () => { type: TYPE; label: ReactNode };
};

// web
interface ContentRendererProps<TYPE extends ContentType> {
    type: TYPE;
    data: ContentSchema[TYPE];
    index: number;
}
export type ContentRendererComponent<TYPE extends ContentType> = FC<ContentRendererProps<TYPE>> & {
    getConfig: () => { type: TYPE };
};
