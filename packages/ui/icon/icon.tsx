import { cx } from "@uxf/core/utils/cx";
import { forwardRef } from "@uxf/core/utils/forwardRef";
import { rem } from "@uxf/styles/units/rem";
import React, { CSSProperties } from "react";
import { useComponentContext } from "../context";
import { IconName } from "./types";

type NameOrComponentType =
    | {
          name: IconName;
          Component?: never;
      }
    | {
          name?: never;
          Component: any;
      };

export type IconProps = {
    className?: string;
    mode?: "meet" | "slice";
    size?: number;
    style?: Partial<CSSProperties>;
} & NameOrComponentType;

export const Icon = forwardRef<SVGSVGElement, IconProps>("Icon", (props, ref) => {
    const componentContext = useComponentContext("icon");

    const CustomComponent = props.Component;

    const className = cx("uxf-icon", props.className);
    const preserveAspectRatio = `xMidYMid ${props.mode ?? "meet"}`;
    const style = props.size
        ? ({
              "--i-h": rem(props.size),
              "--i-w": rem(props.size),
              ...(props.style ?? {}),
          } as Partial<CSSProperties>)
        : props.style;

    if (CustomComponent) {
        return (
            <CustomComponent
                className={className}
                preserveAspectRatio={preserveAspectRatio}
                ref={ref}
                role="img"
                style={style}
            />
        );
    }

    if (props.name && props.name in componentContext.iconsConfig) {
        const icon = componentContext.iconsConfig[props.name];

        return (
            <svg
                className={className}
                preserveAspectRatio={preserveAspectRatio}
                ref={ref}
                role="img"
                style={style}
                viewBox={`0 0 ${icon.w} ${icon.h}`}
            >
                <use xlinkHref={`${componentContext.spriteFilePath}#icon-sprite--${props.name}`} />
            </svg>
        );
    }

    // eslint-disable-next-line no-console
    console.warn(`Icon "${props.name}" not found.`);

    return (
        <span title={`icon "${props.name}" not found`}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width={props.size} height={props.size}>
                <path d="M144 416c-17.67 0-32 14.33-32 32s14.33 32.01 32 32.01s32-14.34 32-32.01S161.7 416 144 416zM211.2 32H104C46.66 32 0 78.66 0 136v16C0 165.3 10.75 176 24 176S48 165.3 48 152v-16c0-30.88 25.12-56 56-56h107.2C244.7 80 272 107.3 272 140.8c0 22.66-12.44 43.27-32.5 53.81L167 232.8C137.1 248 120 277.9 120 310.6V328c0 13.25 10.75 24.01 24 24.01S168 341.3 168 328V310.6c0-14.89 8.188-28.47 21.38-35.41l72.47-38.14C297.7 218.2 320 181.3 320 140.8C320 80.81 271.2 32 211.2 32z" />
            </svg>
        </span>
    );
});
