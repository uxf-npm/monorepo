import { Icon } from "./icon";
import React from "react";
import { useComponentContext } from "../context";
import Docs from "./icon.docs.mdx";

export default {
    title: "UI/Icon",
    component: Icon,
    parameters: {
        docs: {
            page: Docs,
        },
    },
};

export function AllIcons() {
    const iconContext = useComponentContext("icon");

    return (
        <div className="flex flex-row flex-wrap">
            {Object.keys(iconContext.iconsConfig).map((iconName) => (
                <div key={iconName} className="flex h-28 w-28 flex-col items-center justify-center gap-3">
                    <Icon name={iconName} size={24} />
                    <div className="text-gray-400">
                        <small>{iconName}</small>
                    </div>
                </div>
            ))}
        </div>
    );
}

export function ColorAndSizes() {
    return (
        <div className="flex flex-row flex-wrap gap-4">
            <Icon name="camera" size={12} />
            <Icon name="camera" size={16} />
            <Icon name="camera" size={20} />
            <Icon name="camera" size={12} className="text-primary-600" />
            <Icon name="camera" size={16} className="text-orange-600" />
            <Icon name="camera" size={20} className="text-green-600" />
        </div>
    );
}

export function CustomComponent() {
    const SvgIcon: React.FC = (props) => {
        return (
            <svg viewBox="0 0 29.65 40" {...props}>
                <path
                    fill="currentColor"
                    fillRule="evenodd"
                    d="M0 14.83C0 6.61 6.61 0 14.83 0c8.11 0 14.83 6.61 14.83 14.83 0 8.64-8.85 19.41-14.83 25.17C8.75 34.24 0 23.47 0 14.83zm9.28 0c0-3.09 2.45-5.55 5.55-5.55 2.99 0 5.44 2.45 5.44 5.55 0 2.99-2.45 5.44-5.44 5.44-3.1 0-5.55-2.46-5.55-5.44z"
                    clipRule="evenodd"
                />
            </svg>
        );
    };

    return <Icon Component={SvgIcon} size={16} />;
}

export function SpriteIcon() {
    return <Icon name="camera" size={16} />;
}

export function SpriteIconNotExists() {
    return <Icon name="not-exists" size={16} />;
}
