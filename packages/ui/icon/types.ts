import { IconsSet } from "@uxf/ui/icon/theme";

export type IconName = keyof IconsSet;
