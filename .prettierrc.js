module.exports = {
    bracketSameLine: false,
    bracketSpacing: true,
    parser: "typescript",
    plugins: [],
    printWidth: 120,
    semi: true,
    singleQuote: false,
    tabWidth: 4,
    trailingComma: "all",
};
