/** @type {import('tailwindcss').Config} */
const tailwindConfig = {
    presets: [require("./packages/ui/utils/tailwind-config")],
    content: ["./packages/**/*.{ts,tsx}"],
};

module.exports = tailwindConfig;
