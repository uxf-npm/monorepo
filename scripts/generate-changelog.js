const conventionalRecommendedBump = require(`conventional-recommended-bump`);
var conventionalChangelog = require("conventional-changelog");

function getVersion() {
    return new Promise((resolve, reject) => {
        conventionalRecommendedBump(
            {
                preset: `angular`,
            },
            (error, recommendation) => {
                if (error) {
                    reject(error);
                    return;
                }
                resolve(recommendation.releaseType);
            },
        );
    });
}

function getChangeLog() {
    conventionalChangelog({
        preset: "angular",
    }).pipe(process.stdout);
}
