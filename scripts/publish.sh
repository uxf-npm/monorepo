#!/bin/sh

npmVersion=$1;

rm -rf ./node_modules
npm install
npm run build --workspaces

npm version $npmVersion --workspaces
